/*!
 * 修正コレスキー分解(modified Cholesky decomposition)
 *  - 対称行列A(n×n)を下三角行列(L:Lower triangular matrix)と対角行列の積(LDL^T)に分解する
 *  - l_ii = 1とした場合
 *  - L: i > jの要素が非ゼロで対角成分は1
 * @param[in] A n×nの対称行列
 * @param[out] L 対角成分が1の下三角行列
 * @param[out] d 対角行列(対角成分のみ)
 * @param[in] n 行列の大きさ
 * @return 1:成功,0:失敗
 */
int spModifiedCholeskyDecomp(const int *Ap, const int *Ai, const t_element *A, t_element (*L)[DIM], t_element *d, int n)
{
    if(n <= 0) return 0;
 	int *LUp, *LUi;
	t_element *LUx;
    /***
	int idx = 0;
	int Apidx = 0;
	int num;
	  for (Apidx = 0;Apidx < n;Apidx++){
	    num = Ap[Apidx+1] - Ap[Apidx];

	    for (int x = 0;x < n;x++){
	      if( x == Ai[idx] && num > 0) { fprintf( stderr, "%.4f  ", A[idx]); ++idx; --num;}
	      else               { fprintf( stderr, "*.****  " );                  }
	    } fprintf(stderr,"\n");
	}
    ***/
    
    d[0] = A[0];
    //L[0][0] = 1.0;
	Lp[0] = 0;
	Li = 0;
	Lx = 1.0;
    //    fprintf(stderr,"A[0] %f\n",A[0]);

    int idx = 0;
    int cidx;
    int num;
    t_element val;
    t_element lld;
    t_element ld;
    
	int lidx;
	int liidx = 0;
    for (int i = 1;i < n;i++) {
		if(i % 100 == 0)fprintf(stderr,".");
		idx = Ap[i];//対角成分から先は見ないから毎回基底アドレスを代入しないといけない
		num = Ap[i+1] - Ap[i];
		//i行目で処理する要素の数
		lidx = Lp[i-1];
		liidx = 0;
		//fprintf(stderr,"%d\n",num);
		for (cidx = 0; cidx < i; cidx++) {
			if( cidx == Ai[idx] && num > 0) 
			{
				lld = A[idx];
				++idx;
				--num;	
			}
			else
				lld = 0.0;

			for (int k = 0;k < cidx;k++) {
				//lld -= L[i][k]*L[cidx][k]*d[k];
				lld -= Lx[lidx + k] * Lx[lidx + cidex + k] * d[k];
			}
			//L[i][cidx] = (1.0/d[cidx])*lld;
			Lx[lidx + cidex] = (1.0/d[cidx])*lld;
			Lp[i] += 1;
			Li[liidx++] =  
		}

		//if(cidx != i){fprintf(stderr,"%d %d\n",cidx,i);sleep(1);}
		//fprintf(stderr,"%d(%d) \n",num,i);
		//if(cidx == Ai[idx] ) 
		if(i == Ai[idx] ) 
			ld = A[idx];
		else 
			ld = 0.0;
		for(int k = 0; k < i; ++k){
			//ld -= L[cidx][k]*L[cidx][k]*d[k];
			ld -= L[i][k]*L[i][k]*d[k];
		}
		//d[cidx] = ld;
		//L[cidx][cidx] = 1.0;  
		d[i] = ld;
		L[i][i] = 1.0;  
      
	}
    
	fprintf(stderr,"LDLT factorization(sp) Completed.\n");
	return 1;

}


