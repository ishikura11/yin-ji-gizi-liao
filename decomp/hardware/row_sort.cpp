#include "takapack.h"

void row_sort(int *arow, int *acol, t_element *aval, int nnz)
{
	int idx = 0;
	q_sort(arow, acol, aval, 0, nnz-1);

	int row_idx = 0;
	int *aidx_list = (int *)sds_alloc(sizeof(int) * DIM);
	int *tmp_acol = (int *)sds_alloc(sizeof(int) * DIM);
	t_element *tmp_aval = (t_element *)sds_alloc(sizeof(t_element) * DIM);

	arr_init(aidx_list, DIM);
	arr_init(tmp_acol, DIM);
	arr_init(tmp_aval, DIM);
	int list_idx = 0;
	for (int i = 0; i < nnz; i++ ) {
		if (arow[i] != row_idx) {
			q_sort(tmp_acol, tmp_aval, 0, list_idx - 1);
			for (int j = 0; j < list_idx; j++) {
				acol[aidx_list[j]] = tmp_acol[j];
				aval[aidx_list[j]] = tmp_aval[j];
			}
			list_idx = 0;
			arr_init(aidx_list, DIM);
			arr_init(tmp_acol, DIM);
			arr_init(tmp_aval, DIM);
			row_idx = arow[i];

			aidx_list[list_idx] = i;
			tmp_acol[list_idx] = acol[i];
			tmp_aval[list_idx] = aval[i];
			list_idx++;
		} else if (i == (nnz)-1) {
			aidx_list[list_idx] = i;
			tmp_acol[list_idx] = acol[i];
			tmp_aval[list_idx] = aval[i];
			list_idx++;
			q_sort(tmp_acol, tmp_aval, 0, list_idx - 1);
			for (int j = 0; j < list_idx; j++) {
				acol[aidx_list[j]] = tmp_acol[j];
				aval[aidx_list[j]] = tmp_aval[j];
			}
		} else {
			aidx_list[list_idx] = i;
			tmp_acol[list_idx] = acol[i];
			tmp_aval[list_idx] = aval[i];
			list_idx++;
		}
	}
	sds_free(aidx_list);
	sds_free(tmp_acol);
	sds_free(tmp_aval);
}
