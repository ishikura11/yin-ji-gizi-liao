/* ----------------------------------------------------
takapack.h is written by Takashi Ijiri @ RIKEN
This libraly support computing
 + Dense  linear system (by CG method)
 + Sparse linear system (by CG method)
 + Sparse linear system (by LU factorization)

 This codes is distributed with NYSL (Version 0.9982) license.

 免責事項   : 本ソースコードによって起きたいかなる障害についても、著者は一切の責任を負いません。
 disclaimer : The author (Takashi Ijiri) is not to be held responsible for any problems caused by this software.
 ----------------------------------------------------*/

//#include "StdAfx.h"
#include "takapack.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/*----------------------------
    2  3  0  0  0        8
    3  0  4  0  6       45
A =   0 -1 -3  2  0    b =-3
    0  0  1  0  0        3
    0  4  2  0  1       19
----------------------------*/

//(umfpackは complessed columnだけど、実装の都合上 Compressed Rowを採用 ())
// umfpackと併用するときは umfpack_solve の第一引数に UMFPACK_At を食わせて転置すればOK
//compressed row form of A
/*
const int N = 5;
int    Ap [ ] = { 0,        2,          5,            8,   9,      12} ;
int    Ai [ ] = { 0,  1,    0, 2, 4,   1,  2, 3,    2,   1, 2, 4   } ;
t_element Ax [ ] = { 2., 3.,   3, 4, 6,  -1, -3, 2,    1,   4, 2, 1   } ;
*/
/*
void takapack_test(int *Ap, int *Ai, t_element *Ax, t_element *x1, t_element *x2, t_element *b, t_element (*e)[DIM], int dim)
{

  const int N = DIM;

  //takapack_traceMat(dim,Ap,Ai,Ax);

  int    *LUp, *LUi, *LU_rowflip;
  t_element *LUx;
#ifdef LU
  ///***
  static t_element inv[N][N];
  takapack_LU_factorization( dim, Ap, Ai, Ax, LUp, LUi, LUx, LU_rowflip);

  //takapack_traceMat(N,LUp,LUi,LUx);

#ifdef SOL
  takapack_LU_solve        ( dim, LUp, LUi, LUx, LU_rowflip, b, x1 );
  //fprintf( stderr, "%f %f %f %f %f\n", x1[0], x1[1], x1[2], x1[3], x1[4]);
#endif

#ifdef INV
  fprintf(stderr,"[");
  for (int j = 0;j < dim;j++){
    takapack_LU_solve        ( dim, LUp, LUi, LUx, LU_rowflip, e[j], x1 );

    for (int i = 0; i < dim;i++){
      fprintf(stderr,"%f ",x1[i]);
      inv[i][j] = x1[i];
    }
    if(j < dim-1)fprintf(stderr,";\n");
  }
  fprintf(stderr,"]\n\n");
#ifdef VERIFY
  verify_inv(dim, Ap, Ai , Ax, inv );
#endif
#endif
#endif

  //***/
  //takapack_LU_free( LUp, LUi, LUx, LU_rowflip);
  //{
  //takapack_CG_sparse_solve(N, Ap, Ai, Ax, b, x2, 0.00000001);
  //fprintf( stderr, "%f %f %f %f %f\n", x2[0], x2[1], x2[2], x2[3], x2[4]);

  //}
//}
/*
void takapack_CG_dense_solve( const int N, t_element** A, const t_element* b, t_element* result)
{
	size_t t_element_N = sizeof( t_element ) * N;

	t_element *cg_r = new t_element[ N ];
	t_element *cg_d = new t_element[ N ];
	t_element *cg_q = new t_element[ N ];

	memset( result, 0, t_element_N );
	int iteration = 0;

	//r = b - Ax (  A is symmetric!!  )
	memcpy( cg_r, b, t_element_N );
	for( int i = 0; i < N; ++i)
	for( int j = 0; j < N; ++j) cg_r[ i ] -= A[i][j] * result[ j ];

	//d = r
	memcpy( cg_d, cg_r, t_element_N );

	//deltaNew = r_t * r
	t_element deltaNew = 0;
	for( int i = 0; i < N; ++i) deltaNew += cg_r[i] * cg_r[i];

	while( iteration < N && deltaNew > 0.000000001)
	{
		//q = Ad
		memset( cg_q, 0, t_element_N );
		for( int i = 0; i < N; ++i)
		for( int j = 0; j < N; ++j) cg_q[i] += A[i][j] * cg_d[j];

		//alpha = deltaNew / (d_t * q )
		t_element alpha = 0;
		for( int i = 0; i < N ; ++i ) alpha += cg_d[i] * cg_q[i];
		alpha = deltaNew / alpha;

		// x = x + alpha * d
		for(int i = 0; i < N; ++i) result[i] += alpha * cg_d[i];

		if( iteration % 30 == 0 ){//r = b - Ax
			memcpy( cg_r, b, t_element_N );
			for( int i = 0; i < N; ++i)
			for( int j = 0; j < N; ++j) cg_r[i] -= A[i][j] * result[j];

		}else{//r = r - alpha * q
			for( int i = 0; i < N; ++i) cg_r[i] -= alpha * cg_q[i];
		}

		//deltaOld = deltaNew
		t_element deltaOld = deltaNew;

		//deltaNew = r*r
		deltaNew = 0;
		for( int i = 0; i < N; ++i) deltaNew += cg_r[i] * cg_r[i];

		//beta = deltaNew / deltaOld
		t_element beta = deltaNew / deltaOld;

		//d = r + beta + d
		for( int i = 0; i < N; ++i) cg_d[i] = cg_r[i] + beta * cg_d[i];
		++iteration;
	}

	delete[] cg_r;
	delete[] cg_d;
	delete[] cg_q;
}
*/


/*
bool takapack_CG_sparse_solve( const int N,
							   const int    *Ap, const int* Ai, const t_element* Ax,
							   const t_element *b ,
							   t_element* result, t_element  threshold)
{
	size_t t_element_N = sizeof( t_element ) * N;
	t_element *m_cg_r = new t_element[ N ];
	t_element *m_cg_d = new t_element[ N ];
	t_element *m_cg_q = new t_element[ N ];
	const int    *m_Ap_c = Ap;
	const int    *m_Ai_c = Ai;
	const t_element *m_Ax_c = Ax;

	int iteration = 0;

	//r = b - Ax (  A is symmetric!!  )
	memcpy( m_cg_r, b, t_element_N );
	memset( result, 0, t_element_N );

	for( int i = 0        ; i < N          ; ++i)
	for( int j = m_Ap_c[i]; j < m_Ap_c[i+1]; ++j)
			m_cg_r[ i ] -= m_Ax_c[ j ] * result[ m_Ai_c[j] ];

	//d = r
	memcpy( m_cg_d, m_cg_r, t_element_N );

	//deltaNew = r_t * r
	t_element deltaNew = 0;
	for( int i = 0; i < N; ++i) deltaNew += m_cg_r[i] * m_cg_r[i];


	//deltaZero = deltaNew
	t_element deltaZero = deltaNew;

	while( ( iteration < N && deltaNew > threshold) )
	{
		//q = Ad
		memset( m_cg_q, 0, t_element_N );
		for( int i = 0        ; i < N          ; ++i)
		for( int j = m_Ap_c[i]; j < m_Ap_c[i+1]; ++j)
			m_cg_q[ i ] += m_Ax_c[ j ] * m_cg_d[ m_Ai_c[j] ];


		//alpha = deltaNew / (d_t * q )
		t_element alpha = 0;
		for( int i = 0; i < N ; ++i ) alpha += m_cg_d[i] * m_cg_q[i];
		alpha = deltaNew / alpha;


		// x = x + alpha * d
		for(int i = 0; i < N; ++i) result[i] += alpha * m_cg_d[i];

		if( iteration % 20 == 0 ){
			//r = b - Ax
			memcpy( m_cg_r, b, t_element_N );
			for( int i = 0        ; i < N          ; ++i)
			for( int j = m_Ap_c[i]; j < m_Ap_c[i+1]; ++j)
					m_cg_r[ i ] -= m_Ax_c[ j ] * result[ m_Ai_c[j] ];

		}else{
			//r = r - alpha * q
			for( int i = 0; i < N; ++i) m_cg_r[i] -= alpha * m_cg_q[i];
		}

		//deltaOld = deltaNew
		t_element deltaOld = deltaNew;

		//deltaNew = r*r
		deltaNew = 0;
		for( int i = 0; i < N; ++i) deltaNew += m_cg_r[i] * m_cg_r[i];

		//beta = deltaNew / deltaOld
		t_element beta = deltaNew / deltaOld;

		//d = r + beta + d
		for( int i = 0; i < N; ++i) m_cg_d[i] = m_cg_r[i] + beta * m_cg_d[i];

		++iteration;
	}

	delete[] m_cg_r ;
	delete[] m_cg_d ;
	delete[] m_cg_q ;
	return true;
}
*/

/*
void takapack_traceMat( int N, const int *Ap , const int *Ai , const t_element *Ax )
{
	fprintf( stderr, "\ntakapack_tracemat\n");

	int idx = 0;
	int Apidx = 0;
	int num;
	  for (Apidx = 0;Apidx < N;Apidx++){
	    num = Ap[Apidx+1] - Ap[Apidx];

	    for (int x = 0;x < N;x++){
	      if( x == Ai[idx] && num > 0) { fprintf( stderr, "%.4f  ", Ax[idx]); ++idx; --num;}
	      else               { fprintf( stderr, "*.****  " );                  }
	    } fprintf(stderr,"\n");
	}

	/*
	for( int y = 0; y<N; ++y)
	{
		for( int x = 0; x<N; ++x)
		{
			if( x == Ai[idx] ) { fprintf( stderr, "%.4f  ", Ax[idx]); ++idx; }
			else               { fprintf( stderr, "*.****  " );                  }
		}
		fprintf( stderr, "\n");
	}
	*/
//}

/*
void verify_inv( int N, const int *Ap , const int *Ai , const t_element *Ax, const t_element (*inv)[DIM] )
{
	int idx = 0;
	int base = 0;
	int Apidx = 0;
	int j;
	int num,cnt;
	t_element sum;
	static t_element c[DIM][DIM];
	t_element element;

	fprintf(stderr,"verification:\n");

	for (Apidx = 0;Apidx < N;Apidx++){
	  num = Ap[Apidx+1] - Ap[Apidx];

	  for (j = 0;j < N;j++) {
	    cnt = num;
	    //fprintf(stderr,"num: %d(%d,%d)\n",num,Apidx,idx);

	    for (int x = 0;x < N;x++){
	      if( x == Ai[idx] && cnt > 0) {
		element = Ax[idx];
		++idx; --cnt;
	      }
	      else {
		element = 0.0;
	      }
	      //fprintf(stderr,"%f ",element);
	      sum += element * inv[x][j];
	    }
	    idx -= num;
	    c[Apidx][j] = sum;
	    sum = 0.0;
	    //fprintf(stderr,"\n");
	  }
	  idx += num;
	  //fprintf(stderr,"\n");
	}

	for (int i = 0;i < N;i++) {
	  for (int j = 0;j < N;j++) {
	    fprintf(stderr,"%.3f ", c[i][j]);
	  }fprintf(stderr,"\n");
	}

}
*/

/*
void takapack_LU_factorization( const int N, const int *Ap , const int *Ai , const t_element *Ax, int* &LUp, int* &LUi, t_element* &LUx, int* &LU_rowFlip)
{
	typedef list<pair<int,t_element> >::iterator RowItr;

	//compressed row form用の LU分解
	LU_rowFlip = new int[ N ];
	for( int i=0;i<N; ++i) LU_rowFlip[i] = i;

	int nunZeroEntryNum  = 0;
	list<pair<int,t_element> > *Row = new list<pair<int,t_element> > [ N ];
	t_element   *Myi_tmp = new t_element[N]; //(これに変えて Axを縦方向につぶしたcompressed column行列を置いてもいいかも)
	t_element   *Myi     = new t_element[N]; //(これに変えて Axを縦方向につぶしたcompressed column行列を置いてもいいかも)

	for( int I = 0; I < N; ++I)// for each column(各列(縦)について)
	{
		//1)-----------I列 (縦)を全て集める : Myi -------------------------------------------------------------
#pragma omp parallel for
		for( int y=0; y < N; ++y){
			Myi_tmp[y] = 0;
			for( int kk=Ap[y]; kk < Ap[y+1] ; ++kk) if( Ai[kk] == I ){ Myi_tmp[y] = Ax[kk]; break;}
		}
		//row flipを適用
		for( int y=0; y<N; ++y) Myi[y] = Myi_tmp[ LU_rowFlip[y] ];

		//2)-----------Myiの値を計算 (この時Aiにアクセスしてはダメ (計算結果は入っていないから))---------------
		for( int y=0; y <=I; ++y) {
			t_element v = Myi[y];
			for( RowItr it = Row[y].begin(); it != Row[y].end() && it->first < y; ++it) v -= it->second * Myi[ it->first ];
			Myi[y] = v;
		}

		t_element big = fabs( Myi[I] ), tmp;
		int    piv = I;

		for( int y=I+1; y < N; ++y) {
			t_element v = Myi[y];
			for( RowItr it = Row[y].begin(); it != Row[y].end() && it->first < I; ++it) v -= it->second * Myi[ it->first ];
			Myi[y] = v;
			if( (tmp = fabs( Myi[y] )) > big ){ big = tmp;  piv = y;  }
		}

		//3)------------係数掛ける-----------------------
		t_element pivV = Myi[piv],  coef = 1.0 / pivV;
#pragma omp parallel for
		for( int y = I; y < N; ++y) Myi[y] *= coef;
		Myi[piv] = pivV;

		//4)------------軸選択(I<-->piv)----------------
		if( piv != I ) {
			std::swap( Myi       [I], Myi       [piv] );
			std::swap( Row       [I], Row       [piv] ); //listのflipは遅いかも
			std::swap( LU_rowFlip[I], LU_rowFlip[piv] );
		}

		//5) -----------値を代入 LU <-- Myi-------------
		for( int y = 0; y < N; ++y) if( Myi[y] != 0.0 ) {
			++nunZeroEntryNum;
			Row[y].push_back( make_pair( I, Myi[y] ) );
		}
	}

	//最後に Row を Compressed row formにまとめる
	LUp = new int   [ N+1 ];
	LUi = new int   [ nunZeroEntryNum ];
	LUx = new t_element[ nunZeroEntryNum ];

	int index = 0;
	for( int i = 0; i < N; ++i)
	{
		LUp[i] = index;
		for( list<pair<int,t_element> >::iterator it = Row[i].begin(); it != Row[i].end(); ++it)
		{
			LUi[index] = it->first;
			LUx[index] = it->second;
			++index;
		}
	}
	LUp[ N ] = index;
	/*
	for (int i = 0; i< 5; i++) {
		printf("LUp[%d] = %d\n", i, LUp[i]);
		printf("LUi[%d] = %d\n", i, LUi[i]);
		printf("LUx[%d] = %f\n", i, LUx[i]);
		printf("\n");
	}

	printf("lup[%d] = %d\n", N, LUp[N]);
	printf("lui[%d] = %d\n", nunZeroEntryNum-1, LUi[nunZeroEntryNum-1]);
	printf("lux[%d] = %f\n", nunZeroEntryNum-1, LUx[nunZeroEntryNum-1]);
	*/
	//分解後のCRS形式の結果を外部ファイルに出力

/*
	export_LU(LUp, LUi, LUx, nunZeroEntryNum, N+1);
	delete[] Myi ;
	delete[] Myi_tmp;
	delete[] Row ;
}
*/

/*
//solve Ax = b --> LUx = b  --> L(Ux=y)=b
void takapack_LU_solve( const int N, const int *LUp, const int *LUi, const t_element *LUx, const int *LU_rowFlip, const t_element *b, t_element *res)
{
	t_element *f_B = new t_element[N];//fliped B
	t_element *f_Y = new t_element[N];//fliped Y
	for( int i=0; i<N;++i) f_B[i] = b[ LU_rowFlip[i] ];

	//前進代入  L * f_Y = flipB --> f_Y (Lの対角成分は1であることに注意)
	for( int y = 0; y < N; ++y){
		t_element val = f_B[y];
		for( int kk=LUp[y]; kk < LUp[y+1] && LUi[kk] < y; ++kk) val -= LUx[ kk ] * f_Y[ LUi[kk] ];
		f_Y[y] = val;
	}

	//後退代入 solve U x = flipY (L-1 * flipB)
	for( int y = N-1; y>=0; --y){
		int centerIdx = 0;
		for( int kk = LUp[y] ; kk < LUp[y+1]; ++kk) if( LUi[kk] == y) { centerIdx = kk; break;} //対角部分に当たるindexを取得

		t_element val = f_Y[y];
		for( int kk = centerIdx + 1 ; kk < LUp[y+1]; ++kk) val -= LUx[kk] * res[ LUi[kk] ];
		res[y] = val / LUx[centerIdx];
	}
	delete[] f_B;
	delete[] f_Y;
}


void takapack_LU_free ( int *LUp, int *LUi, t_element *LUx, int *LU_rowFlip )
{
	delete[] LUp;
	delete[] LUi;
	delete[] LUx;
	delete[] LU_rowFlip;
}
*/

//
/*
void gencrs(t_element x1[DIM], t_element x2[DIM], t_element *b, t_element (*e)[DIM], t_element *sparseMatrix,
	    //const int *crsrow, const int *crscol, const t_element *crsval,
	    //crsData* crs_p,
	    int dim)
{
    //#ifdef DISP
    for (int i = 0;i < dim;i++) {
      t_element x =  (t_element)(rand() % 100);
      if(rand() % 100 < 50)x *= -1;
      b[i] = x;
    }
    for (int i = 0;i < dim;i++) {
      x1[i] = 0;
    }
    for (int i = 0;i < dim;i++) {
      x2[i] = 0;
    }

    for (int j = 0;j < dim;j++){
      for (int i = 0;i < dim;i++) {
	if(j == i){
	  e[j][i] = 1;
	}
	else{
	  e[j][i] = 0;
	}
      }
    }   //   fprintf(stdout,"};\n\n");
    //#endif


    t_element v = 0;
    int r = 0;
    int loopCnt = 0;
    //fprintf(stderr,"Size spMat = %lu -> CRS =%lu, loopCount = %d\n", sizeof(sparseMatrix), sizeof(crsData), loopCnt);

    return;
}
*/
/*!
 * 修正コレスキー分解(modified Cholesky decomposition)
 *  - 対称行列A(n×n)を下三角行列(L:Lower triangular matrix)と対角行列の積(LDL^T)に分解する
 *  - l_ii = 1とした場合
 *  - L: i > jの要素が非ゼロで対角成分は1
 * @param[in] A n×nの対称行列
 * @param[out] L 対角成分が1の下三角行列
 * @param[out] d 対角行列(対角成分のみ)
 * @param[in] n 行列の大きさ
 * @return 1:成功,0:失敗
 */
/*
int ModifiedCholeskyDecomp(const t_element *A, t_element (*L)[DIM], t_element *d, int n)
{
	if(n <= 0) return 0;

	d[0] = A[0*DIM+0];
	L[0][0] = 1.0;

	for(int i = 1; i < n; ++i){
		if(i % 100 == 0)fprintf(stderr,".");
		// i < k の場合
		for(int j = 0; j < i; ++j){
			t_element lld = A[i*DIM+j];
			//if(lld == .0)fprintf(stderr,"hoge\n");sleep(1);
			for(int k = 0; k < j; ++k){
				lld -= L[i][k]*L[j][k]*d[k];
			}

			L[i][j] = (1.0/d[j])*lld;
		}

		// i == k の場合
		t_element ld = A[i*DIM+i];
		for(int k = 0; k < i; ++k){
			ld -= L[i][k]*L[i][k]*d[k];
		}
		d[i] = ld;
		L[i][i] = 1.0;
	}

	fprintf(stderr,"LDLT Factorization Completed.\n");
	return 1;
}
*/
/*もともとのコレスキー分解 */
/*
int spModifiedCholeskyDecomp_sub(const int *Ap, const int *Ai, const t_element *A, t_element (*L)[DIM], t_element *d, int n)
{
    if(n <= 0) return 0;

    /***
	int idx = 0;
	int Apidx = 0;
	int num;
	  for (Apidx = 0;Apidx < n;Apidx++){
	    num = Ap[Apidx+1] - Ap[Apidx];

	    for (int x = 0;x < n;x++){
	      if( x == Ai[idx] && num > 0) { fprintf( stderr, "%.4f  ", A[idx]); ++idx; --num;}
	      else               { fprintf( stderr, "*.****  " );                  }
	    } fprintf(stderr,"\n");
	}
    ***/
/*
    d[0] = A[0];
    L[0][0] = 1.0;
    //    fprintf(stderr,"A[0] %f\n",A[0]);

    int idx = 0;
    int cidx;
    int num;
    t_element val;
    t_element lld;
    t_element ld;

    for (int i = 1;i < n;i++) {
		if(i % 100 == 0)fprintf(stderr,".");
		idx = Ap[i];//対角成分から先は見ないから毎回基底アドレスを代入しないといけない
		num = Ap[i+1] - Ap[i]; //この列ではいくつの要素を処理するか
		//fprintf(stderr,"%d\n",num);
		for (cidx = 0; cidx < i; cidx++) {
			if( cidx == Ai[idx] && num > 0)
			{
				lld = A[idx];
				++idx;
				--num;
			}
			else
				lld = 0.0;

			for (int k = 0;k < cidx;k++) {
				lld -= L[i][k]*L[cidx][k]*d[k];
			}
			L[i][cidx] = (1.0/d[cidx])*lld;
		}

		//if(cidx != i){fprintf(stderr,"%d %d\n",cidx,i);sleep(1);}
		//fprintf(stderr,"%d(%d) \n",num,i);
		//if(cidx == Ai[idx] )
		if(i == Ai[idx] )
			ld = A[idx];
		else
			ld = 0.0;
		for(int k = 0; k < i; ++k){
			//ld -= L[cidx][k]*L[cidx][k]*d[k];
			ld -= L[i][k]*L[i][k]*d[k];
		}
		//d[cidx] = ld;
		//L[cidx][cidx] = 1.0;
		d[i] = ld;
		L[i][i] = 1.0;
	}


	fprintf(stderr,"LDLT factorization(sp) Completed.\n");
	return 1;
}
*/

/* CRS形式に対応 */
int spModifiedCholeskyDecomp(const int *Ap, const int *Ai, const t_element *A, int *Lp, int *Li, t_element *Lx, t_element *d, int n)
{
    if(n <= 0) return 0;
    /***
	int idx = 0;
	int Apidx = 0;
	int num;
	  for (Apidx = 0;Apidx < n;Apidx++){
	    num = Ap[Apidx+1] - Ap[Apidx];

	    for (int x = 0;x < n;x++){
	      if( x == Ai[idx] && num > 0) { fprintf( stderr, "%.4f  ", A[idx]); ++idx; --num;}
	      else               { fprintf( stderr, "*.****  " );                  }
	    } fprintf(stderr,"\n");
	}
    ***/

    d[0] = A[0];
    //L[0][0] = 1.0;
	/*
    Lp = new int   [ n+1 ];
	Li = new int   [ n * n / 2 + n];
	Lx = new t_element[ n * n / 2 + n ];
*/
	Li[0] = 0;
	Lp[0] = 1;
	Lx[0] = 1.0;
    //    fprintf(stderr,"A[0] %f\n",A[0]);
    int idx = 0;
    int cidx;
    int num;
    t_element val;
    t_element lld;
    t_element ld;
    t_element tmp_ik;
    t_element tmp_cidxk;
	int lidx = 1;
	int cidx_ind = 0;
	int ik_ind = 0;
	int cidx_num = 0;
	int ik_num = 0;
	int count = 0;
	for (int i = 1; i < n; i++) {
		if(i % 100 == 0)fprintf(stderr,".");
		//printf("i = %d\n",i );
		idx = Ap[i];//対角成分から先は見ないから毎回基底アドレスを代入しないといけない
		num = Ap[i+1] - Ap[i]; //この列ではいくつの要素を処理するか
		//fprintf(stderr,"%d\n",num);
		for (cidx = 0; cidx < i; cidx++) {
			if( cidx == Ai[idx] && num > 0)
			{
				lld = A[idx];
				++idx;
				--num;
			}
			else
				lld = 0.;

			ik_ind = 0;
			cidx_ind = 0;
			ik_num = lidx - Lp[i-1];
			cidx_num = Lp[cidx] - Lp[cidx-1];
			for (int k = 0; k < cidx; k++) {
				count++;
				tmp_ik = 0; tmp_cidxk = 0;
				//lld -= L[i][k]*L[cidx][k]*d[k];
				if (Li[Lp[i-1] + ik_ind] == k && ik_num > 0) {
					tmp_ik = Lx[Lp[i-1] + ik_ind];
					ik_ind++;
					ik_num--;
				}
				if (Li[Lp[cidx-1] + cidx_ind] == k && cidx_num > 0) {
					/*	
					printf("lidx = %d\n", lidx);
						printf("cidx-1= %d\n", cidx-1);
						//printf("Lp[cidx-1] + cidx_ind = %d\n", Lp[cidx-1] + cidx_ind);
						printf("Lp[cidx-1] = %d,  cidx_ind = %d\n", Lp[cidx-1], cidx_ind);
						printf("k = %d\n", k);
					*/
					tmp_cidxk = Lx[Lp[cidx-1] + cidx_ind];
					cidx_ind++;
					cidx_num--;
				}

				lld -= tmp_ik*tmp_cidxk*d[k];
				/*
				if (i >= 5 && i < 7) {
					printf("i = %d, cidx = %d, k = %d, lld = %f\n", i, cidx, k, lld);
					printf("tmp_ik = %f, tmp_cidxk = %f\n", tmp_ik, tmp_cidxk);
				}
				*/
			}
			//L[i][cidx] = (1.0/d[cidx])*lld;
			if (lld != 0) {
			//if (fabs(lld) > 0.1) {
			//if (fabs(lld) != DBL_EPSILON) {
				//printf("lidx = %d\n", lidx);
				Lx[lidx] = (1.0/d[cidx])*lld;
				Li[lidx] = cidx;
				lidx++;
			}
		}
		//if(cidx != i){fprintf(stderr,"%d %d\n",cidx,i);sleep(1);}
		//fprintf(stderr,"%d(%d) \n",num,i);
		if(i == Ai[idx] )
			ld = A[idx];
		else
			ld = 0.0;
		ik_ind = 0;
		ik_num = lidx - Lp[i-1];
		for(int k = 0; k < i; ++k){
			//ld -= L[i][k]*L[i][k]*d[k];
			tmp_ik = 0;
			if (Li[Lp[i-1] + ik_ind] == k && ik_num > 0) {
				tmp_ik = Lx[Lp[i-1] + ik_ind];
				ik_ind++;
				ik_num--;
			}
			ld -= tmp_ik*tmp_ik*d[k];
		}
		d[i] = ld;
		//L[i][i] = 1.0;
		Lx[lidx] = 1;
		Li[lidx] = i;
		lidx++;
		Lp[i] = lidx;
	}
    /*
	for (int i = 0; i < 10; i++) {
		printf("Lx[%d] = %f\n", i, Lx[i]);
	}
	*/
	export_LU(Lp, Li, Lx, lidx, n);
	printf("count = %d\n", count);
	fprintf(stderr,"LDLT factorization(sp) Completed.\n");
	return 1;

}


/*!
 * 不完全コレスキー分解(incomplete Cholesky decomposition)
 *  - 対称行列A(n×n)を下三角行列(L:Lower triangular matrix)と対角行列の積(LDL^T)に分解する
 *  - l_ii = 1とした場合
 *  - L: i > jの要素が非ゼロで対角成分は1
 *  - 行列Aの値が0である要素に対応する部分を飛ばす
 * @param[in] A n×nの対称行列
 * @param[out] L 対角成分が1の下三角行列
 * @param[out] d 対角行列(対角成分のみ)
 * @param[in] n 行列の大きさ
 * @return 1:成功,0:失敗
 */
/*
int spIncompleteCholeskyDecomp(const int *Ap, const int *Ai, const t_element *A, t_element (*L)[DIM], t_element *d, int n)
{
    int idx = 0;
    int cidx;
    int num;
    t_element val;
    t_element lld;
    t_element ld;

    if(n <= 0) return 0;

    d[0] = A[0];
    L[0][0] = 1.0;

    for(int i = 1; i < n; ++i){
      if(i % 100 == 0)fprintf(stderr,".");
      idx = Ap[i];
      num = Ap[i+1] - Ap[i];
      for (cidx = 0;cidx < i;cidx++) {
	//lld = 0;
	if( cidx == Ai[idx] && num > 0)
	  {
	    lld = A[idx];
	    ++idx;
	    --num;
	    //if(fabs(lld) >= 1.0e-10){
	      for (int k = 0;k < cidx;k++) {
		lld -= L[i][k]*L[cidx][k]*d[k];
	      }
	      //if(fabs(A[idx]) >= 1.0e-10)
		L[i][cidx] = (1.0/d[cidx])*lld;
		//}
	  }
	//else continue;
      }

      if(cidx == Ai[idx] ){
	ld = A[idx];
	for(int k = 0; k < i; ++k){
	  ld -= L[cidx][k]*L[cidx][k]*d[k];
	}
	d[cidx] = ld;
	L[cidx][cidx] = 1.0;
      }

    }
    fprintf(stderr,"Incomplete Cholesky factorization(sp) Completed.\n");
    return 1;
    /*
    for (int i = 1;i < n;i++){
      // i < k の場合
      for(int j = 0; j < i; ++j){
            if(fabs(A[i*DIM+j]) < 1.0e-10) continue;

            t_element lld = A[i*DIM+j];

	      for(int k = 0; k < j; ++k){
                lld -= L[i][k]*L[j][k]*d[k];
	      }

            L[i][j] = (1.0/d[j])*lld;
        }

        // i == k の場合
        t_element ld = A[i*DIM+i];
        for(int k = 0; k < i; ++k){
            ld -= L[i][k]*L[i][k]*d[k];
        }
        d[i] = ld;
        L[i][i] = 1.0;
    }
    return 1;
    */

//}

/*
int IncompleteCholeskyDecomp(const t_element *A, t_element (*L)[DIM], t_element *d, int n)
{
    if(n <= 0) return 0;

    d[0] = A[0*DIM+0];
    L[0][0] = 1.0;

    for(int i = 1; i < n; ++i){
        // i < k の場合
      for(int j = 0; j < i; ++j){
	//if(fabs(A[i*DIM+j]) < 1.0e-10) continue;
	if(fabs(A[i*DIM+j]) == .0) continue;

            t_element lld = A[i*DIM+j];

	      for(int k = 0; k < j; ++k){
                lld -= L[i][k]*L[j][k]*d[k];
	      }

            L[i][j] = (1.0/d[j])*lld;
        }

        // i == k の場合
        t_element ld = A[i*DIM+i];
        for(int k = 0; k < i; ++k){
            ld -= L[i][k]*L[i][k]*d[k];
        }
        d[i] = ld;
        L[i][i] = 1.0;
    }

    return 1;
}
*/

void inputfile_mtx_crs(char *file, int *dim, int *nnz, int *crsrow, int *crscol, t_element *crsval)
{
	int ch, n;
	char word[64];
	int cnt = -1;
	int head = 0;

	int ridx = 0;
	int cidx = 0;
	t_element val;

	int num = 0;
	int prev_cidx = -1;
	//  int diff_cidx = 0;

	//static int ccs_num[DIM];

	//static int ctable[DIM][DIM];
	//static t_element vtable[DIM][DIM];
	//static t_element *vtable = (t_element *)malloc(DIM * DIM  * sizeof(t_element));
	//static int *ctable = (int *)malloc(DIM * DIM  * sizeof(int));
	//static int tbl_ptr[DIM];
	//static int *tbl_ptr = (int *)malloc(DIM * sizeof(int));
	int *arow = (int *)malloc(sizeof(int) * _SPCOL_SIZE);
	int *acol = (int *)malloc(sizeof(int) * _SPCOL_SIZE);
	double *aval = (double *)malloc(sizeof(double) * _SPCOL_SIZE);
	int idx = 0;
	init_read(file);

	while (skip_blanks() != EOF) {
		switch (ch = Getc()) {
		case '\%':
			if(head == 1){
				skip_line();
			}
			else {
				get_word(word);
				if(head == 0 && strcmp(word, "%MatrixMarket") != 0){
					parse_error("not MatrixMarket format");
					exit(1);
				}
				while((ch = Getc()) != '\n'){
					unGetc(ch);
					get_word(word);
					if(strcmp(word,"complex") == 0){
						fprintf(stderr,"error: complex matrix can not be handled.\n");
						exit(1);
					}
				}
				if(strcmp(word,"symmetric") == 0)symm = 1;
				head = 1;
			}
			break;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '-':
				unGetc(ch);

				for (n = 0;(ch = Getc()) != '\n'; n++) {
					unGetc(ch);
					get_word(word);
					///*
					if(cnt < 0){
						if(n == 0){
	    //fprintf(stderr,"dim %d\n",atoi(word));
							*dim = atoi(word);
						}
						if(n == 2){
							*nnz = atoi(word);
							//fprintf(stderr,"nnz %d\n",atoi(word));
							//cnt = 1;
						}
					}//*/
					else
					{
						if(n == 0){
							ridx = atoi(word)-1;
							arow[idx] = ridx;
							//fprintf(stderr,"ridx %d\n",ridx);
						}
						if(n == 1) {
							prev_cidx = cidx;
							cidx = atoi(word)-1;
							acol[idx] = cidx;
						}
						if(n == 2){
							val = atof(word);
							aval[idx] = val;
							idx++;
						}
					}
				}
				if(cnt >= 0){
					if(cidx != prev_cidx) {
					}

					//if(symm){

					//}
				}
				cnt++;
				/*
				if(cnt > 0) {
					//fprintf(stderr,"%d %d %f\n",ridx,cidx,val);
					//mat[ridx*DIM+cidx] = val;
					//ctable[ridx][tbl_ptr[ridx]] = cidx;
					ctable[ridx * DIM + tbl_ptr[ridx]] = cidx;
					//vtable[ridx][tbl_ptr[ridx]] = val;
					vtable[ridx * DIM + tbl_ptr[ridx]] = val;
					tbl_ptr[ridx]++;
					if(tbl_ptr[ridx] > DIM/16)exit(1);
					if(symm && (cidx != ridx) ) {
						//ctable[cidx][tbl_ptr[cidx]] = ridx;
						ctable[cidx * DIM + tbl_ptr[cidx]] = ridx;
						//vtable[cidx][tbl_ptr[cidx]] = val;
						vtable[cidx * DIM + tbl_ptr[cidx]] = val;
						tbl_ptr[cidx]++;
					}
				}
				*/
				break;
			default:
				skip_line();
				break;
		}
	}
	q_sort(arow, acol, aval, 0, *nnz-1);

	int row_idx = 0;
	int *aidx_list = (int *)malloc(sizeof(int) * DIM);
	int *tmp_acol = (int *)malloc(sizeof(int) * DIM);
	double *tmp_aval = (double *)malloc(sizeof(double) * DIM);

	arr_init(aidx_list, DIM);
	arr_init(tmp_acol, DIM);
	arr_init(tmp_aval, DIM);
	int list_idx = 0;
	for (int i = 0; i < *nnz; i++ ) {
		if (arow[i] != row_idx) {
			q_sort(tmp_acol, tmp_aval, 0, list_idx - 1);
			for (int j = 0; j < list_idx; j++) {
				acol[aidx_list[j]] = tmp_acol[j];
				aval[aidx_list[j]] = tmp_aval[j];
			}
			list_idx = 0;
			arr_init(aidx_list, DIM);
			arr_init(tmp_acol, DIM);
			arr_init(tmp_aval, DIM);
			row_idx = arow[i];

			aidx_list[list_idx] = i;
			tmp_acol[list_idx] = acol[i];
			tmp_aval[list_idx] = aval[i];
			list_idx++;
		} else if (i == (*nnz)-1) {
			aidx_list[list_idx] = i;
			tmp_acol[list_idx] = acol[i];
			tmp_aval[list_idx] = aval[i];
			list_idx++;
			q_sort(tmp_acol, tmp_aval, 0, list_idx - 1);
			for (int j = 0; j < list_idx; j++) {
				acol[aidx_list[j]] = tmp_acol[j];
				aval[aidx_list[j]] = tmp_aval[j];
			}
		} else {
			aidx_list[list_idx] = i;
			tmp_acol[list_idx] = acol[i];
			tmp_aval[list_idx] = aval[i];
			list_idx++;
		}
	}

	idx = 0;
	int dim_ind = 1;
	/*
	for (int i = 0;i < *dim;i++) {
		crsrow[i] = idx;
		//fprintf(stderr,"%d(%d)\n",crsrow[i]-crsrow[i-1],idx);//sleep(1);
		for (int j = 0;j < tbl_ptr[i];j++){
			//crscol[idx] = ctable[i][j];
			//crsval[idx] = vtable[i][j];
			crscol[idx] = ctable[i * DIM + j];
			crsval[idx] = vtable[i * DIM + j];
			idx++;
		}
	}
	*/
	for (int i = 0; i < *nnz; i++) {
		if (i > 0) {
			if (arow[i] > arow[i-1]) {
				crsrow[dim_ind++] = idx;
			}
		}
		crscol[i] = acol[i];
		crsval[i] = aval[i];
		idx++;
	}
	//if(symm) {idx/=2;}
	//  if(idx != *nnz){
	//fprintf(stderr,"making CRS error(%d %d)\n",*nnz,idx);exit(1);
	//}
	crsrow[0] = 1;
	crsrow[*dim] = idx;
/*
	free(ctable);
	free(vtable);
	free(tbl_ptr);
*/
	free(arow);
	free(acol);
	free(aval);
	free(aidx_list);
	free(tmp_acol);
	free(tmp_aval);
}
/*
void inputfile_mtx_ccs(char *file, int *dim, int *nnz, int *ccsrow, int *ccscol, t_element *ccsval)
{
  int ch, n;
  char word[64];
  int cnt = -1;
  int head = 0;

  int ridx = 0;
  int cidx = 0;
  t_element val;

  int num = 0;
  int prev_cidx = -1;
  //  int diff_cidx = 0;

  //static int ccs_num[DIM];

  init_read(file);

  while (skip_blanks() != EOF) {
    switch (ch = Getc()) {
    case '\%':
      if(head == 1){
	skip_line();
      }
      else {
	get_word(word);
	if(head == 0 && strcmp(word, "%MatrixMarket") != 0){
	  parse_error("not MatrixMarket format");
	  exit(1);
	}
	while((ch = Getc()) != '\n'){
	  unGetc(ch);
	  get_word(word);
	  if(strcmp(word,"complex") == 0){
	    fprintf(stderr,"error: complex matrix can not be handled.\n");
	    exit(1);
	  }
	}
	if(strcmp(word,"symmetric") == 0)symm = 1;
	head = 1;
      }
      break;
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '-':
      unGetc(ch);

      for (n = 0;(ch = Getc()) != '\n'; n++) {
	unGetc(ch);
	get_word(word);
	///*
	if(cnt < 0){
	  if(n == 0){
	    //fprintf(stderr,"dim %d\n",atoi(word));
	    *dim = atoi(word);
	  }
	  if(n == 2){
	    *nnz = atoi(word);
	    ccscol[*dim] = *nnz;
	    //fprintf(stderr,"nnz %d\n",atoi(word));
	    //cnt = 1;
	  }
	}//*/
/*
	else
	  {
	    if(n == 0){
	      ridx = atoi(word)-1;
	      //fprintf(stderr,"ridx %d\n",ridx);
	    }
	    if(n == 1) {
	      prev_cidx = cidx;
	      cidx = atoi(word)-1;
	    }
	    if(n == 2){
	      val = atof(word);
	    }
	  }
      }
      cnt++;
      if(cnt >= 0){
	if(cidx != prev_cidx) {
	  //ccscol[cidx] = cnt;
	  //fprintf(stderr,"%d\n",ccscol[cidx]);
	}
	//ccsrow[cnt] = ridx;
	//ccsval[cnt] = val;

	//if(symm){

	//}
      }
      if(cnt > 0) {
	//fprintf(stderr,"%d %d %f(%d)\n",ridx,cidx,val,cnt);
	if(cidx == 0 && prev_cidx != cidx)ccscol[cidx] = 0;
	else if (prev_cidx != cidx) ccscol[cidx] = cnt-1;
	ccsrow[cnt-1] = ridx;
	ccsval[cnt-1] = val;
      }
      break;
    default:
      skip_line();
      break;
    }
  }
}
*/
/*
void inputfile_mtx(char *file, t_element *mat, int *dim, int *nnz)
{
  int ch, n;
  char word[64];
  int cnt = -1;
  int head = 0;

  int ridx,cidx;
  t_element val;

  init_read(file);

  while (skip_blanks() != EOF) {
    switch (ch = Getc()) {
    case '\%':
      if(head == 1){
	skip_line();
      }
      else {
	get_word(word);
	//fprintf(stderr,"%s\n",word);
	if(head == 0 && strcmp(word, "%MatrixMarket") != 0){
	  parse_error("not MatrixMarket format");
	  exit(1);
	}
	while((ch = Getc()) != '\n'){
	  unGetc(ch);
	  get_word(word);
	  if(strcmp(word,"complex") == 0){
	    fprintf(stderr,"error: complex matrix can not be handled.\n");
	    exit(1);
	  }
	}
	if(strcmp(word,"symmetric") == 0)symm = 1;
	//	if(strcmp(word,"complex") == 0)symm = 1;
	//fprintf(stderr,"%d\n",symm);
	//fprintf(stderr,"%s\n",word);
	head = 1;
      }
      break;
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '-':
      unGetc(ch);

      /*
      if(cnt == 0){
	cnt = 1;
	//最初は行列サイズと非ゼロ要素の数//なのでスキップ
	//unGetc(ch);
	//get_word(word);
	//fprintf(stderr,"%d\n", atoi(word));sleep(1);
	skip_line();
      }
      */
/*
      //for (n = 0;n < 3;n++) {
      for (n = 0;(ch = Getc()) != '\n'; n++) {
	unGetc(ch);
	get_word(word);
	///*
	if(cnt == -1){
	  if(n == 0){
	    //fprintf(stderr,"dim %d\n",atoi(word));
	    *dim = atoi(word);
	  }
	  if(n == 2){
	    *nnz = atoi(word);
	    //fprintf(stderr,"nnz %d\n",atoi(word));
	    //cnt = 1;
	  }
	}//*/
/*	else
	  {
	    if(n == 0){
	      ridx = atoi(word)-1;
	      //fprintf(stderr,"ridx %d\n",ridx);
	    }
	    if(n == 1)
	      cidx = atoi(word)-1;
	    if(n == 2){
	      val = atof(word);
	      //cnt++;
	    }
	  }
      }
      cnt++;
      if(cnt > 0) {
	//fprintf(stderr,"%d %d %f\n",ridx,cidx,val);
	mat[ridx*DIM+cidx] = val;
	if(symm) mat[cidx*DIM+ridx] = val;
      }
      //skip_line();
      break;
    default:
      skip_line();
      break;
    }
  }
}
*/
int main(int argc, char *argv[])
{
    int    *LUp, *LUi, *LU_rowflip;
    t_element *LUx;

    static t_element *x1 = (t_element *)malloc(DIM * sizeof(t_element));
    //static t_element x1[DIM];
    //static t_element x2[DIM];
    static t_element *x2 = (t_element *)malloc(DIM * sizeof(t_element));
    //static t_element d[DIM];
    //static t_element b[DIM];
    static t_element *b = (t_element *)malloc(DIM * sizeof(t_element));
    //static t_element e[DIM][DIM];
    //static t_element f[DIM][DIM];
    static int *crsrow = (int *)malloc(sizeof(int) * _SPROW_SIZE);
    static int *crscol = (int *)malloc(sizeof(int) * _SPCOL_SIZE);
    static t_element *crsval = (t_element *)malloc(sizeof(t_element) * _SPCOL_SIZE);
    //static crsData crs;
    //static t_element *sparseMatrix = (t_element *)malloc(sizeof(t_element)*DIM*DIM);
    //static t_element e1[DIM][DIM];
    int dim;
    int nnz;

    int rseed = getpid();
    int method = 0;
    int regen = 0;
    /*
	Lp = new int   [ n+1 ];
	Li = new int   [ n * n / 2 + n];
	Lx = new t_element[ n * n / 2 + n ];
	*/
    //static t_element L[DIM][DIM];
	int *Lp = (int *)malloc((DIM + 1) * sizeof(int));
	int *Li = (int *)malloc((_OUT_SPCOL_SIZE) * sizeof(int));
	t_element *Lx = (t_element *)malloc((_OUT_SPCOL_SIZE) * sizeof(t_element));

	for (int i = 0; i < DIM+1; i++) {
		Lp[i] = 0;
	}

	for (int i = 0; i < _OUT_SPCOL_SIZE; i++) {
		Li[i] = 0;
		Lx[i] = 0;
	}
    for (int i = 1; i < argc; i++) {
    	switch (argv[i][0]) {
    		case '-':
    		switch (argv[i][1]) {
    			case 's':
    				rseed = atoi(argv[++i]);
    				break;
    			//case ''
    			case 'm':
    				method = atoi(argv[++i]);//0: 修正コレスキー分解、1:不完全コレスキー分解 2: LU分解
    				break;
    			case 'r':
    				regen = 1;
    				break;
    		}
    		break;
    	default:
    		srand(rseed);

	/***
	static int ccsrow[DIM*DIM];
	static int ccscol[DIM + 1];
	static t_element ccsval[DIM*DIM];
	inputfile_mtx_ccs(argv[i],&dim,&nnz, ccsrow, ccscol, ccsval);

	for (int ii = 0;ii < dim;ii++) {
	  int num = ccscol[ii+1] - ccscol[ii];
	  int idx = 0;
	  fprintf(stderr,"%d\n",num);sleep(1);
	  for (int j = 0;j < dim;j++) {
	    if(ccsrow[ccscol[ii]+idx] == j && num){
	      fprintf(stderr,"%f ",ccsval[ccscol[ii]+idx]);
	      idx++;
	      if(num > 0)num--;
	    }
	    else fprintf(stderr,"%f ",0.0);
	  }fprintf(stderr,"\n");
	}
	//exit(1);
	***/
    		if (method == SPMCH || method == SPINCH || method == LU) {
    			inputfile_mtx_crs(argv[i], &dim, &nnz, crsrow, crscol, crsval);//&crs);//, crs.row, crs.col, crs.val);
    		} else {
    			//inputfile_mtx(argv[i],sparseMatrix,&dim,&nnz);
    		}
    		//exit(1);
    		///*
    		int idx = 0;
#ifdef DISP
    		t_element ld;
    		for (int ii = 0;ii < dim;ii++) {
    			int num = crsrow[ii+1] - crsrow[ii];
    			//fprintf(stderr,"%d(%d)\n",num,ccscol[i+1]);//sleep(1);
    			for (int j = 0;j < dim;j++) {
    				if(crscol[idx] == j && num){
    					ld = crsval[idx];
    					fprintf(stderr,"%.3f ",crsval[idx]);
    					idx++;
    					if(num > 0)num--;
    				}
    				else {
    					fprintf(stderr,"%.3f ",0.0);
    					ld = 0;
    				}
    				//if(fabs(ld - sparseMatrix[i*DIM+j]) != 0.0){
    				//fprintf(stderr,"%d %d\n",i,j);
    				//exit(1);
    				//}
    			}fprintf(stderr,"\n");
    		}
    		//*/
    		fprintf(stderr,"\n");
    		for (int i = 0;i < dim;i++) {
    			for (int j = 0;j < dim;j++) {
    				fprintf(stderr,"%.3f ",sparseMatrix[i*DIM+j]);
    			}fprintf(stderr,"\n");
    		}
    		//exit(1);
#endif
    		//fprintf(stderr,"%d %d\n",dim,nnz);
/*
    		for (int i = 0;i < dim;i++) {
    			e[i][i] = 1.0;
    		}

    		if(symm != 1 && (method != LU)){
    			fprintf(stderr,"warning: selected algorithm is MCH or INCH but inputted matrix is asymmetric.");
    			//exit(1);
    		}

    		if(method == MCH){
    			ModifiedCholeskyDecomp(sparseMatrix, L, x1, dim);
    		}*/
    		//else if (method == SPMCH) {
        	if (method == SPMCH) {
    			//gencrs(x1, x2, b, e, sparseMatrix, dim);
    			//if(regen != 1)free(sparseMatrix);
    			/*
				spModifiedCholeskyDecomp_sub(crsrow, crscol, crsval, L, x1, dim);

				char *fileldlt = "testldlt.dat";
				ofstream writing_ldlt;
				writing_ldlt.open(fileldlt, std::ios::out);

				for (int i = 0;i < dim; i++) {
					for (int j = 0; j <= i; j++) {
						if (L[i][j] != 0)
							writing_ldlt << fixed << setprecision(15) << L[i][j] << endl;
					}
				}
    			*/

    			spModifiedCholeskyDecomp(crsrow, crscol, crsval, Lp, Li, Lx, x1, dim);

    		}
        	/*
    		else if(method == INCH) {
    			IncompleteCholeskyDecomp(sparseMatrix, L, x1, dim);
    		}
    		else if(method == SPINCH) {
    			//gencrs(x1, x2, b, e, sparseMatrix, &crs, dim);
    			//if(regen != 1)free(sparseMatrix);
    			spIncompleteCholeskyDecomp(crsrow, crscol, crsval, L, x1, dim);
    		}
    		else if(method == LU){
    			//gencrs(x1, x2, b, e, sparseMatrix, &crs, dim);
    			takapack_test(crsrow, crscol, crsval, x1, x2, b, e, dim);
    			//    takapack_LU_factorization( N, Ap, Ai, Ax, LUp, LUi, LUx, LU_rowflip);
    			//    takapack_LU_solve        ( N, LUp, LUi, LUx, LU_rowflip, b, x1 );
    			//        fprintf( stderr, "%f %f %f %f %f\n", x1[0], x1[1], x1[2], x1[3], x1[4]);//適当にprintfしてください...
    			//    takapack_LU_free( LUp, LUi, LUx, LU_rowflip);
    		}


    		if(regen == 1 && (method == MCH || method == SPMCH || method == INCH || method == SPINCH)) {

    			for (int i = 0;i < dim;i++) {
    				if(i % 100 == 0)fprintf(stderr,".");
    				for (int k = 0;k < dim;k++){
    					for (int j = 0;j < dim;j++) {
    						if(k == j)
    							f[i][j] += L[i][k]*x1[j];
    						//else
    						//f[i][j] += 0.0;
    					}
    				}
    			}fprintf(stderr,"\n");

    			//int e1[dim][dim];

    			for (int i = 0;i < dim;i++) {
    				if(i % 100 == 0)fprintf(stderr,".");
    				for (int k = 0;k < dim;k++){
    					for (int j = 0;j < dim;j++) {
    						if(f[i][k] == 0.0)continue;
    						e1[i][j] += f[i][k]*L[j][k];
    					}
    				}//fprintf(stderr,"\n");
    			}fprintf(stderr,"\n");
    			fprintf(stderr,"regeneration completed.\n");


#ifndef SHOW_LDLT

    			fprintf(stderr,"regenerated matrix:\n");
    			for (int i = 0;i < dim;i++) {
    				for (int j = 0;j < dim;j++) {
    					fprintf(stderr,"%f ",e1[i][j]);
    				}fprintf(stderr,"\n");
    			}
    			fprintf(stderr,"\n");

    			if(method == SPMCH || method == SPINCH){
    				///***
    				//int
    				idx = 0;
    				t_element lld;
    				for (int ii = 0;ii < dim;ii++) {
    					int num = crsrow[ii+1] - crsrow[ii];
    					//int idx = crs.row[ii];
    					//fprintf(stderr,"%d(%d)\n",num,ccscol[i+1]);//sleep(1);


    					for (int j = 0;j < dim;j++) {
    						if(crscol[idx] == j && num){
    							lld = crsval[idx];
    							//fprintf(stderr,"%f ", lld);
    							idx++;
    							if(num > 0)num--;
    						}
    						else {
    							lld = 0.0;
    							//fprintf(stderr,"%f ", lld);
    						}
    						if(fabs(e1[ii][j] - lld) > 0.1)
    							fprintf(stderr,"large diff(%f %f) at (%d %d)\n",e1[ii][j], lld,ii,j);
    					}fprintf(stderr,"\n");
    				}fprintf(stderr,"SP\n");
    				//***/
        	/*
    			}
    			if(method == MCH || method == INCH){
    				for (int i = 0;i < dim;i++) {
    					for (int j = 0;j < dim;j++){
    						//fprintf(stderr,"%f ",sparseMatrix[i*DIM+j]);
    						if(fabs(e1[i][j] - sparseMatrix[i*DIM+j]) > 0.1)
    							fprintf(stderr,"large diff(%f %f) at (%d %d)\n",e1[i][j], sparseMatrix[i*DIM+j],i,j);
    					}fprintf(stderr,"\n");
    				}
    			}
#endif
    			//*/
    			//return 0;
    			//goto final;
    		//}


    		final:
			//free(sparseMatrix);
			free(x1);
			free(x2);
			free(Li);
			free(Lp);
			free(Lx);
			free(b);
			free(crsrow);
			free(crscol);
			free(crsval);
			return 0;
    	}
    }
}
