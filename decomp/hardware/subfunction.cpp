#include "takapack.h"

char buf[BUF_SIZE+256];
int n_chars_in_buf = 0;
int buf_pos = 0;
int fd;

int Getc()
{
    int ch;

    if (buf_pos == n_chars_in_buf) {
	n_chars_in_buf = read(fd, &buf[128], BUF_SIZE);
	if (n_chars_in_buf == 0)
	    return EOF;
	n_chars_in_buf += 128;
	buf_pos = 128;
    }
    ch = buf[buf_pos++];
    return ch;
}

void unGetc(int ch)
{
    buf[--buf_pos] = ch;
}

void parse_error(char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

void skip_line()
{
    int ch;

    while ((ch = Getc()) != '\n' && ch != EOF);
}

int skip_blanks()
{
    int ch;

    while ((ch = Getc()) == ' ' || ch == '\t');
    unGetc(ch);
    return ch;
}

void get_word(char word[])
{
    int ch, i = 0;

    while ((ch = Getc()) == ' ' || ch == '\t');

    if (ch != ' ' && ch != '\t' && ch != '\n' && ch != EOF) {
	word[i++] = ch;
	while ((ch = Getc()) != ' ' && ch != '\t' && ch != '\n' && ch != EOF)
	    word[i++] = ch;
    }

    word[i] = '\0';
    unGetc(ch);

    return;

    skip_blanks();
    while ((ch = Getc()) == ' ' || ch == '\t');
    unGetc(ch);

    while ((ch = Getc()) != ' ' && ch != '\t' && ch != '\n' && ch != EOF)
	word[i++] = ch;

    word[i] = '\0';
    unGetc(ch);
}

void init_read(char *file)
{
    if ((fd = open(file, O_RDONLY)) < 0) {
	fprintf(stderr, "can't open %s\n", file);
	exit(1);
    }
    n_chars_in_buf = 128;
    buf_pos = 128;
}

void export_LU(int *lup, int *lui, t_element *lux, int nunZeroEntryNum, int matsize)
{
	char *filelup = "testlup.dat";
	char *filelui = "testlui.dat";
	char *filelux = "testlux.dat";

	printf("size = %d\n", matsize);
	printf("nunZeroEntryNum = %d\n", nunZeroEntryNum);

	ofstream writing_lup;
	ofstream writing_lui;
	ofstream writing_lux;

	writing_lup.open(filelup, std::ios::out);
	writing_lui.open(filelui, std::ios::out);
	writing_lux.open(filelux, std::ios::out);

	for (int i = 0; i < nunZeroEntryNum; i++) {
		writing_lui << lui[i] << endl;
		writing_lux << lux[i] << endl;
	}
	for (int i = 0; i < matsize; i++) {
		writing_lup << fixed << setprecision(.15) << lup[i] << endl;
	}
}

void swap(t_element *array, int i, int j) {
	t_element tmp;
	tmp = array[i];
	array[i] = array[j];
	array[j] = tmp;
}

void swap(int *array, int i, int j) {
	int tmp;
	tmp = array[i];
	array[i] = array[j];
	array[j] = tmp;
}

void q_sort(int *numbers, int *numbers2, t_element *numbers3, int left, int right) {
	int pivot, l_hold, r_hold;

	l_hold = left;
	r_hold = right;
	pivot = numbers[(left + right)/2];
	while (1) {
		while (numbers[l_hold] < pivot)
			l_hold++;
		while ((numbers[r_hold] > pivot))
			r_hold--;
		if (l_hold >= r_hold) break;
				swap(numbers,  l_hold, r_hold);
				swap(numbers2, l_hold, r_hold);
				swap(numbers3, l_hold, r_hold);
				l_hold++;
				r_hold--;
	}
	if (left < l_hold - 1)
		q_sort(numbers, numbers2, numbers3, left, l_hold-1);
	if (right > r_hold + 1)
		q_sort(numbers, numbers2, numbers3, r_hold+1, right);
}

void q_sort(int *numbers, t_element *numbers2, int left, int right) {
	int pivot, l_hold, r_hold;

	l_hold = left;
	r_hold = right;
	pivot = numbers[(left + right)/2];
	while (1) {
		while (numbers[l_hold] < pivot)
			l_hold++;
		while ((numbers[r_hold] > pivot))
			r_hold--;
		if (l_hold >= r_hold) break;
				swap(numbers,  l_hold, r_hold);
				swap(numbers2, l_hold, r_hold);
				l_hold++;
				r_hold--;
	}
	if (left < l_hold - 1)
		q_sort(numbers, numbers2, left, l_hold-1);
	if (right > r_hold + 1)
		q_sort(numbers, numbers2, r_hold+1, right);
}

void arr_init(int *arr, int size) {
	for (int i = 0; i < size; i++) {
		arr[i] = 99999999;
	}
}

void arr_init(t_element *arr, int size) {
	for (int i = 0; i < size; i++) {
		arr[i] = 99999999;
	}
}
