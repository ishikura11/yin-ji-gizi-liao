#include "takapack.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


int spModifiedCholeskyDecomp(const int *Ap, const int *Ai, const t_element *A, int *Lp, int *Li, t_element *Lx, t_element *d, int n)
{
    if(n <= 0) return 0;
    /***
	int idx = 0;
	int Apidx = 0;
	int num;
	  for (Apidx = 0;Apidx < n;Apidx++){
	    num = Ap[Apidx+1] - Ap[Apidx];

	    for (int x = 0;x < n;x++){
	      if( x == Ai[idx] && num > 0) { fprintf( stderr, "%.4f  ", A[idx]); ++idx; --num;}
	      else               { fprintf( stderr, "*.****  " );                  }
	    } fprintf(stderr,"\n");
	}
    ***/

    d[0] = A[0];
    //L[0][0] = 1.0;
	/*
    Lp = new int   [ n+1 ];
	Li = new int   [ n * n / 2 + n];
	Lx = new t_element[ n * n / 2 + n ];
*/
	Li[0] = 0;
	Lp[0] = 1;
	Lx[0] = 1.0;
    //    fprintf(stderr,"A[0] %f\n",A[0]);
    int idx = 0;
    int cidx;
    int num;
    t_element val;
    t_element lld;
    t_element ld;
    t_element tmp_ik;
    t_element tmp_cidxk;
	int lidx = 1;
	int cidx_ind = 0;
	int ik_ind = 0;
	int cidx_num = 0;
	int ik_num = 0;
	int count = 0;
	int ik_num_max = 0;
	int lld_count = 0;
	//int lld_count_max = 0;
	//int lld_total = 0;
	t_element tmp_lld[DIM];
	t_element tmp_lld2[DIM/SUBDIV_DIM];
	
	for (int i = 1; i < n; i++) {
		if(i % 100 == 0)fprintf(stderr,".");
		//printf("i = %d\n",i );
		idx = Ap[i];//対角成分から先は見ないから毎回基底アドレスを代入しないといけない
		num = Ap[i+1] - Ap[i]; //この列ではいくつの要素を処理するか
		//fprintf(stderr,"%d\n",num);
		for (cidx = 0; cidx < i; cidx++) {
			if( cidx == Ai[idx] && num > 0)
			{
				lld = A[idx];
				++idx;
				--num;
			}
			else
				lld = 0.;

			ik_ind = 0;
			cidx_ind = 0;
			ik_num = lidx - Lp[i-1];
			cidx_num = Lp[cidx] - Lp[cidx-1];
			count = 0;
			
			if (ik_num > ik_num_max) ik_num_max = ik_num;
			for (int k = 0; k < DIM;k++) {
				tmp_lld[k] = 0;
			}
			lld_count = 0;
			
			for (int k = 0; k < cidx; k++) {
#pragma HLS pipeline
				count++;
				tmp_ik = 0; tmp_cidxk = 0;
				//lld -= L[i][k]*L[cidx][k]*d[k];
				if (k > Li[Lp[i-1] + ik_num] || k > Li[Lp[cidx-1] + cidx_num]) {
					tmp_lld[k] = 0;
				} else {
					for (int j = 0; j < ik_num; j++) {
						if (Li[Lp[i-1] + j] == k) {
							tmp_ik == Lx[Lp[i-1] + j];
							ik_ind++;
							break;
						}
					}
					for (int j = 0; j < cidx_num; j++) {
						if (Li[Lp[cidx-1] + j] == k) {
							tmp_cidxk = Lx[Lp[cidx-1] + j];
							cidx_ind++;
							break;
						}
					}
					tmp_lld[k] = tmp_ik * tmp_cidxk * d[k];
				}
			}
			if (ik_ind != ik_num) printf("i = %d, cidx = %d, ik_ind = %d, ik_num = %d\n", i, cidx, ik_ind, ik_num);
			if (cidx_ind != cidx_num) printf("i = %d, cidx = %d, cidx_ind = %d, cidx_num = %d\n", i, cidx, cidx_ind, cidx_num);
			/*
			for (int k = 0; k < cidx; k++) {
				count++;
				tmp_ik = 0; tmp_cidxk = 0;
				//lld -= L[i][k]*L[cidx][k]*d[k];
				if (Li[Lp[i-1] + ik_ind] == k && ik_num > 0) {
					tmp_ik = Lx[Lp[i-1] + ik_ind];
					ik_ind++;
					ik_num--;
				}
				if (Li[Lp[cidx-1] + cidx_ind] == k && cidx_num > 0) {
					tmp_cidxk = Lx[Lp[cidx-1] + cidx_ind];
					cidx_ind++;
					cidx_num--;
				}
				tmp_lld[lld_count] = tmp_ik*tmp_cidxk*d[k];
				if (tmp_lld[lld_count] != 0) lld_count++;

				//lld -= tmp_ik*tmp_cidxk*d[k];
			}
			*/
			//if (lld_count > lld_count_max) lld_count_max= lld_count;
			//lld_total += lld_count;


			for (int j = 0; j < DIM/SUBDIV_DIM; j++) {
				for (int k = 0; k < SUBDIV_DIM; k+=2) { 
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+1 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=4) { 
					//tmp_lld[k] += tmp_lld[k+2];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+2 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=8) { 
					//tmp_lld[k] += tmp_lld[k+4];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+4 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=16) { 
					//tmp_lld[k] += tmp_lld[k+8];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+8 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=32) { 
					//tmp_lld[k] += tmp_lld[k+16];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+16 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=64) { 
					//tmp_lld[k] += tmp_lld[k+32];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+32 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=128) { 
					//tmp_lld[k] += tmp_lld[k+64];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+64 + j*SUBDIV_DIM];
				}
				/*
				for (int k = 0; k < SUBDIV_DIM; k+=256) { 
					tmp_lld[k] += tmp_lld[k+128];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=512) { 
					tmp_lld[k] += tmp_lld[k+256];
				}
				*/
				tmp_lld2[j] = tmp_lld[j*SUBDIV_DIM] + tmp_lld[128 + j*SUBDIV_DIM];
			}

			for (int k = 0; k < DIM/SUBDIV_DIM; k+=2) { 
				tmp_lld2[k] += tmp_lld2[k+1];
			}

			for (int k = 0; k < DIM/SUBDIV_DIM; k+=4) { 
				tmp_lld2[k] += tmp_lld2[k+2];
			}

			for (int k = 0; k < DIM/SUBDIV_DIM; k+=8) { 
				tmp_lld2[k] += tmp_lld2[k+4];
			}
			for (int k = 0; k < DIM/SUBDIV_DIM; k+=16) { 
				tmp_lld2[k] += tmp_lld2[k+8];
			}

			lld -= tmp_lld2[0] + tmp_lld2[16];
			
			//printf("count = %d\n", count);
			//L[i][cidx] = (1.0/d[cidx])*lld;
			if (lld != 0) {
			//if (fabs(lld) > 0.1) {
			//if (fabs(lld) != DBL_EPSILON) {
				//printf("lidx = %d\n", lidx);
				Lx[lidx] = (1.0/d[cidx])*lld;
				Li[lidx] = cidx;
				lidx++;
			}
		}
		//if(cidx != i){fprintf(stderr,"%d %d\n",cidx,i);sleep(1);}
		//fprintf(stderr,"%d(%d) \n",num,i);
		if(i == Ai[idx] )
			ld = A[idx];
		else
			ld = 0.0;
		ik_ind = 0;
		ik_num = lidx - Lp[i-1];
		for(int k = 0; k < i; ++k){
			//ld -= L[i][k]*L[i][k]*d[k];
			tmp_ik = 0;
			if (Li[Lp[i-1] + ik_ind] == k && ik_num > 0) {
				tmp_ik = Lx[Lp[i-1] + ik_ind];
				ik_ind++;
				ik_num--;
			}
			ld -= tmp_ik*tmp_ik*d[k];
		}
		d[i] = ld;
		//L[i][i] = 1.0;
		Lx[lidx] = 1;
		Li[lidx] = i;
		lidx++;
		Lp[i] = lidx;
	}
    /*
	for (int i = 0; i < 10; i++) {
		printf("Lx[%d] = %f\n", i, Lx[i]);
	}
	*/

	//printf("lld_count avg:%d, max:%d \n", lld_total/n, lld_count_max);
	printf("ik_num avg:%d, max:%d \n", lidx/n, ik_num_max);
	
	export_LU(Lp, Li, Lx, lidx, n);
	fprintf(stderr,"LDLT factorization(sp) Completed.\n");
	return 1;

}


