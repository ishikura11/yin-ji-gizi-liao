%author: Jiaolong Yang
function [ flow, flows ] = jly_TVL1Flow( I1, I2, param, flow_init, px1, px2, py1, py2)

factor = sqrt(2);  % sqrt(3) worse
smooth_sigma = sqrt(param.pyramid_spacing)/factor;   % or sqrt(3) recommended by Manuel Werlberger 
f = fspecial('gaussian', 2*round(1.5*smooth_sigma) +1, smooth_sigma);

I1s = cell(1,param.pyramid_levels);
I2s = cell(1,param.pyramid_levels);
I1s{1} = I1;
I2s{1} = I2;
for l = 2:param.pyramid_levels
    I1 = imfilter(I1, f, 'corr', 'symmetric', 'same');     
    I2 = imfilter(I2, f, 'corr', 'symmetric', 'same');
    I1 = imresize(I1,1/param.pyramid_spacing,'bicubic');
    I2 = imresize(I2,1/param.pyramid_spacing,'bicubic');
    
    I1s{l} = I1;
    I2s{l} = I2;
end

flows = cell(1,param.pyramid_levels);
for l = param.pyramid_levels:-1:1
    
    fprintf(['Level ' num2str(l) ': ']);
    I1 = I1s{l};
    I2 = I2s{l};
    
    h = size(I1,1);
    w = size(I1,2);
    
    if l < param.pyramid_levels
        flow_init_1 = imresize(flows{l+1}(:,:,1)*param.pyramid_spacing, [h,w], 'bicubic');
        flow_init_2 = imresize(flows{l+1}(:,:,2)*param.pyramid_spacing, [h,w], 'bicubic');
        flow_init = cat(3,flow_init_1,flow_init_2);
    elseif nargin < 4
        flow_init = zeros(h, w, 2);
    end

    %if nargin < 8
        px1 = zeros(1,h*w); px2 = zeros(1,h*w);
        py1 = zeros(1,h*w); py2 = zeros(1,h*w);
    %end


    ind = int32(reshape(1:h*w, h, w));
    upper_pixel_ind = zeros(h,w,'int32'); upper_pixel_ind(2:end,:) = ind(1:end-1,:); upper_pixel_ind = upper_pixel_ind(:) - 1;
    lower_pixel_ind = zeros(h,w,'int32'); lower_pixel_ind(1:end-1,:) = ind(2:end,:); lower_pixel_ind = lower_pixel_ind(:) - 1;
    left_pixel_ind = zeros(h,w,'int32');  left_pixel_ind(:,2:end) = ind(:,1:end-1);  left_pixel_ind = left_pixel_ind(:) - 1;
    right_pixel_ind = zeros(h,w,'int32'); right_pixel_ind(:,1:end-1) = ind(:,2:end); right_pixel_ind = right_pixel_ind(:) - 1;


    flow = flow_init;
    u = flow(:,:,1);
    v = flow(:,:,2);

    fprintf('Warp ');
    for i = 1:param.Nwarp
        
        fprintf([num2str(i) ' ']);
        %disp(['Warp ' num2str(i)]);

        %warp
        u_pre = u; v_pre = v;
        [I2w, I2wx, I2wy] = warp_and_partial_deriv_bicubic(I1,I2,cat(3,u_pre,v_pre),param.deriv.filter,param.deriv.blend);
        
        for j = 1:param.Nouter
            %threshold: solve v
            [u_, v_] = jly_thresholding(u_pre, v_pre, u, v, I1, I2w, I2wx, I2wy, param.lambda_d, param.theta);
            
            %tv-l2: solve u
            [u, px1, py1] = jly_tvl2(u_(:), px1, py1, upper_pixel_ind, left_pixel_ind, lower_pixel_ind, right_pixel_ind, param.theta, param.Ninner, 1e-10);
            [v, px2, py2] = jly_tvl2(v_(:), px2, py2, upper_pixel_ind, left_pixel_ind, lower_pixel_ind, right_pixel_ind, param.theta, param.Ninner, 1e-10);
            u = reshape(u,[h,w]); v = reshape(v,[h,w]);
            
            if param.medfilt_w > 0
                u = medfilt2(u,[param.medfilt_w,param.medfilt_w]);
                v = medfilt2(v,[param.medfilt_w,param.medfilt_w]);
            end
        end
        
    end
    fprintf('\n');

    flow = cat(3, reshape(u,[h,w]), reshape(v,[h,w]));
    flows{l} = flow;
end