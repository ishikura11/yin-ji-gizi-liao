//author: Jiaolong Yang
#include "mex.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define MAX(x,y) (x)>(y)?(x):(y)

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{   
    /*u0*/
    const mxArray *mxu0 = prhs[0];
    int M = mxGetM(mxu0);
    int N = mxGetN(mxu0);
    double *u0 = (double *)mxGetPr(mxu0);
    
    /*v0*/
    const mxArray *mxv0 = prhs[1];
    double *v0 = (double *)mxGetPr(mxv0);
    
    /*u1*/
    const mxArray *mxu1 = prhs[2];
    double *u1 = (double *)mxGetPr(mxu1);
    
    /*v1*/
    const mxArray *mxv1 = prhs[3];
    double *v1 = (double *)mxGetPr(mxv1);
    
    /*I1*/
    const mxArray *mxI1 = prhs[4];
    double *I1 = (double *)mxGetPr(mxI1);
    
    /*I2w*/
    const mxArray *mxI2w = prhs[5];
    double *I2w = (double *)mxGetPr(mxI2w);
    
    /*I2wx*/
    const mxArray *mxI2wx = prhs[6];
    double *I2wx = (double *)mxGetPr(mxI2wx);
    
    /*I2wy*/
    const mxArray *mxI2wy = prhs[7];
    double *I2wy = (double *)mxGetPr(mxI2wy);
    
    /*lamda*/
    double lambda = mxGetScalar(prhs[8]);
    
    /*theta*/
    double theta = mxGetScalar(prhs[9]);
    
    /***************************************************************/
    /*Output u2*/
    plhs[0] = mxCreateDoubleMatrix(M, N, mxREAL);
    double *u2 = mxGetPr(plhs[0]);
    
    /*Output v2*/
    plhs[1] = mxCreateDoubleMatrix(M, N, mxREAL);
    double *v2 = mxGetPr(plhs[1]);
    
    double lt = lambda*theta;
    double I2wxp, I2wyp, rho, grad, th, du, dv;
    double eps = 1e-10;
    
    for(int p = 0; p < M*N; p++)
    {
        I2wxp = I2wx[p];
        I2wyp = I2wy[p];
        rho = (u1[p]-u0[p])*I2wxp + (v1[p]-v0[p])*I2wyp + I2w[p] - I1[p];
        grad = I2wxp*I2wxp + I2wyp*I2wyp;
        th = lt * grad;
        if(rho < -th)
        {
            du = lt * I2wxp;
			dv = lt * I2wyp;
        }
        else if(rho > th)
        {
            du = -lt * I2wxp;
			dv = -lt * I2wyp;
        }
        else
        {
            if(grad < eps)//grad is 0
                du = dv = 0;
            else
            {
                du = -rho * I2wxp / grad;
                dv = -rho * I2wyp / grad;
            }
        }
        
        u2[p] = u1[p] + du;
		v2[p] = v1[p] + dv;
    }
}