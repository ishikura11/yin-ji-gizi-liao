//author: Jiaolong Yang
#include "mex.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define MAX(x,y) (x)>(y)?(x):(y)

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{   
     /*f*/
    const mxArray *mxf = prhs[0];
    int N = MAX(mxGetM(mxf),mxGetN(mxf));
    double *f = (double *)mxGetPr(mxf);
    
    /*px, py*/
    double *px = (double *)mxGetPr(prhs[1]);
    double *py = (double *)mxGetPr(prhs[2]);
    
    /*pixel_ind*/
    int *upper_pixel_ind = (int *)mxGetPr(prhs[3]);
    int *left_pixel_ind = (int *)mxGetPr(prhs[4]);
    int *lower_pixel_ind = (int *)mxGetPr(prhs[5]);
    int *right_pixel_ind = (int *)mxGetPr(prhs[6]);
    
    /*lamda*/
    double lamda = mxGetScalar(prhs[7]);
    /*maximum iteration*/
    int maxiter = mxGetScalar(prhs[8]);
    /*stop threshold*/
    double epsilon = mxGetScalar(prhs[9]);
    
    /***************************************************************/
    double * u = (double *)malloc(N*sizeof(double));
    double * u_pre = (double *)malloc(N*sizeof(double));
    double * u_ = (double *)malloc(N*sizeof(double));
    
    for(int i = 0; i < N; i++)
    {
        u[i] = u_pre[i] = u_[i] = 0;
    }
    memcpy(u, f, N*sizeof(double));
    memcpy(u_, f, N*sizeof(double));
    
    double sigma, tau, theta;
    sigma = tau = 1/sqrt(8.0);
    //tau = 0.01;
    //sigma = 12.5;
    theta = 1;
    
    double l2norm, dpx, dpy, sad;
    
    for(int iter = 0; iter < maxiter; iter++)
    {
        //Update dual px, py;
        int i;
        for(i = 0; i < N; i++)
        {
            if(right_pixel_ind[i] >= 0)
                px[i] += sigma*(u_[right_pixel_ind[i]] - u_[i]);
            
            if(lower_pixel_ind[i] >= 0)
                py[i] += sigma*(u_[lower_pixel_ind[i]] - u_[i]);
            
            //Projection
            l2norm = sqrt( px[i]*px[i] + py[i]*py[i] );
            if(l2norm > 1)
            {
                px[i] /= l2norm;
                py[i] /= l2norm;
            }   
        }
        
        //Update u
        memcpy(u_pre, u, N*sizeof(double));
        sad = 0;
        for(i = 0; i < N; i++)
        {
            if(left_pixel_ind[i] >=0 && right_pixel_ind[i] >=0)
                dpx = px[i] - px[left_pixel_ind[i]];
            else if(left_pixel_ind[i] < 0)
                dpx = px[i];
            else // right_pixel_ind[i] < 0
                dpx = - px[left_pixel_ind[i]];
            
            if(upper_pixel_ind[i] >=0 && lower_pixel_ind[i] >=0)
                dpy = py[i] - py[upper_pixel_ind[i]];
            else if(upper_pixel_ind[i] < 0)
                dpy = py[i];
            else // lower_pixel_ind[i] < 0
                dpy = - py[upper_pixel_ind[i]];
            
            u[i] = ( lamda*u[i] + lamda*tau*(dpx+dpy) + tau*f[i] ) / ( lamda + tau );
            
            sad += abs(u[i] - u_pre[i]);
        }
        //printf("#%d:%lf %lf\n",iter,sad,epsilon);
        if(sad < epsilon)
            break;
        
        //Update u_
        for(i = 0; i < N; i++)
        {
            u_[i] = u[i] + theta*(u[i]-u_pre[i]);
        }
    }
    
    /*Output u, px, py*/
    plhs[0] = mxCreateDoubleMatrix(1, N, mxREAL);
    memcpy(mxGetPr(plhs[0]), u, N*sizeof(double));
    plhs[1] = mxCreateDoubleMatrix(1, N, mxREAL);
    memcpy(mxGetPr(plhs[1]), px, N*sizeof(double));
    plhs[2] = mxCreateDoubleMatrix(1, N, mxREAL);
    memcpy(mxGetPr(plhs[2]), py, N*sizeof(double));
    
    delete(u);
    delete(u_pre);
    delete(u_);
}