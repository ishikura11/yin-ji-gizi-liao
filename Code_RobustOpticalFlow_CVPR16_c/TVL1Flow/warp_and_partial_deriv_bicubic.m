function [I2w, I2wx, I2wy, It] = warp_and_partial_deriv_bicubic(I1, I2, uv_prev, deriv_filter, b)

%PARTIAL_DERIV   Spatio-temporal derivatives
%   P = PARTIAL_DERIV(IMAGES, INIT) computes the spatio-temporal derivatives
%
%   Author: Deqing Sun, Department of Computer Science, Brown University
%   Contact: dqsun@cs.brown.edu
%   $Date: 2007-11-30 $
%   $Revision $
%
% Copyright 2007-2010, Brown University, Providence, RI. USA
% 
%                          All Rights Reserved
% 
% All commercial use of this software, whether direct or indirect, is
% strictly prohibited including, without limitation, incorporation into in
% a commercial product, use in a commercial service, or production of other
% artifacts for commercial purposes.     
%
% Permission to use, copy, modify, and distribute this software and its
% documentation for research purposes is hereby granted without fee,
% provided that the above copyright notice appears in all copies and that
% both that copyright notice and this permission notice appear in
% supporting documentation, and that the name of the author and Brown
% University not be used in advertising or publicity pertaining to
% distribution of the software without specific, written prior permission.        
%
% For commercial uses contact the Technology Venture Office of Brown University
% 
% THE AUTHOR AND BROWN UNIVERSITY DISCLAIM ALL WARRANTIES WITH REGARD TO
% THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
% FITNESS FOR ANY PARTICULAR PURPOSE.  IN NO EVENT SHALL THE AUTHOR OR
% BROWN UNIVERSITY BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
% DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
% PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
% ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
% THIS SOFTWARE.        

if nargin < 4
    deriv_filter = [1 -8 0 8 -1]/12; % used in Wedel etal "improved TV L1"
end
h = deriv_filter;

if nargin < 5
    b = 0.5;    %blending ratio
end

H   = size(I1, 1);
W   = size(I1, 2);

[x,y]   = meshgrid(1:W,1:H);
x2      = x + uv_prev(:,:,1);        
y2      = y + uv_prev(:,:,2);  

%h = [-0.5, 0, 0.5]; % consistent with bi-cubic interpolation        

if size(I2,3) == 1
    % gray-level
    [I2w, I2wx, I2wy] = interp2_bicubic(I2, x2, y2, h);
else
    % color
    I2w  = zeros(size(I2));
    I2wx  = I2w;
    I2wy  = I2w;
    for j = 1:size(I2,3)
        [I2w(:,:,j), I2wx(:,:,j), I2wy(:,:,j)] = interp2_bicubic(I2(:,:,j),x2,y2, h);
    end;
end;

indx = isnan(I2w);

It = I2w - I1;
% Disable those out-of-boundary pixels in warping
It(indx)    = 0;      

% Temporal average
I1x = imfilter(I1, h,  'corr', 'symmetric', 'same');  %
I1y = imfilter(I1, h', 'corr', 'symmetric', 'same');

% b = 0.6;    % recommended in Wedal etal "improved TV-L1" 2008
% b = 0.5;
% b = 1;

I2wx  = b*I2wx+(1-b)*I1x;
I2wy  = b*I2wy+(1-b)*I1y;

I2wx(indx) = 0;
I2wy(indx) = 0;       
    