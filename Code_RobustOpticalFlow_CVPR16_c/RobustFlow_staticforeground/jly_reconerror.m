function [ rmse ] = jly_reconerror( I_gt, I_e  )
%RECONERROR Summary of this function goes here
%   Detailed explanation goes here


f1 = I_gt(:);
f2 = I_e(:);

f1 = f1-mean(f1);
f2 = f2-mean(f2);

f1_n = f1./norm(f1);
f2_n = f2./norm(f2);

rmse = 1- sum(f1_n.*f2_n);
%平均二条誤差平方根を求める

end

