An implementation of the robust optical flow algorithm described in
"Jiaolong Yang, Hongdong Li, Yuchao Dai, Robby T. Tan,
Robust Optical Flow Estimation of Double-Layer Images under Transparency or Reflection,
Proceedings of the 32th IEEE Conference on Computer Vision and Pattern Recognition (CVPR), 2016"

The provided code is for static foregound cases only, but could be easily extended for dynamic foregound cases.
反射側が固定されているもののみ有効、ただし簡単に動的なものにも設定できる

Author: Jiaolong Yang (yangjiaolong@gmail.com)
Copyright 2015-2016, Jiaolong Yang, All Rights Reserved

This software is for academic research purpose only. If you find it usefull, please cite our paper.

All commercial use of this software, whether direct or indirect, is
 strictly prohibited including, without limitation, incorporation into in
 a commercial product, use in a commercial service, or production of other
 artifacts for commercial purposes. 

Permission to use, copy, modify, and distribute this software and its
 documentation for research purposes is hereby granted without fee,
 provided that the above copyright notice appears in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation.

THE AUTHOR DISCLAIM ALL WARRANTIES WITH REGARD TO
 THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR ANY PARTICULAR PURPOSE.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 THIS SOFTWARE.        
