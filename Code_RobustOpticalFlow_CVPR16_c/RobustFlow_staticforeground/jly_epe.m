function [ epe_noc, epe_all ] = jly_epe( f, f_gt, valid, noc )
%EPE Summary of this function goes here
%   Detailed explanation goes here

if nargin < 3
    valid = true(size(f));
end

if nargin < 4
    noc = true(size(f));
end

valid_noc = valid.*noc;
epe_noc = sum(sum((sum((f-f_gt).^2 ,3).^0.5.*valid_noc))) / numel(valid_noc);
epe_all = sum(sum((sum((f-f_gt).^2 ,3).^0.5.*valid))) / numel(valid);

end
