function [ x ] = jly_L1Min_IRLS_Proj4( A, b, iter, lb, ub, x0)
%L1MINIMIZE Summary of this function goes here
%   Detailed explanation goes here

warning off;

thresh = 1e-2;

delta = 1e-4;%1e-5
num1 = 50;
num2 = 50;

if nargin < 6
    x = zeros(size(b));
else
    x = x0;
end
    
%IRLS
x_pre = x;
es = abs(A*x-b);
e_pre = sum(es);
for ii = 1:iter+1
    w = 1./max(es,delta);
    W = spdiags(w,0,length(w),length(w));
    x = (A'*W*A)\(A'*W*b); %線方程式 (A'*W*A) * x = (A'*W*b)を解く
    
    %proj
    x = proj(A, b, x, lb, ub,  num1, num2);
    es = abs(A*x-b);
    e = sum(es);
    dec = (e_pre - e)/e_pre;
    if e < e_pre
        e_pre = e;
        x_pre = x;
    else
        fprintf('Failed. Trying gradient descent.. ');
        %error: failed to reduce error via LS and projection
        %try gradient descent
        grad = 2*A'*(A*x_pre-b);
        for gamma = 0.1:-0.003:0.001
            x = x_pre - gamma*grad;
            %x(x<lb) = lb; x(x>ub) = ub;
            x = proj(A, b, x, lb, ub,  num1, num2);
            es = abs(A*x-b);
            e = sum(es);
            if e < e_pre
                fprintf(['Succeeded: gamma=' num2str(gamma) '\n']);
                break;
            end
        end
        dec = (e_pre - e)/e_pre;
        if e < e_pre
            e_pre = e;
            x_pre = x;
        else %still fail
            fprintf('Failed again..\n');
            break;
        end
    end
    
    fprintf('IRLS-Porj Iter #%d, error=%g (-%.3f%%) \n', ii, e, dec*100); 
    if dec < 0
        break;
    end
end

warning on;
end


function x = proj(A, b, x, lb, ub,  num1, num2)

    meanx = mean(x);
    x = x-meanx;
    x = x+(ub-lb)/2;

    %coarser
    st = -(ub-lb)/2;
    step = (ub-lb)/num1;
    err = zeros(1,num1);
    for i = 1:num1
        x_= x + (st + i*step);
        x_(x_<lb) = lb; x_(x_>ub) = ub;
        err(i) = sum(abs(A*x_-b));
    end
    [~,ind] = min(err);
    x = x + (st + ind*step);
    
    %finer
    st = -(ub-lb)/num1/2;
    step = (ub-lb)/num1/num2;
    
	err = zeros(1,num2);
    for i = 1:num2
        x_= x + (st + i*step);
        x_(x_<lb) = lb; x_(x_>ub) = ub;
        err(i) = sum(abs(A*x_-b));
    end
    [~,ind] = min(err);
    x = x + (st + ind*step);

    x(x<lb) = lb; x(x>ub) = ub;
end
