function param = jly_load_tvl1_param

param.lambda_d = 0.2;
param.theta = 0.3;
param.Nwarp = 5;
param.Nouter = 10;
param.Ninner = 3;
%param.deriv.filter = [-0.5,0,0.5];
param.deriv.filter = [1 -8 0 8 -1]/12;% used in Wedel etal "improved TV L1"
param.deriv.blend = 0.5;
param.medfilt_w = 3;

param.pyramid_levels = 6;
param.pyramid_spacing = 2;

end