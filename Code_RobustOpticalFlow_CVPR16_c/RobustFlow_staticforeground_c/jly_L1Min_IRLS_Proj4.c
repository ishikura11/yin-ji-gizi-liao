//function [ x ] = jly_L1Min_IRLS_Proj4( A, b, iter, lb, ub, x0)

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void jly_L1Min_IRLS_Proj4( double **A, double **b, int iter, double lb, double ub, double **x0, double **x) { 
//return x
int size_A  = sizeof(A)/sizeof(A[0]);
int size_b  = sizeof(b)/sizeof(b[0]);
int size_x0 = sizeof(x0)/sizeof(x0[0]); 
//double *A
//double *b
//int iter
//int lb
//int ub
//double x0

//%L1MINIMIZE Summary of this function goes here
//%   Detailed explanation goes here

//warning off;

double thresh = 1e-2;

double delta = 1e-4;//1e-5
double *es;
int num1 = 50;
int num2 = 50;
int i, j;

if nargin < 6
    //x = zeros(size(b));
	return;
	//x = (double *)malloc(sizeof(double));
else
    //x = x0;
	for (i = 0; i < sizeof(x0)/sizeof(x0[0]) < i++) {
		x[i] = x0[i];
	}
end
    

%IRLS
double x_pre = x;
//es = abs(A*x-b);

es = (double *)malloc(w * h * sizeof(double));


//行列の積
for (i = 0; i < A_size; i++) {
	for (j = 0; j < x_size; j++) {
		es[i] += A[i * x_size + j] * x[j] 
	}
}

//e_pre = sum(es);
double e_pre = 0;
for (i = 0; i < sizeof(es)/ sizeof(es[0]); i++) {
   e_pre += es[i];
}   

//for ii = 1:iter+1
for (i = 0; i < iter+1; i++) {

    //w = 1./max(es,delta);
	for (j = 0; j < es_size; j++) { 
		es[j] = 1 / es[j];
	}

    //パスW = spdiags(w,0,length(w),length(w));
    x = (A'*W*A)\(A'*W*b);
    
    %proj
    x = proj(A, b, x, lb, ub,  num1, num2);
    es = abs(A*x-b);
 
	//e = sum(es);
	double e = 0;
	for (i = 0; i < sizeof(es)/ sizeof(es[0]); i++) {
		e += es[i];
	}   

	double dec;
	dec = (e_pre - e)/e_pre;
	if (e < e_pre) {
		e_pre = e;
		x_pre = x;
	}
	/*
	else
		fprintf("Failed. Trying gradient descent.. ");
        //error: failed to reduce error via LS and projection
        //try gradient descent
        grad = 2*A'*(A*x_pre-b);
		//
		//gradのサイズはA, bと同じ
        //for gamma = 0.1:-0.003:0.001
		for (j = 100; j > 0; j = j-3) {
			double gamma = j / 1000;
			//x = x_pre - gamma*grad;
			for (k = 0; k < 
            %x(x<lb) = lb; x(x>ub) = ub;
            x = proj(A, b, x, lb, ub,  num1, num2);
            es = abs(A*x-b);
            e = sum(es);
            if e < e_pre
                fprintf(['Succeeded: gamma=' num2str(gamma) '\n']);
                break;
            end
        end
        dec = (e_pre - e)/e_pre;
        if e < e_pre
            e_pre = e;
            x_pre = x;
        else %still fail
            fprintf('Failed again..\n');
            break;
        end
    end
    */
    fprintf('IRLS-Porj Iter #%d, error=%g (-%.3f%%) \n', ii, e, dec*100); 
    if (dec < 0) {
        break;
	}
}

//warning on;
end


//function x = proj(A, b, x, lb, ub,  num1, num2)

double proj(double **A, int Aw, int Ah, double *b, double *x, double lb, double ub, int num1, int num2) {

	int i, j;
	double st, step;
	double *err, *x_;
	x_ = (double *)malloc(sizeof(x));

    //meanx = mean(x);
	double tmp_sum = 0.;
	for (i = 0; i < sizeof(x)/sizeof(x[0]); i++) { 
		tmp_sum += x[i];
	}
	meanx = tmp_sum / (sizeof(x) / sizeof(x[0]));
    
	//x = x-meanx;
	//x = x+(ub-lb)/2;
	for (i = 0; i < sizeof(x)/sizeof(x[0]); i++) {
		x[i] = x[i] - meanx;
		x[i] = x[i] + ((ub - lb)/2);
	}


    //coarser
    st = -(ub-lb)/2;
    step = (ub-lb)/num1;
    //err = zeros(1,num1); //err = [0, 0, ...0]
	err = (double *)malloc(num1 * sizeof(double));
    /*
	 for i = 1:num1
        x_= x + (st + i*step);
        x_(x_<lb) = lb; x_(x_>ub) = ub;
        err(i) = sum(abs(A*x_-b));
    end */

	for (i = 0; i < num1+1; i++) { 
		for (j = 0; j < sizeof(x)/sizeof(x[0]); j++) {
			x_[j] = x[j] + (st + j * step);
			if (x_[j] < lb) 
				x_[j] = lb;
			if (x_[j] > ub)
				x_[j] = ub;
		}
        err(i) = sum(abs(A*x_-b));
		
	}
		
   [~,ind] = min(err); // == ones(1, err)?
   double minerr[sizeof(err)/sizeof(err[0])];
   //double min_err = 9999.;
   /*
   for (i = 0; i < sizeof(err) / sizeof(err[0]); i++) {
	   if(min_err > err[i])
		   min_err = err[i];
   }

   for (i = 0; i < ind; i++) {
	*/	
    x = x + (st + ind * step);
    
    %finer
    st = -(ub-lb)/num1/2;
    step = (ub-lb)/num1/num2;
    err = zeros(1,num2);

	/*
    for i = 1:num2
        x_= x + (st + i*step);
        x_(x_<lb) = lb; x_(x_>ub) = ub;
        err(i) = sum(abs(A*x_-b));
    end
    */
	for (i = 0; i< num2; i++) {
		x_= x + (st + i*step);
		x_(x_ < lb) = lb; x_(x_>ub) = ub;

		for (j = 0; j < sizeof(x_)/ sizeof(x_[0]); j++) { 
			if (x_[j] < lb) x_[j] = lb;
			if (x_[j] > ub) x_[j] = ub;
		}
		err(i) = sum(abs(A*x_-b));
		double tmpsum = 0;
		for (j = 0; j < sizeof(x_)/ sizeof(x_[0]); j++) { 
			
		}
	}
 


	[~,ind] = min(err);
	minerr[sizeof(err)/sizeof(err[0])];
	//x = x + (st + ind*step);

    //x(x<lb) = lb; x(x>ub) = ub;
	for (j = 0; j < sizeof(x)/ sizeof(x[0]); j++) { 
		x[j] = x[j] + (st + ind*step);
		if (x[j] < lb) x[j] = lb;
		if (x[j] > ub) x[j] = ub;
	}
}

