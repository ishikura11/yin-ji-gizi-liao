function [ J, fail ] = jly_separate_IRLS_Proj( O1, O2, flow, lambda_dJ, lambda_dI, J_init, IRLS_iter, lb, ub, interp )
%SEPRATE Summary of this function goes here
%   Detailed explanation goes here

h = size(O1,1);
w = size(O1,2);
s = w*h;

% [Gx, Gy] = getGMat(w,h);
Gfilename = ['G_' num2str(h) '_' num2str(w) '.mat'];
if exist(Gfilename,'file')
    load(Gfilename);
else
    disp('Computing Gx Gy...');
    [Gx, Gy] = getGMat(w,h);
    save(Gfilename,'Gx','Gy');
end


A = zeros(0,w*h);
b = zeros(0,1);

disp('IRLS内getGMat');
toc;

%% construct A, b 
% constraint: brightness consitency of I1(x), I2(x+u)
% tic;
if strcmp(interp, 'bilinear')
    [A_, b_] = jly_constructAbfromFlow_bilinear(flow, O1, O2);
elseif strcmp(interp, 'nearest')
    [A_, b_] = jly_constructAbfromFlow_nearest(flow, O1, O2);
else
    
end
% toc;
A = [A;A_];
b = [b;b_];

% constraint: gradients of I1, I2
if lambda_dI > 0
    A = [A;[Gx;Gy]*lambda_dI];
    b = [b;[Gx*O1(:);Gy*O1(:);]*lambda_dI];
%     A = [A;[Gx;Gy]*lambda_dI];
%     b = [b;[;Gx*O2(:);Gy*O2(:)]*lambda_dI];
end


% constraint: gradients of J
if lambda_dJ > 0
    A = [A;Gx*lambda_dJ;Gy*lambda_dJ];
    b = [b;zeros(s*2,1)];
end

% % constraint: J
% donomalize = true;
% if lambda_J > 0
%     A = [A;speye(s)*lambda_J];
%     b = [b;zeros(s,1)];
%     donomalize = false;
% end

x_pre = J_init(:);
%fprintf(['L1 residual before: ' num2str(sum(abs(A*x-b))) '\n']);
%% solve L1 minimization
% x = jly_L1Min_IRLS(A, b, IRLS_iter);
x = jly_L1Min_IRLS_Proj4(A, b, IRLS_iter, lb, ub, x_pre);
% x = jly_L1min_LP(A, b, lb, ub);
%fprintf(['L1 residual after: ' num2str(sum(abs(A*x-b))) '\n']);
disp('IRLS内 L1の最小化');
toc;

%% output
if sum(abs(A*x-b)) >= sum(abs(A*x_pre-b))
    fail = true;
    J = J_init;
else
    fail = false;
    J = reshape(x,size(O1));
end
end
