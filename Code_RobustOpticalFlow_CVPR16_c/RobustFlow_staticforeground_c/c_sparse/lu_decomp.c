#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define N 4
#define BIG_NUM 99999999

//参考 
//http://ktm11.eng.shizuoka.ac.jp/lesson/2012/ip/lecture_11.pdf

extern double check_val(int *row, int *col, double *val, int p_row, int p_col, int val_size);
extern int check_val_p(int *row, int *col, double *val, int p_row, int p_col, int val_size);

extern void insert_matrix(int *row, int *col, double *val,
						  int valsize, 
						  int add_row, int add_col, double add_val );

extern void erase_matrix(int *row, int *col, double *val, int val_size, int set_p);

void lu_decomp(int *arow, int *acol, double *aval, int asize, int *valsize, int *p){
	/*
	arow, acol, aval ... 'ax = b'の行列aを座標で表したもの
	asize			 ... 行列aの行列の行, 列の長さ
	valsize			 ... 行列aの要素の数
	p				 ... LU分解する際に利用するピボット
*/
	int i,j,k, l,ip, tmp_p, tmp_p2, check = 0;
	double alpha, tmp, tmp2, tmp_kj;
	double amax, eps=pow(2.0,-50.0); /* eps=2^{-50}とする */
	//asizeは行列の長さ
	printf("valsize = %d\n", *valsize);
	for(k = 0; k < asize-1; k++){
		if (k % 10 == 0) fprintf(stderr, ".");
		amax = fabs(check_val(arow, acol, aval, k, k, *valsize));
		//printf("amax = %f k = %d valsize = %d\n", amax, k, *valsize);
		ip = k; /* ピボットの選択 */
		for (i = 0; i < *valsize; i++) {
			if (aval[i] == 0.) {
				printf("aval[%d] = 0, k = %d\n", i, k);
				return;
			}
		}
		if (amax == 0) {
			printf("check_val_p = %d\n", check_val_p(arow, acol, aval, k, k, *valsize));
			printf("k = %d\n", k);
		}
		for(i = k + 1; i < asize; i++){
			if(fabs(check_val(arow, acol, aval, i, k, *valsize)) > amax){
				amax = fabs(check_val(arow, acol, aval, i, k, *valsize));
				//printf("amax = %f k = %d i = %d valsize = %d\n", amax, k, i, valsize);
				ip=i;
			}
		}
		if(amax < eps) { // 正則性の判定 
			printf("入力した行列は正則ではない!!\n"); 
			printf("amax = %f\n", amax);
			exit(1);
		}
		//ここまで問題なし
		p[k]=ip;
		//printf("p[%d] = %d\n", k, p[k]);
		//printf("k = %d, ip = %d\n", k, ip);
		if(ip != k){
			for(j = k; j < asize; j++){
				tmp_p  = check_val_p(arow, acol, aval, k, j, *valsize);
				tmp_p2 = check_val_p(arow, acol, aval, ip, j, *valsize);
				//printf("k = %d, j = %d\n", k, j);
				//printf("tmp = %f, tmp_p = %d, tmp_p2 = %d\n", tmp, tmp_p, tmp_p2);
				//printf("aval[tmp_p] = %f, aval[tmp_p2] = %f\n", aval[tmp_p], aval[tmp_p2]);
				if (tmp_p == BIG_NUM) { //(k, j)座標の値が0 
					if(tmp_p2 != BIG_NUM) { //(ip, j)座標の値が0でない
						//tmp2 = check_val(arow, acol, aval, ip, j, *valsize);
						tmp2 = aval[tmp_p2];
						erase_matrix(arow, acol, aval, *valsize, tmp_p2);
						*valsize = *valsize - 1;
						insert_matrix( arow, acol, aval,
									   *valsize, 
									   k, j, tmp2);
						*valsize = *valsize + 1;
					}
				} else if (tmp_p2 == BIG_NUM) { //(ip, j)の値が0
					if (tmp_p != BIG_NUM) { //間違いなし
						/*
						printf("erase before erase tmp_p = %d\n", tmp_p);
						printf("arow = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", arow[l]);
						}	   
						printf("\n");
						printf("acol = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", acol[l]);
						}
						printf("\n");
						printf("aval = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%f ", aval[l]);
						}	   
						printf("\n");
						*/
						//tmp = check_val(arow, acol, aval, k, j, *valsize);
						tmp = aval[tmp_p];
						insert_matrix( arow, acol, aval,
									   *valsize, 
									   ip, j, tmp);
						*valsize = *valsize + 1;
						erase_matrix(arow, acol, aval, *valsize, tmp_p);
						*valsize = *valsize - 1;
						
					}
				} else { //(k, j), (ip, j)の座標の値二つとも0でないとき
				//tmp = check_val(arow, acol, aval, k, j, *valsize);
				//tmp2 = check_val(arow, acol, aval, ip, j, *valsize);
					tmp = aval[tmp_p];
					tmp2 = aval[tmp_p2];
					aval[tmp_p] = tmp2; 
					aval[tmp_p2] = tmp;
					//printf("fuga\n");// (k, j), (ip, j)どちらも0のときはなにもしない
				}
			}
		}
		for(i = k + 1; i < asize; i++){ /* 前進消去 */
			alpha = -check_val(arow, acol, aval, i, k, *valsize) / 
					check_val(arow, acol, aval, k, k, *valsize);
			//printf("i = %d, k = %d, alpha = %f\n", i, k, alpha);
			//printf("i = %d, k = %d\n", i, k);
			/*
			printf("a[i][k] = %f, a[k][k] = %f, alpha = %f\n", 
					check_val(arow, acol, aval, i, k, valsize), 
					check_val(arow, acol, aval, k, k, valsize), 
				   	alpha);*/
			tmp_p  = check_val_p(arow, acol, aval, i, k, *valsize);
			/*
			if(alpha == 0) {
				if(tmp_p != BIG_NUM) {
					erase_matrix(arow, acol, aval, valsize, tmp_p);
					valsize--;
				}
			}*/
			if(alpha != 0.) {// alpha = 0なら実行する必要がない 
					if(tmp_p == BIG_NUM) { //(i, k)座標の値が0のとき
					//そもそもalpha != 0ならばtmp_p != 0を表すのでこの分岐いらない?
					//printf("alpha = %f,  0 error2 \n", alpha);
					
					insert_matrix( arow, acol, aval,
								   *valsize, 
								   i, k, alpha);
					*valsize = *valsize + 1;
					//printf("1insert !!!\n");
				} else {
					aval[tmp_p] = alpha;
				}
			//}
			//if(alpha != 0) { 
				for(j = k + 1; j < asize; j++){
					/* 
					 * n行目のもののみ扱う配列なのでn列目の
					 * いちいちcheck_val関数を使わずとも行単位で
					 * 処理を行うことで高速化できそう
					 */
					/*
					if(i == 9 && j == 9 && k == 8) {
						printf("hoge i = %d, j = %d, arow = \n", i, j);
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", arow[l]);
						}	   
						printf("\n");
						printf("acol = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", acol[l]);
						}
						printf("\n");
						printf("aval = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%f ", aval[l]);
						}	   
						printf("\n");
					}
					*/

					tmp_p = check_val_p(arow, acol, aval, i, j, *valsize);
					tmp_kj = check_val(arow, acol, aval, k, j, *valsize); 
					
					if (tmp_p == BIG_NUM && tmp_kj != 0) {
						insert_matrix( arow, acol, aval,
										   *valsize, 
										   i, j, alpha * tmp_kj);
						*valsize = *valsize + 1;
					} else if (tmp_p != BIG_NUM) {
						//tmp = check_val(arow, acol, aval, i, j, *valsize);
						tmp = aval[tmp_p];
						/*
						if(i == 99 && j == 99) {
							printf("i = %d, j = %d, k = %d\n", i, j , k);
							printf("alpha = %f, tmp_kj = %f, aval[%d] = %f, tmp = %f\n", alpha, tmp_kj, tmp_p, aval[tmp_p], tmp);
						}
						*/
						aval[tmp_p] = tmp + alpha * tmp_kj;
						if (aval[tmp_p] == 0) { 
							/*
							printf("erase tmp_p = %d\n", tmp_p);
						printf("arow = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", arow[l]);
						}	   
						printf("\n");
						printf("acol = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", acol[l]);
						}
						printf("\n");
						printf("aval = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%f ", aval[l]);
						}	   
						printf("\n");
						*/
							erase_matrix(arow, acol, aval, *valsize, tmp_p);
							*valsize = *valsize - 1;
						/*
							printf("erase after\n");
						printf("arow = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", arow[l]);
						}	   
						printf("\n");
						printf("acol = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", acol[l]);
						}
						printf("\n");
						printf("aval = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%f ", aval[l]);
						}	   
						printf("\n");
						*/
						}
					}
				//a[i][j] = a[i][j]+alpha*a[k][j]
					/*
					if(i == 99 && j == 99) {
						tmp_p = check_val_p(arow, acol, aval, i, j, *valsize);
						printf("after\n");
						printf("tmp_p = %d\n", tmp_p);
					}
					*/
				}
			}
		}
		/*
		if( k == 8) {
						printf("k = %d\n", k);
						printf("arow = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", arow[l]);
						}	   
						printf("\n");
						printf("acol = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%d ", acol[l]);
						}
						printf("\n");
						printf("aval = \n");
						for (l = 0; l < *valsize; l++) {
						   printf("%f ", aval[l]);
						}	   
						printf("\n");
		}
		*/
	}
	//printf("decomp last valsize = %d\n", *valsize);
}
