#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "lusub.h"
//#define N 100
void lu_decomp_sub(double a[N][N],int p[N]){
	int i,j,k,ip, iptmp;
	double alpha, tmp;
	double amax, eps=pow(2.0,-50.0); /* eps=2^{-50}とする */
/*
	for (k = 0; k < N; k++)	
		printf("init p[%d] = %d\n", k, p[k]);
*/	
	for (i = 0; i < N; i ++ ) { 
		for (j = 0; j < N; j++) {
			printf(" %f", a[i][j]);
		}
		printf("\n");
	}

	for(k = 0; k < N-1; k++){
		amax = fabs(a[k][k]); ip=k; /* ピボットの選択 */
		//printf("amax = %f k = %d\n", amax, k);
		for(i = k+1; i < N; i++){
			if(fabs(a[i][k]) > amax){
				amax = fabs(a[i][k]); ip=i;
				//printf("amax = %f k = %d i = %d\n", amax, k, i);
			}
		}
		
		if(amax < eps) { /* 正則性の判定 */
			printf("k = %d\n", k);
			printf("入力した行列は正則ではない!!¥n"); exit(1);
		}
		//p[k] = ip;
		//printf("p[%d] = %d\n", k, p[k]);
		if(ip != k){
			for(j = 0; j < N; j++){
				//printf("k = %d, j = %d\n", k, j);
				//printf("tmp = %f, a[%d][%d] = %f\n", a[k][j], ip, j, a[ip][j]);
				tmp = a[k][j]; a[k][j] = a[ip][j]; a[ip][j] = tmp;
			}
			
			iptmp = p[ip];
			p[ip] = p[k];
			p[k] = iptmp;	
			
		}
		/*
		printf("a = \n");
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				printf("%f ", a[i][j]);
			}
			printf("\n");
		}
		*/

		for(i = k+1; i < N; i++){ /* 前進消去 */
			alpha = a[i][k] / a[k][k];
			//printf("i = %d, k = %d\n", i, k);
			//printf("a[i][k] = %f, a[k][k] = %f, alpha = %f\n", a[i][k], a[k][k], alpha);
			a[i][k] = alpha;
			for(j = k+1; j < N; j++){
				a[i][j] -= alpha * a[k][j];
			}
		}
	}
}
