#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define BIG_NUM 99999999

extern void proj(int *arow, int *acol, double *aval,
		  double *b, double *x, 
		  int asize, int bsize, int xsize,
		  double lb, double ub, int num1, int num2);

void print_arrd(double *arr, int arr_size) { 
	int i;
	for (i = 0; i < arr_size; i++) {
		if (arr[i] == 0.) break;
		printf("%f ", arr[i]);
	}
	printf("\n");
	return;
}
void import_matrix(int *array, int *valsize, char *fname);
void import_matrixd(double *array, int *valsize, char *fname);
void export_matrixd(double *array, int arrsize, char *fname);
void transposition_matrix(int *row, int *col, double *val, 
						  int *ans_row, int *ans_col, double *ans_val, 
						  int val_size);

int main() {
	int *arow, *acol, asize = 50000 * 10000, msize = asize * 5, valsize;
	int i, j, bsize = 50000, xsize = 10000; /* p[0…N-2]を利用, p[N-1]は未使用 */
	double *b, *aval, *x;

	arow = (int *)malloc(sizeof(int) * 100000);
	acol = (int *)malloc(sizeof(int) * 100000);
	aval = (double *)malloc(sizeof(double) * 100000);
	b = (double *)malloc(sizeof(double) * bsize);
	x = (double *)malloc(sizeof(double) * xsize);

	for (i = 0; i < bsize; i++) {
		b[i] = 0.;
	}

	for (i = 0; i < 100000 ; i++) {
		arow[i] = BIG_NUM;
		acol[i] = BIG_NUM;
		aval[i] = 0.;
	}
import_matrix(arow, &valsize, "./dataset_sparse/proj/L1min_Arow.dat");
import_matrix(acol, &valsize, "./dataset_sparse/proj/L1min_Acol.dat");
import_matrixd(aval, &valsize, "./dataset_sparse/proj/L1min_Aval.dat");
import_matrixd(b, &bsize, "./dataset_sparse/proj/L1min_b.dat");
import_matrixd(x, &xsize, "./dataset_sparse/proj/L1min_xproj.dat");
printf("aval[0] = %.15f, b[0] = %.15f, x[0] = %.15f\n", aval[0], b[0], x[0]);

/*
	arow[0] = 0, arow[1] = 0,  arow[2] = 0; 
	acol[0] = 0, acol[1] = 1,  acol[2] = 2;
	aval[0] = 1, aval[1] = 2,  aval[2] = 3;

	arow[3] = 1,  arow[4] = 1, arow[5] = 1;
	acol[3] = 0,  acol[4] = 1, acol[5] = 2;
	aval[3] = 4, aval[4] = 5, aval[5] = 6;

	arow[6] = 2, arow[7] = 2,  arow[8] = 2;
	acol[6] = 0, acol[7] = 1,  acol[8] = 2;
	aval[6] = 7, aval[7] = 8,  aval[8] = 10;

	b[0] = 1, b[1] = 1, b[2] = 1;
	valsize = 9;
	bsize = 3;
	xsize = 3;
	extern void proj(int *arow, int *acol, double *aval,
		  double *b, double *x, 
		  int asize, int bsize, int xsize,
		  double lb, double ub, int num1, int num2);
*/
	printf("arow = ");
	for (j = 0; j < 10; j++) { 
		printf("%d ", arow[j]);
	}
	printf("\n");
		
	printf("acol = ");
	for (j = 0; j < 10; j++) { 
		printf("%d ", acol[j]);
	}
	printf("\n");
		
	printf("aval = ");
	for (j = 0; j < 10; j++) { 
		printf("%f ", aval[j]);
	}
	printf("\n");

	proj(arow, acol, aval, 
		 b, x, 
		 valsize, bsize, xsize, 
		 0, 70, 50, 50);

	/*printf("x = ");
	for (i = 0; i < xsize; i++) {
		printf("%f ", x[i]);
	}*/
	printf("fugafugafuga\n");
	//printf("x[0] = %.15f\n", x[0]);
	export_matrixd(x, xsize, "xsub.dat");

	free(arow);
	free(acol);
	free(aval);
	free(b);
	free(x);

	return 0;

}
