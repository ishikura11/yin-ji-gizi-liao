#include <stdio.h>
#define BIG_NUM 99999999


extern void insert_matrix(int *row, int *col, double *val,
						  int val_size, 
						  int add_row, int add_col, double add_val);

void print_arrd(double *arr, int arr_size) { 
	int i;
	for (i = 0; i < arr_size; i++) {
		if (arr[i] == 0.) break;
		printf("%f ", arr[i]);
	}
	printf("\n");
	return;
}

void print_arri(int *arr, int arr_size) { 
	int i;
	for (i = 0; i < arr_size; i++) {
		if (arr[i] == BIG_NUM) break;
		printf("%d ", arr[i]);
	}
	printf("\n");
	return;
}


void plus_matrix(int *row1, int *col1, double *val1, 
				 int *row2, int *col2, double *val2,
				 int valsize1, int valsize2,
				 int *out_matsize) {
	//row2に足した値を出力

	int i, j, out_ind = valsize2, mat2_ind = 0;
	/*
	if (matsize1 != matsize2 ) {
		printf("no match row size or col size")
		return;
	}
	*/
	
	for (i = 0; i < valsize1; i++) {
		printf("loop %d\n ", i);
		if (val1[i] == 0.) {
			printf("break\n");
			break;
		}
		//row 一致
		if (row1[i] == row2[mat2_ind] ) {
			printf("row 一致\n");
			if( col1[i] == col2[mat2_ind]) {
				printf("col一致\n");
				val2[mat2_ind] += val1[i];
				printf("row, col一致 val2 = ");
				print_arrd(val2, out_ind);
				mat2_ind++;

			} else if (col1[i] < col2[mat2_ind]) {
				printf("col1 < col2 \n");
				insert_matrix(row2, col2, val2, 
							  valsize2,
							  row1[i], col1[i], val1[i]);
				out_ind++;
				printf("rowのみ一致 val2 = ");
				print_arrd(val2, out_ind);

			} else {
				printf("col1 > col2 \n");
				mat2_ind++;
				printf("mat2_ind = %d\n", mat2_ind);
				for (j = mat2_ind; j < out_ind; j++) {
					printf("col1 > col2 loop j = %d\n", j);
					if (col1[i] < col2[j] || row1[i] != row2[j]) {
						insert_matrix(row2, col2, val2, 
									  valsize2,
									  row1[i], col1[i], val1[i]);
						out_ind++;
						printf("hoge val2 = ");
						print_arrd(val2, out_ind);
						mat2_ind = j;
						break;
					} else if( col1[i] == col2[j] && row1[i] == row2[j]) {
						printf("col一致\n");
						val2[j] += val1[i];
						printf("row, col一致 val2 = ");
						print_arrd(val2, out_ind);
						mat2_ind = j;
						break;
					}
				}
			}
		// rowだけ違う
		} else if(row1[i] < row2[mat2_ind]) {
			printf("row < row1\n");
			insert_matrix(row2, col2, val2, 
						  valsize2,
						  row1[i], col1[i], val1[i]);
			out_ind++;
			printf("fuga val2 = ");
			print_arrd(val2, out_ind);

		} else { //row1[i] > row2[mat2_ind] 
			printf("row > row1\n");
			mat2_ind++;
			for (j = mat2_ind; j < out_ind; j++) {
				printf("row > row1 loop j = %d\n", j);
				if(row1[i] < row2[j] || val2[j] == 0) {
					printf("out_ind = %d\n", out_ind);
					insert_matrix(row2, col2, val2,
								  out_ind,
								  row1[i], col1[i], val1[i]);
					out_ind++;
					printf("piyo val2 = ");
					print_arrd(val2, out_ind);
					mat2_ind = j;
					break;
				} else if( col1[i] == col2[j] && row1[i] == row2[j]) {
					printf("col一致\n");
					val2[j] += val1[i];
					printf("row, col一致 val2 = ");
					print_arrd(val2, out_ind);
					mat2_ind = j;
					break;
				}
			}
		}
	}
	*out_matsize = out_ind;
}



