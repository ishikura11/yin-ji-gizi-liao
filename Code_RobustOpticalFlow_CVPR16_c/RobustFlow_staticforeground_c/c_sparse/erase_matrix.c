#define BIG_NUM 99999999

//疎行列row, col, valからset_p座標にある値を削除

void erased(double *ptr, double *last) {
	while ( ++ptr < last ) {
		*(ptr - 1) = *ptr;       //  要素をひとつ前にずらす
	}
	*(last-1) = 0.;
}

void erasei(int *ptr, int *last) {
	while ( ++ptr < last ) {
		*(ptr - 1) = *ptr;       //  要素をひとつ前にずらす
	}
	*(last-1) = BIG_NUM;
}


void erase_matrix(int *row, int *col, double *val, int valsize, int set_p) { 
	erasei(row + set_p, row + valsize);
	erasei(col + set_p, col + valsize);
	erased(val + set_p, val + valsize);
}


