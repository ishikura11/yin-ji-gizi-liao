#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define BIG_NUM 99999999

/*
extern void proj(int *arow, int *acol, double *aval,
		  double *b, double *x, 
		  int asize, int bsize, int xsize,
		  double lb, double ub, int num1, int num2);
*/
extern void transposition_matrix(int *row, int *col, double *val, 
						  int *ans_row, int *ans_col, double *ans_val, 
						  int val_size);
extern void multi_matrix(int *row1, int *col1, double *val1, 
				 int *row2, int *col2, double *val2,
				 int *out_row, int *out_col, double *out_val,
				 int valsize1, int valsize2, int *out_valsize);
extern void lu_decomp(int *arow, int *acol, double *aval, int asize, int *valsize, int *p);
extern void lu_solve(int *arow, int *acol, double *aval, int asize, int *valsize, double *b, int *p);
extern void export_matrixd(double *array, int arrsize, char *fname);
extern void export_matrixi(int *array, int arrsize, char *fname);

void jly_L1Min_IRLS_Proj4(int *arow, int *acol, double *aval, int avalsize, 
						  double *b, int b_size, int iter, double lb, double ub, 
						  double *x0,int x0_size, double *x) { 
//double *A 1255000 * 251000
//double *b 1255000 * 1
//int iter 5
//int lb 0
//int ub 70
//double x0 251000  * 1

	double thresh = 1e-2, delta = 1e-4;//1e-5
	double e_pretotal, e_total, dec, gamma;
	double *esval, *wval, *avaltrans, *awval, *awaval, *awbval, 
		   *avaltrans2, *grad, *x_pre;
	
	int *arowtrans, *acoltrans, *wrow, *wcol, *awrow, *awcol,
		*awarow, *awacol, *p;
	int num1 = 50, num2 = 50;
	int i, j, k, awvalsize, awavalsize, wvalsize;
	
	clock_t c_start, c_end;
	
	esval = (double *)malloc(b_size * sizeof(double));
	wrow = (int *)malloc(b_size * sizeof(int));
	wcol = (int *)malloc(b_size * sizeof(int));
	wval = (double *)malloc(b_size * sizeof(double));

	arowtrans = (int *)malloc(avalsize * sizeof(int));
	acoltrans = (int *)malloc(avalsize * sizeof(int));
	avaltrans = (double *)malloc(avalsize * sizeof(double));

	avaltrans2 = (double *)malloc(avalsize * sizeof(double));

	awrow = (int *)malloc(avalsize * sizeof(int));
	awcol = (int *)malloc(avalsize * sizeof(int));
	awval = (double *)malloc(avalsize * sizeof(double));

	awarow = (int *)malloc(avalsize * 10 * sizeof(int));
	awacol = (int *)malloc(avalsize * 10 * sizeof(int));
	awaval = (double *)malloc(avalsize * 10 * sizeof(double));

	awbval = (double *)malloc(x0_size * sizeof(double));
	grad = (double *)malloc(x0_size * sizeof(double));
	x_pre = (double *)malloc(x0_size * sizeof(double));

	p = (int *)malloc(arow[avalsize-1] * sizeof(int));
	
	/*
	double esval[b_size], wval[b_size], avaltrans[avalsize], avalsub2[avalsize], 
		   awval[avalsize], awval[avalsize], awaval[avalsize * 10], awbval[x0_size], 
		   grad[x0_size], x_pre[x0_size];
	
	int wrow[b_size], wcol[b_size], arowtrans[avalsize], acoltrans[avalsize], 
		awrow[avalsize], awcol[avalsize], awarow[avalsize*10], awacol[valsize*10], 
		p[arow[avalsize-1]];
	*/
	for (i = 0; i < avalsize; i++) {
		arowtrans[i] = BIG_NUM;
		acoltrans[i] = BIG_NUM;
		avaltrans[i] = 0.;
	}

	c_start = clock();
	transposition_matrix(arow, acol, aval, 
						 arowtrans, acoltrans, avaltrans, avalsize);
	c_end = clock();
	printf("%f\n", (double)(c_end - c_start) / CLOCKS_PER_SEC);
	for (i = 0; i < avalsize; i++) {
		avaltrans2[i] = 2 * avaltrans[i];
		awrow[i] = BIG_NUM;
		awcol[i] = BIG_NUM;
		awval[i] = 0.;
	}
	for (i = 0; i < avalsize * 10; i++) {
		awarow[i] = BIG_NUM;
		awacol[i] = BIG_NUM;
		awaval[i] = 0.;
	}

	for (i = 0; i < b_size; i++) {
		esval[i] = 0.;
		wrow[i] = i;
		wcol[i] = i;
		wval[i] = 0.;
	}

	for (i = 0; i < x0_size; i++) {
		awbval[i] = 0;
		grad[i] = 0.;
		x_pre[i] = 0.;
	}	
	/*
		export_matrixi(arowtrans, avalsize, "./dataset_sparse/out_dataset/arowsub.dat");
		export_matrixi(acoltrans, avalsize, "./dataset_sparse/out_dataset/acolsub.dat");
		export_matrixd(avaltrans, avalsize, "./dataset_sparse/out_dataset/avalsub.dat");
*/

	/*
if nargin < 6
    //x = zeros(size(b));
	return;
	//x = (double *)malloc(sizeof(double));
else
    //x = x0;
	for (i = 0; i < sizeof(x0)/sizeof(x0[0]) < i++) {
		x[i] = x0[i];
	}
end
*/
//だいたいx = x_pre
//%IRLS
//double x_pre = x;
	for (i = 0; i < x0_size; i++) {
		x_pre[i] = x0[i];
		x[i] = x0[i];
	}

//es = abs(A*x-b);

	for (i = 0; i < avalsize; i++) {
		esval[arow[i]] += aval[i] * x[acol[i]]; 
	}
	for (i = 0; i < b_size; i++) { 
		esval[i] -= b[i];
		esval[i] = fabs(esval[i]);
	}
	export_matrixd(esval, b_size, "./dataset_sparse/out_dataset/esval.dat");

	printf("avalsize = %d, b_size = %d, x_pre_size = %d\n", 
			avalsize, b_size, x0_size);
//e_pre = sum(es);
	e_pretotal = 0.;
	e_total = 0.;
	for (i = 0; i < b_size; i++) {
		//printf("i = %d\n", i);
		e_pretotal += esval[i];
	}
	printf("esval = ");
	for (i = 0; i < 10; i++) { 
		printf("%f ", esval[i]);
	}
	printf("\n");
//for ii = 1:iter+1
	for (i = 0; i < iter+1; i++) {
		printf("i = %d\n", i);
		//w = 1./max(es,delta);
		for (j = 0; j < b_size; j++) { 
			if(delta < esval[j]) 
				esval[j] = 1 / esval[j];
			else 
				esval[j] = 1 / delta;
		}
		printf("loop 1 end\n");
	    //パスW = spdiags(w,0,length(w),length(w));
		for (j = 0; j < b_size; j++) {
			wval[j] = esval[j];
		}
		wvalsize = b_size;
		printf("loop 2 end\n");
		printf("\n");
		export_matrixd(wval, wvalsize, "./dataset_sparse/out_dataset/wval.dat");

		printf("avalsize = %d wvalsize = %d\n", avalsize, wvalsize);
		for (j = 0; j < avalsize; j++) {
			awrow[j] = arowtrans[j];
			awcol[j] = acoltrans[j];
			awval[j] = avaltrans[j] * wval[acoltrans[j]];
		}
	/*
		for (j = 0; j < 10; j++) {
			printf("awval[%d] = %.15f aval[%d] = %f, acol[%d] = %d, wval[acol[%d]] = %f\n",
				   	j, awval[j], j, avaltrans[j], j, acoltrans[j], j, wval[acolsub[j]-1]);
		}
	*/	
		awvalsize = avalsize;
			/*
		multi_matrix(arow, acol, aval, 
					 wrow, wcol, wval, 
					 awrow, awcol, awval,
					 valsize, wvalsize, &awvalsize);
		*/
		printf("awvalsize = %d\n", awvalsize);
		
		printf("awrow = ");
		for (j = 0; j < 10; j++) { 
			printf("%d ", awrow[j]);
		}
		printf("\n");

		printf("awcol = ");
		for (j = 0; j < 10; j++) { 
			printf("%d ", awcol[j]);
		}
		printf("\n");

		printf("awval = ");
		for (j = 0; j < 10; j++) { 
			printf("%f ", awval[j]);
		}
		printf("\n");
		export_matrixi(awrow, awvalsize, "./dataset_sparse/out_dataset/awrow.dat");
		export_matrixi(awcol, awvalsize, "./dataset_sparse/out_dataset/awcol.dat");
		export_matrixd(awval, awvalsize, "./dataset_sparse/out_dataset/awval.dat");

		c_start = clock();
		multi_matrix(awrow, awcol, awval, 
					 arow, acol, aval, 
					 awarow, awacol, awaval,
					 awvalsize, avalsize, &awavalsize);
		c_end = clock();
		printf("%f\n", (double)(c_end - c_start) / CLOCKS_PER_SEC);
		//19分くらいかかる
		for (j = 0; j < awvalsize; j++) {
			awbval[awrow[j]] += awval[j] * b[awcol[j]]; 
		}
		printf("loop 3 end awavalsize = %d\n", awavalsize);

		printf("awarow = ");
		for (j = 0; j < 10; j++) { 
			printf("%d ", awarow[j]);
		}
		printf("\n");

		printf("awacol = ");
		for (j = 0; j < 10; j++) { 
			printf("%d ", awacol[j]);
		}
		printf("\n");

		printf("awaval = ");
		for (j = 0; j < 10; j++) { 
			printf("%f ", awaval[j]);
		}
		printf("\n");
		

		export_matrixi(awarow, awavalsize, "./dataset_sparse/out_dataset/awarow.dat");
		export_matrixi(awacol, awavalsize, "./dataset_sparse/out_dataset/awacol.dat");
		export_matrixd(awaval, awavalsize, "./dataset_sparse/out_dataset/awaval.dat");
		export_matrixd(awbval, b_size, "./dataset_sparse/out_dataset/awbval.dat");
		/*
		if(awaval[awavalsize-1] == 0) {
			awavalsize -= 1;
		}
		*/
		printf("awavalsize = %d\n", awavalsize);
		lu_decomp(awarow, awacol, awaval, b_size, &awavalsize, p);
		printf("lu_decomp clear\n");
		lu_solve(awarow, awacol, awaval, b_size, &awavalsize, awbval ,p);
		//awbval がxになる
		printf("lu_solve clear\n");

		for (j = 0; j < x0_size; j++) {
			x[j] = awbval[j];
		}
		//x = (A'*W*A)\(A'*W*b);
		printf("loop 4 end\n");
 
	    //%proj
		/*
		proj(arow, acol, aval,
			 b, x, 
			 valsize, b_size, x0_size,
			 lb, ub, num1, num2);*/
	    //es = abs(A*x-b);
		for (j = 0; j < b_size; j++) {
			esval[j] = 0; 
		}	
		printf("loop 5 end\n");
		for (j = 0; j < avalsize; j++) {
			esval[arow[j]] += aval[j] * x[acol[j]]; 
		}
		printf("loop 6 end\n");
		for (j = 0; j < b_size; j++) { 
			esval[j] -= b[j];
			esval[j] = fabs(esval[j]);
		} 
		//e = sum(es);
		printf("loop 7 end\n");
		e_total = 0;
		for (j = 0; j < b_size; j++) {
			e_total += esval[j];
		}
		printf("loop 8 end\n");

		dec = (e_pretotal - e_total) / e_pretotal;
		if (e_total < e_pretotal) {
			e_pretotal = e_total;
			for (j = 0; j < b_size; j++) {
				x_pre[j] = x[j];
			}
		} else { 
			printf("Failed. Trying gradient descent.. \n");
			//error: failed to reduce error via LS and projection
			//try gradient descent
			//grad = 2*A'*(A*x_pre-b);
			//esval をA * x_preの解をいれて利用する
			for (j = 0; j < b_size; j++) {
				esval[j] = 0; 
			}	
			for (j = 0; j < avalsize; j++) {
				esval[arow[j]] += aval[j] * x_pre[acol[j]]; 
			}
			for (j = 0; j < b_size; j++) { 
				esval[j] -= b[j];
			} 
			//A' * esval
			for (j = 0; j < avalsize; j++) {
				grad[arowtrans[j]] += avaltrans2[j] * esval[acoltrans[j]]; 
			}	

			//gradのサイズはA, bと同じ
			//for gamma = 0.1:-0.003:0.001
			for (j = 100; j > 0; j = j-3) {
				gamma = j / 1000;
				//x = x_pre - gamma*grad;
				for (k = 0; k < x0_size; k++) { 
					x[k] = x_pre[k] - gamma * grad[k];
				}

				//%x(x<lb) = lb; x(x>ub) = ub;
				//x = proj(A, b, x, lb, ub,  num1, num2);
				/*
				proj(arow, acol, aval,
					 b, x, 
					 valsize, b_size, x0_size,
					lb, ub, num1, num2);
				*/
				//es = abs(A*x-b);
				for (k = 0; k < b_size; k++) {
					esval[j] = 0; 
				}	
				for (k = 0; k < avalsize; k++) {
					esval[arow[k]] += aval[k] * x[acol[k]]; 
				}
				for (k = 0; k < b_size; k++) { 
					esval[k] -= b[k];
					esval[k] = fabs(esval[k]);
				} 
				//e = sum(es);
				e_total = 0;
				for (k = 0; k < b_size; k++) {
					e_total += esval[k];
				}

				if (e_total < e_pretotal) {
					//fprintf(['Succeeded: gamma=' num2str(gamma) '\n']);
					break;
				}
			}
			dec = (e_pretotal - e_total) / e_pretotal ;
			if (e_total < e_pretotal) {
				e_pretotal = e_total;
				for (j = 0; j < x0_size; j++) {
					x_pre[j] = x[j];
				}
			} else  { //%still fail
			    printf("Failed again..\n");
				break;
			}
		}
    
		printf("IRLS-Porj Iter #%d, error=%g (-%.3f%%) \n", i, e_total, dec*100); 
		if (dec < 0) {
			break;
		}
	}
	free(esval);
	free(wrow);
	free(wcol);
	free(wval);

	free(arowtrans);
	free(acoltrans);
	free(avaltrans);

	free(avaltrans2);

	free(awrow);
	free(awcol);
	free(awval);

	free(awarow);
	free(awacol);
	free(awaval);

	free(awbval);
	free(grad);
	free(x_pre);
	free(p);

}

//warning on;




