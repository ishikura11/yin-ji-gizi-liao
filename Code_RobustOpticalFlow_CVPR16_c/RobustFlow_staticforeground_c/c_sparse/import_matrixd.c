#include <stdio.h>
#include <stdlib.h>

void import_matrixd(double *array, int *valsize, char *fname) {
	FILE *fp; // FILE型構造体
	//char fname[] = "test.txt";
	char str[30];
 
	double d1;
	int arrcnt = 0;

	fp = fopen(fname, "r"); // ファイルを開く。失敗するとNULLを返す。
	if(fp == NULL) {
		printf("%s file not open!\n", fname);
		return;
	}
 
	//printf("\n");
	while(fgets(str, 30, fp) != NULL) {
		sscanf(str, "%lf", &d1);
		array[arrcnt] = d1;
		/*
		if (arrcnt < 5) {
			printf("array[%d] = %f\n", arrcnt, array[arrcnt]);
		}
		*/
		//printf("array = %d", array[arrcnt]);
		arrcnt++;
	}
 
	*valsize = arrcnt;
	fclose(fp); // ファイルを閉じる

	return;
}


