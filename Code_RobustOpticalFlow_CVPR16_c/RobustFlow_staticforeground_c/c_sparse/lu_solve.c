#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 4

extern double check_val(int *row, int *column, double *val, int p_row, int p_col, int val_size);
extern double check_val_p(int *row, int *column, double *val, int p_row, int p_col, int val_size);
extern void insert_matrix(int *row, int *column, double *val,
				   int valsize, 
				   int add_row, int add_column, double add_val );

extern void erase_matrix(int *row, int *col, double *val, int valsize, int set_p);

//void lu_solve(double a[N][N],double b[N],int p[N]){
void lu_solve(int *arow, int *acol, double *aval, int asize, int *valsize, double *b, int *p){
//void lu_solve(double a[N][N],double b[N],int p[N]){
	int i,j,k;
	double tmp;

	for(k = 0; k < asize - 1; k++){
		tmp=b[k];
		b[k] = b[p[k]];
		b[p[k]] = tmp; /*右辺の行変換*/
		for(i = k + 1; i < asize; i++){ /* 前進代入 */
			b[i] = b[i] + check_val(arow, acol, aval, i, k, *valsize) * b[k];
			//printf("checkval(%d, %d) = %f\n", k, i, check_val(arow, acol, aval, i, k, asize));
		}
	}
/*
	for (i = 0; i < asize; i++) {
		printf("b[%d] = %f\n", i, b[i]);
	}
*//*
	printf("valsize = %d\n", *valsize);
			printf("awarow = ");
			for (j = 0; j < *valsize; j++) { 
				printf("%d ", arow[j]);
			}
			printf("\n");

			printf("awacol = ");
			for (j = 0; j < *valsize; j++) { 
				printf("%d ", acol[j]);
			}
			printf("\n");

			printf("awaval = ");
			for (j = 0; j < *valsize; j++) { 
				printf("%f ", aval[j]);
			}
			printf("\n");
			*/
	if(check_val(arow, acol, aval, asize-1, asize-1, *valsize) == 0)
		printf("(%d, %d) 0 error1 \n", asize-1, asize-1);
	b[asize-1] = b[asize-1] / check_val(arow, acol, aval, asize-1, asize-1, *valsize);
	for(k = asize - 2; k >= 0; k--){ /* 後退代入 */
		tmp = b[k];
		for(j = k + 1; j < asize; j++){
		/*
		 * lu_decompと同様行ごとに処理を行うため
		 * 高速化できそう
		 * check_val関数の内部でやってることと変わらないので高速化されない?
		 */
			
			tmp -= check_val(arow, acol, aval, k, j, *valsize) * b[j];
		}
		if(check_val(arow, acol, aval, k, k, *valsize) == 0)
			printf("(%d, %d) 0 error2\n", k, k);
		b[k] = tmp / check_val(arow, acol, aval, k, k, *valsize);
		//printf("b[%d] = %f\n", k, b[k]);
	}
}
