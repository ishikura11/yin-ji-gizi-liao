#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define N 4
#define BIG_NUM 99999999

//参考 
//http://ktm11.eng.shizuoka.ac.jp/lesson/2012/ip/lecture_11.pdf

extern double check_val(int *row, int *col, double *val, int p_row, int p_col, int valsize);
extern int check_val_p(int *row, int *col, double *val, int p_row, int p_col, int valsize);

extern void insert_matrix(int *row, int *col, double *val,
						  int valsize, 
						  int add_row, int add_col, double add_val );

extern void erase_matrix(int *row, int *col, double *val, int valsize, int set_p);

void lu_decomp(int *arow, int *acol, double *aval, int asize, int *valsize, int *p){
	/*
	arow, acol, aval ... 'ax = b'の行列aを座標で表したもの
	asize			 ... 行列aの行列の行, 列の長さ
	valsize			 ... 行列aの非0要素の数
	p				 ... LU分解する際に利用するピボット
	matlabの関数lu(A)の解を返す
*/
	int i,j,k, l,ip, tmp_p, tmp_p2, check = 0;
	double alpha, tmp, tmp2, tmp_kj;
	double amax, eps=pow(2.0,-50.0); /* eps=2^{-50}とする */
	//asizeは行列の長さ
	printf("valsize = %d\n", *valsize);
	for(k = 0; k < asize-1; k++){
		amax = fabs(check_val(arow, acol, aval, k, k, *valsize));
		printf("amax = %f k = %d valsize = %d\n", amax, k, *valsize);
		ip = k; /* ピボットの選択 */
		for (i = 0; i < *valsize; i++) {
			if (aval[i] == 0.) {
				printf("aval[%d] = 0, k = %d\n", i, k);
				return;
			}
		}
		if (amax == 0) {
			printf("check_val_p = %d\n", check_val_p(arow, acol, aval, k, k, *valsize));
			printf("k = %d\n", k);
		}
		for(i = k + 1; i < asize; i++){
			if(fabs(check_val(arow, acol, aval, i, k, *valsize)) > amax){
				amax = fabs(check_val(arow, acol, aval, i, k, *valsize));
				//printf("amax = %f k = %d i = %d valsize = %d\n", amax, k, i, valsize);
				ip=i;
			}
		}
		if(amax < eps) { // 正則性の判定 
			printf("入力した行列は正則ではない!!\n"); 
			printf("amax = %f\n", amax);
			exit(1);
		}
		//ここまで問題なし
		p[k]=ip;
		if(ip != k){
			for(j = 0; j < asize; j++){
				tmp_p  = check_val_p(arow, acol, aval, k, j, *valsize);
				tmp_p2 = check_val_p(arow, acol, aval, ip, j, *valsize);
				if (tmp_p == BIG_NUM) { //(k, j)座標の値が0 
					if(tmp_p2 != BIG_NUM) { //(ip, j)座標の値が0でない
						tmp2 = aval[tmp_p2];
						erase_matrix(arow, acol, aval, *valsize, tmp_p2);
						*valsize = *valsize - 1;
						insert_matrix( arow, acol, aval,
									   *valsize, 
									   k, j, tmp2);
						*valsize = *valsize + 1;
					}
				} else if (tmp_p2 == BIG_NUM) { //(ip, j)の値が0
					if (tmp_p != BIG_NUM) { //間違いなし
						tmp = aval[tmp_p];
						insert_matrix( arow, acol, aval,
									   *valsize, 
									   ip, j, tmp);
						*valsize = *valsize + 1;
						erase_matrix(arow, acol, aval, *valsize, tmp_p);
						*valsize = *valsize - 1;
						
					}
				} else { //(k, j), (ip, j)の座標の値二つとも0でないとき
					tmp = aval[tmp_p];
					tmp2 = aval[tmp_p2];
					aval[tmp_p] = tmp2; 
					aval[tmp_p2] = tmp;
				}
			}
		}
		for(i = k + 1; i < asize; i++){ /* 前進消去 */
			alpha = check_val(arow, acol, aval, i, k, *valsize) / 
					check_val(arow, acol, aval, k, k, *valsize);
			tmp_p  = check_val_p(arow, acol, aval, i, k, *valsize);
			if(alpha != 0.) {// alpha = 0なら実行する必要がない 
				if(tmp_p == BIG_NUM) { //(i, k)座標の値が0のとき
					//そもそもalpha != 0ならばtmp_p != 0を表すのでこの分岐いらない?
					insert_matrix( arow, acol, aval,
								   *valsize, 
								   i, k, alpha);
					*valsize = *valsize + 1;
				} else {
					aval[tmp_p] = alpha;
				}
			//}
				for(j = k + 1; j < asize; j++){
					/* 
					 * n行目のもののみ扱う配列なのでn列目の
					 * いちいちcheck_val関数を使わずとも行単位で
					 * 処理を行うことで高速化できそう
					 */
					tmp_p = check_val_p(arow, acol, aval, i, j, *valsize);
					tmp_kj = check_val(arow, acol, aval, k, j, *valsize); 
					
					if (tmp_p == BIG_NUM && tmp_kj != 0) {
						insert_matrix( arow, acol, aval,
										   *valsize, 
										   i, j, alpha * tmp_kj);
						*valsize = *valsize + 1;
					} else if (tmp_p != BIG_NUM) {
						aval[tmp_p] -= alpha * tmp_kj;
						if (aval[tmp_p] == 0) { 
							erase_matrix(arow, acol, aval, *valsize, tmp_p);
							*valsize = *valsize - 1;
						}
					}
				}
			}
		}
	}
}
