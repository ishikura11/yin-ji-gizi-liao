#include <stdio.h>
#include <stdlib.h>

#define BIG_NUM 99999999

extern void insert_matrix(int *row, int *col, double *val,
						  int val_size, 
						  int add_row, int add_col, double add_val);

extern void erase_matrix(int *row, int *col, double *val, int val_size, int set_p);

extern void plus_matrix(int *row1, int *col1, double *val1, 
				 int *row2, int *col2, double *val2,
				 int valsize1, int valsize2,
				 int *out_matsize);

extern void multi_matrix(int *row1, int *col1, double *val1, 
				 int *row2, int *col2, double *val2,
				 int *out_row, int *out_col, double *out_val,
				 int rowsize1, int valsize1,
				 int colsize2, int valsize2,
				 int *out_rowsize, int *out_colsize, int *out_valsize);

extern void transposition_matrix(int *row, int *col, double *val,
						  int *ans_row, int *ans_col, double *ans_val,	
						  int val_size);

void print_arrd(double *arr, int arr_size) { 
	int i;
	for (i = 0; i < arr_size; i++) {
		if (arr[i] == 0.) break;
		printf("%f ", arr[i]);
	}
	printf("\n");
	return;
}

void print_arri(int *arr, int arr_size) { 
	int i;
	for (i = 0; i < arr_size; i++) {
		if (arr[i] == BIG_NUM) break;
		printf("%d ", arr[i]);
	}
	printf("\n");
	return;
}

extern void import_matrix(int *array, int *valsize, char *fname);

int main() {
	int i, j, valsize;
	double *val, *val1, *val2;
	int *row, *col, *row1, *col1, *row2, *col2;
	int sparse_num = 9;
	int *out_rowsize;
	int *out_colsize;
	int *out_matsize;
	
	out_rowsize = (int *)malloc(sizeof(int));
	out_colsize = (int *)malloc(sizeof(int));
	out_matsize = (int *)malloc(sizeof(int));

	row = (int *)malloc(sizeof(int) * sparse_num);
	col = (int *)malloc(sizeof(int) * sparse_num);
	val = (double *)malloc(sizeof(double) * sparse_num);

	row1 = (int *)malloc(sizeof(int) * sparse_num);
	col1 = (int *)malloc(sizeof(int) * sparse_num);
	val1 = (double *)malloc(sizeof(double) * sparse_num);

	row2 = (int *)malloc(sizeof(int) * sparse_num);
	col2 = (int *)malloc(sizeof(int) * sparse_num);
	val2 = (double *)malloc(sizeof(double) * sparse_num);

	//初期化
	for (i = 0; i < sparse_num; i++ ) {
		row[i]  = BIG_NUM; col[i]  = BIG_NUM; val[i]  = 0;
		row1[i] = BIG_NUM; col1[i] = BIG_NUM; val1[i] = 0;
		row2[i] = BIG_NUM; col2[i] = BIG_NUM; val2[i] = 0;
	}

	/*
	row[0] = 0, row[1] = 1, row[2] = 2;
	col[0] = 1, col[1] = 0, col[2] = 2;
	val[0] = 1, val[1] = 2, val[2] = 3;

	row1[0] = 0, row1[1] = 0, row1[2] = 1;
	col1[0] = 0, col1[1] = 1, col1[2] = 0;
	val1[0] = 4, val1[1] = 5, val1[2] = 6;
*/
	
	row[0] = 0, row[1] = 0, row[2] = 0;
	col[0] = 0, col[1] = 1, col[2] = 2;
	val[0] = 1, val[1] = 2, val[2] = 3;
	
	row[3] = 1, row[4] = 1, row[5] = 1;
	col[3] = 0, col[4] = 1, col[5] = 2;
	val[3] = 4, val[4] = 5, val[5] = 6;
	
	row[6] = 2, row[7] = 2, row[8] = 2;
	col[6] = 0, col[7] = 1, col[8] = 2;
	val[6] = 7, val[7] = 8, val[8] = 9;

	row1[0] = 0, row1[1] = 1, row1[2] = 2;
	col1[0] = 0, col1[1] = 1, col1[2] = 2;
	val1[0] = 1, val1[1] = 2, val1[2] = 3;
/*	 
	row1[3] = 1, row1[4] = 1, row1[5] = 1;
	col1[3] = 0, col1[4] = 1, col1[5] = 2;
	val1[3] = 4, val1[4] = 5, val1[5] = 6;
	
	row1[6] = 2, row1[7] = 2, row1[8] = 2;
	col1[6] = 0, col1[7] = 1, col1[8] = 2;
	val1[6] = 7, val1[7] = 8, val1[8] = 9;
*/

	printf("row = ");
	print_arri(row1, sparse_num);

	printf("col = ");
	print_arri(col1, sparse_num);
	
	printf("val = ");
	print_arrd(val1, sparse_num);

	/*
	printf("row1 = ");
	print_arri(row1, sparse_num);

	printf("col1 = ");
	print_arri(col1, sparse_num);
	
	printf("val1 = ");
	print_arrd(val1, sparse_num);
*/

// insert test

insert_matrix(row1, col1, val1, 
			  9,
			  0, 2, 6);

insert_matrix(row1, col1, val1, 
			  9,
			  1, 2, 8);

insert_matrix(row1, col1, val1, 
			  9,
			  0, 1, 10);

	printf("ans row = ");
	print_arri(row1, 6);

	printf("ans col = ");
	print_arri(col1, 6);
	
	printf("ans val = ");
	print_arrd(val1, 6);
	
erase_matrix(row1, col1, val1, 9, 4);
erase_matrix(row1, col1, val1, 9, 2);
erase_matrix(row1, col1, val1, 9, 1);

//erase_matrix(row, col, val, sparse_num, 1);

/*
	plus_matrix(row, col, val, 
				row1, col1, val1, 
			   	sparse_num, sparse_num, out_matsize);
*/
	/*
	multi_matrix(row, col, val, 
				row1, col1, val1, 
				row2, col2, val2, 
				sparse_num, sparse_num, 
				3, 3,
				out_rowsize, out_colsize, out_matsize);
*/
/*
	transposition_matrix(row , col , val, 
						 row2, col2, val2,
						 sparse_num);
*/
//char fname[] = "hoge.txt";
//char fname[] = "L1min_Arow.dat";

//import_matrix(row2, &valsize, fname);
//import_matrix(col2, &valsize, fname);

	printf("ans row = ");
	print_arri(row1, sparse_num);

	printf("ans col = ");
	print_arri(col1, sparse_num);
	
	printf("ans val = ");
	print_arrd(val1, sparse_num);
	
	//printf("ans valsize = %d\n", valsize);

	free(row);
	free(col);
	free(val);
	free(row1);
	free(col1);
	free(val1);
	free(row2);
	free(col2);
	free(val2);
	free(out_rowsize);
	free(out_colsize);
	free(out_matsize);

	return 0;
}
