#include <stdio.h>
#include <stdlib.h>

//
void export_matrixi(int *array, int arrsize, char *fname) {
	FILE *fp;
	int i;

	if((fp = fopen(fname, "w")) != NULL){
		for(i = 0; i < arrsize; i++){
			//printf("array[%d] = %f\n", i, array[i]);
			if(fprintf(fp, "%d\n", array[i]) < 0){
				//書き込みエラー
				break;
			}
		}
	fclose(fp);
	}else{
	//ファイルオープンエラー
	}
}
