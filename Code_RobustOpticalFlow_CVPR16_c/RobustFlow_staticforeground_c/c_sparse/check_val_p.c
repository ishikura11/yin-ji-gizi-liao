//(p_row, p_col)の座標にある値の疎行列における座標を返す
#include <stdio.h>

#define BIG_NUM 99999999

int check_val_p(int *row, int *col, double *val, int p_row, int p_col, int valsize) {
	int i, j;
	for (i = 0; i < valsize; i++) { 
		if (row[i] == p_row) {
			for (j = i; j < valsize; j++) {
				if( row[j] == p_row && col[j] == p_col) {
					//printf("row[%d] = %d, p_row = %d, col[%d] = %d, p_col = %d\n", 
					//		j, row[j], p_row, j, col[j], p_col);
					return j;
				} else if (row[j] != p_row || val[j] == 0) return BIG_NUM;
			}
		} else if (row[i] > p_row || val[i] == 0) return BIG_NUM;
	}
	return BIG_NUM;
}
