#include <stdio.h>

extern void insert_matrix(int *row, int *col, double *val,
						  int val_size, 
						  int add_row, int add_col, double add_val);

void transposition_matrix(int *row, int *col, double *val, 
						  int *ans_row, int *ans_col, double *ans_val, 
						  int val_size) {
	int i, j;
	int subvalsize = 0;
	for (i = 0; i < val_size; i++) {
		insert_matrix(ans_row, ans_col, ans_val, 
					  subvalsize,
					  col[i], row[i], val[i]);
		subvalsize++;
	}
}
