#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#define N 100
#include "lusub.h"

void lu_solve_sub(double a[N][N],double b[N],int p[N]){
	int i,j,k;
	double tmp;
	for(k = 0; k < N; k++){
		//tmp = b[k]; b[k] = b[p[k]]; b[p[k]] = tmp; /*右辺の行変換*/
		tmp = b[p[k]];
		for(i = k+1; i < N; i++){ /* 前進代入 */
			//b[i] -= a[i][k] * b[k];
			tmp -= a[k][i] * b[i];
		}
		b[k] = tmp;
	}

	//b[N-1] /= a[N-1][N-1];
	
	for(k = N-1; k >= 0; k--){ /* 後退代入 */
		tmp = b[k];
		for(j = k+1; j < N; j++){
			tmp -= a[k][j] * b[j];
		}
		b[k] = tmp / a[k][k];
	}
}
