#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "lusub.h"
//#define N 4 /* N次正方行列 */
//void input_matrix(double a[N][N],char c,FILE* fin, FILE* fout);
//void input_vector(double b[N],char c,FILE* fin,FILE* fout);
extern void lu_decomp_sub(double a[N][N],int p[N]);
extern void lu_solve_sub(double a[N][N],double b[N],int p[N]);
extern void import_matrixi(int *array, int *valsize, char *fname);
extern void import_matrixd(double *array, int *valsize, char *fname);
extern void export_matrixd(double *array, int arrsize, char *fname);
extern void export_matrixi(int *array, int arrsize, char *fname);

//extern int luDecomposition(int n, int *pivot, double *matrix);
//extern void luSolver(int n, int *pivot, double *matrix, double *b);

int main(void){
	FILE *fin, *fout;
	double a[N][N], b[N], *aval, *b_sub;
	int i, j, I, p[N], *arow, *acol, valsize, b_size; /* p[0…N-2]を利用, p[N-1]は未使用 */
	    //if((fin=fopen("input_lu.dat","r"))==NULL) exit(1);
		 //if((fout=fopen("output_lu.dat","w"))==NULL) exit(1);
		  //input_matrix(a,’A’,fin,fout); input_vector(b,’b’,fin,fout);
	printf("hogefuga\n");
	arow = (int *)malloc(sizeof(int) * N*N);
	acol = (int *)malloc(sizeof(int) * N*N);
	aval = (double *)malloc(sizeof(double) * N * N);
	b_sub = (double *)malloc(sizeof(double) * N);

	for (i = 0; i < N; i++) {
		b_sub[i] = 0.;
	}

	for (i = 0; i < N; i++) {
		arow[i] = 0;
		acol[i] = 0;
		aval[i] = 0.;
	}
	
	for (i = 0; i < N; i++) {
		for (j =0; j < N; j++){ 
			a[i][j] = 0;
		}
		b[i] = 0;
		p[i] = i;
	}

	printf("hoge\n");
	/*
	import_matrixi(arow,&valsize,"./dataset_sparse/awaawb3/L1min_awarow.dat");
	import_matrixi(acol,&valsize,"./dataset_sparse/awaawb3/L1min_awacol.dat");
	import_matrixd(aval,&valsize,"./dataset_sparse/awaawb3/L1min_awaval.dat");
	import_matrixd(b_sub, &b_size,"./dataset_sparse/awaawb3/L1min_awb.dat");
*/
	/*
	import_matrixi(arow,&valsize,"./dataset_sparse/10dataset/L1min_Arow.dat");
	import_matrixi(acol,&valsize,"./dataset_sparse/10dataset/L1min_Acol.dat");
	import_matrixd(aval,&valsize,"./dataset_sparse/10dataset/L1min_Aval.dat");
	import_matrixd(b_sub, &b_size,"./dataset_sparse/10dataset/L1min_b.dat");
	*/
	/*
	import_matrixi(arow,&valsize,"./dataset_sparse/make_sprand6/sprand_sample_row2.dat");
	import_matrixi(acol,&valsize,"./dataset_sparse/make_sprand6/sprand_sample_col2.dat");
	import_matrixd(aval,&valsize,"./dataset_sparse/make_sprand6/sprand_sample_val2.dat");
	import_matrixd(b_sub, &b_size,"./dataset_sparse/make_sprand6/sprand_sample_b2.dat");
	
	for(i = 0; i < valsize; i++) {
		a[arow[i]][acol[i]] = aval[i];
		//printf("i = %d, arow = %d, acol = %d, aval = %f\n", i, arow[i], acol[i], aval[i]);
	}
	
	for (i = 0; i < N; i++) {
		b[i] = b_sub[i];
		//b[i] = i + 1;
		//printf("b[%d] = %f\n", i, b[i]);
	}
*/
	/*
	a[0][0] = 4, a[0][1] = 1,  a[0][3] = 4, a[0][4] = 1, a[0][7] = 2, a[0][9] = 2;
	a[1][1] = 2, a[1][2] = 1,  a[1][4] = 5, a[1][6] = 8, a[1][7] = 3, a[1][8] = 2;
	a[2][2] = 3, a[2][3] = 1,  a[2][4] = 4, a[2][6] = 7, a[2][8] = 4, a[2][9] = 3;
	a[3][0] = 3, a[3][3] = 5,  a[3][4] = 3, a[3][5] = 6, a[3][6] = 3, a[3][7] = 8, a[3][8] = 2, a[3][9] = 9;
	a[4][0] = 2, a[4][1] = 12, a[4][2] = 2, a[4][4] = 2, a[4][5] = 5, a[4][6] = 11, a[4][8] = 1, a[4][9] = 2;
	
	a[5][0] = 2, a[5][1] = 2, a[5][4] = 3,  a[5][5] = 4, a[5][7] = 9, a[5][9] = 2;
	a[6][1] = 4, a[6][2] = 4, a[6][3] = 1,  a[6][4] = 9, a[6][6] = 1, a[6][7] = 3;
	a[7][3] = 9, a[7][5] = 2, a[7][6] = 14, a[7][7] = 3, a[7][9] = 3;//, a[7][3] = 1;
	a[8][2] = 5, a[8][4] = 4, a[8][5] = 7,  a[8][7] = 5, a[8][8] = 8;//, a[8][3] = 1;
	a[9][0] = 6, a[9][1] = 5, a[9][2] = 1,  a[9][4] = 3, a[9][5] = 7, a[9][7] = 8, a[9][8] = 1, a[9][9] = 5;
	
	b[0] = 4, b[1] = 4, b[2] = -4, b[3] = 2, b[4] = 5;
	b[5] = 4, b[6] = 9, b[7] = 7, b[8] = 11, b[9] = 2;
	*/
	/*
	for(i = 0; i < N;i++) {
		for(j = 0; j< N; j++) {
			a[i][j] = i * 5 + j + 1;
		}
		b[i] = i + 1;
	}
	a[0][0] = 5;
	a[4][4] = 19;
	*/
/*
	a[0][0] = 1, a[0][3] = 1, a[0][4] = 3, a[1][0] = 2, a[1][1] = 1;
	a[1][4] = 2, a[2][1] = 1, a[2][2] = -1, a[2][3] = 2, a[2][4] = 2;
	a[3][0] = 3, a[3][3] = 2, a[4][2] = 3, a[4][3] = 4, a[4][4] = 3;
	
	b[0] = 1, b[1] = 1, b[2] = 1, b[3] = 1, b[4] = 1;
*/
	/*
	printf("a = \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) { 
			printf("%f ", i, j, a[i][j]);
		}
		printf("\n");
	}
*/
	/*
	a[0][0] = 1;a[0][1] = 2;a[0][2] = 3;
	a[1][0] = 4;a[1][1] = 5;a[1][2] = 6;
	a[2][0] = 7;a[2][1] = 8;a[2][2] = 9;
	b[0] = 1;b[1] = 2;b[2] = 3;
	*/
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			a[i][j] = i * N + j + 1;
		}
		b[i] = i + 1;
	}
/*
	a[0][0] = 1;a[0][1] = 2;a[1][0] = 3;a[1][1] = 4;
	b[0] = 1;b[1] = 2;
*/
	//luDecomposition(N, p, a);
	lu_decomp_sub(a,p);
	
	for (i = 0; i < N; i ++ ) { 
		for (j = 0; j < N; j++) {
			printf(" %f", a[i][j]);
		}
		printf("\n");
	}
	
	printf("p = ");
	for(i = 0; i < N; i++) {
		printf("%d ",p[i]);
		//fprintf(fout, "%f¥n",b[i]);
	}
	printf("\n");
/*
	export_matrixi(arow, valsize, "arow.txt");
	export_matrixi(acol, valsize, "acol.txt");
	export_matrixd(aval, valsize, "aval.txt");
*/
/*	
	printf("after a = \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) { 
			printf("%f ", i, j, a[i][j]);
		}
		printf("\n");
	}
*/
	//luSolver(N, p, a, b);
	lu_solve_sub(a,b,p);
	export_matrixd(b, N, "ansx.txt");
	
	printf("Ax=bの解は次の通りです¥n");
	for(i=0;i<N;i++){ printf("%f¥n",b[i]);}
			//fclose(fin); fclose(fout);
	free(arow);
	free(acol);
	free(aval);
	free(b_sub);
	return 0;
} 
