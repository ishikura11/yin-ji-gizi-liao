#include <stdio.h>
#include <stdlib.h>

void import_matrixi(int *array, int *valsize, char *fname) {
	FILE *fp; // FILE型構造体
	//char fname[] = "test.txt";
	char str[10];
	int i, j;
	int d1;
	int arrcnt = 0;

	fp = fopen(fname, "r"); // ファイルを開く。失敗するとNULLを返す。
	if(fp == NULL) {
		printf("%s file not open!\n", fname);
		return;
	}
 
	while(fgets(str, 10, fp) != NULL) {
		sscanf(str, "%d", &d1);
		//printf("d1 = %d\n", d1);
		array[arrcnt] = d1;
		//printf("array = %d", array[arrcnt]);
		arrcnt++;
	}
 
	*valsize = arrcnt;

	fclose(fp); // ファイルを閉じる

	return;
}
