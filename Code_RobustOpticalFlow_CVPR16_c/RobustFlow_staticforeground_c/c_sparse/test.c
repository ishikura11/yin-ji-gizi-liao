#include <stdio.h>
#include <stdlib.h>

void insert(double *ptr, double *last, double val)
{
	    for(double *dst = last - 1; --dst >= ptr; )
			        *(dst + 1) = *dst;      // 要素をひとつ後ろにずらす
		    *ptr = val;
}
int main() {
	int i;
	double *v;
	v = (double *)malloc(sizeof(double) * 50000);
	v[0] = 3;
	*(v+1) = 1;
	v[2] = 4, v[3] = 1, v[4] = 5;
	printf("size = %d size = %d\n", sizeof(v), sizeof(v[0]));
	printf("v = ");
	
	for (i = 0; i < 5; i++) {
		printf("%f ", v[i]);
	}
	printf("\n");
	insert(v+2, v + 6, 2);      // [2] の位置に 2 を挿入。最後の 5 は破棄
	printf("v = ");
	
	for (i = 0; i < 6; i++) {
		printf("%f ", v[i]);
	}
	printf("\n");
	
	free(v);
	return 0;
}
