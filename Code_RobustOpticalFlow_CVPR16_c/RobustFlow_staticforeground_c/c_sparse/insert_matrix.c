#include <stdio.h>

//疎行列row, col, valにadd_row, add_col, add_val を挿入する

void insertd(double *ptr, double *last, double val)
{
	    for(double *dst = last; --dst >= ptr; ) { 
			*(dst + 1) = *dst;      // 要素をひとつ後ろにずらす
		}
		*ptr = val;
}

void inserti(int *ptr, int *last, int val)
{
	    for(int *dst = last; --dst >= ptr; ) { 
			*(dst + 1) = *dst;      // 要素をひとつ後ろにずらす
		}
		*ptr = val;
}

void insert_matrix(int *row, int *col, double *val,
				   int valsize, 
				   int add_row, int add_col, double add_val ){

	int i, j, set_p = 0, col_flag = 0;
	for (i = 0; i < valsize; i++) {
		if(add_row > row[i] && val[i] != 0) {
			set_p = i+1; //set_p番目に挿入する
		} else if(add_row == row[i]) {
			col_flag = 1; //col部分に関してもも動かす必要があるか
			break;
		} else { //(add_row < row[i]) 
			break;
		}
	}

	if(col_flag) { 
		for (i = set_p; i < valsize; i++) {
			if(add_col > col[i] && add_row == row[i]) {
				set_p = i+1;
				//printf("col set_p = %d\n", set_p);
			} else 
				break;
		}
	}
	
	inserti(row + set_p, row + valsize, add_row);
	inserti(col + set_p, col + valsize, add_col);
	insertd(val + set_p, val + valsize, add_val);

}


