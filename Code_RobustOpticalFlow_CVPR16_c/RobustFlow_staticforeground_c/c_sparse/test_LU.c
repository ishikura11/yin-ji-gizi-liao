#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#define N 4 /* N次正方行列 */
#define BIG_NUM 99999999

//extern void input_matrix(double a[N][N],char c,FILE* fin, FILE* fout);
//extern void input_vector(double b[N],char c,FILE* fin,FILE* fout);
//extern void lu_decomp(double a[N][N],int p[N]);
//extern void lu_solve(double a[N][N],double b[N],int p[N]);
extern void lu_decomp(int *arow, int *acol, double *aval, int asize, int *valsize, int *p);
extern void lu_solve(int *arow, int *acol, double *aval, int asize, int *valsize, double *b, int *p);
extern double check_val(int *row, int *column, double *val, int p_row, int p_col, int val_size);
extern int check_val_p(int *row, int *column, double *val, int p_row, int p_col, int val_size);
extern void import_matrixi(int *array, int *valsize, char *fname);
extern void import_matrixd(double *array, int *valsize, char *fname);
extern void export_matrixi(double *array, int arrsize, char *fname);
extern void export_matrixd(int *array, int arrsize, char *fname);

void print_arrd(double *arr, int arr_size) { 
	int i;
	for (i = 0; i < arr_size; i++) {
		if (arr[i] == 0.) break;
		printf("%f ", arr[i]);
	}
	printf("\n");
	return;
}

void print_arri(int *arr, int arr_size) { 
	int i;
	for (i = 0; i < arr_size; i++) {
		if (arr[i] == BIG_NUM) break;
		printf("%d ", arr[i]);
	}
	printf("\n");
	return;
}


int main(void){
	//FILE *fin, *fout;
	double a[N][N], *b, *aval;
	int i, j, I, *p; /* p[0…N-2]を利用, p[N-1]は未使用 */
	int *arow, *acol, asize = 1000, msize = asize * asize, valsize, b_size;
	
	arow = (int *)malloc(sizeof(int) * msize);
	acol = (int *)malloc(sizeof(int) * msize);
	aval = (double *)malloc(sizeof(double) * msize);
	p = (int *)malloc(sizeof(int) * asize);
	b = (double *)malloc(sizeof(double) * asize);


	//if((fin=fopen("input_lu.dat","r"))==NULL) exit(1);
	//if((fout=fopen("output_lu.dat","w"))==NULL) exit(1);
	//input_matrix(a, 'A' ,fin,fout); input_vector(b, 'b' ,fin,fout);
	/*
	for (i = 0; i < N; i++) {
		for (j =0; j < N; j++){ 
			a[i][j] = 0;
		}
		b[i] = 0;
	}
	a[0][0] = 1, a[0][1] = 2, a[0][2] = 3, a[0][3] = 4;
	a[1][1] = 1, a[1][2] = 7, a[1][3] = 8;
	a[2][2] = 1, a[2][3] = 1, a[3][3] = 1;
	b[0] = 30, b[1] = 55, b[2] = 7, b[3] = 4;
	*/
	for (i = 0; i < asize; i++) {
		p[i] = 0;
		b[i] = 0.;
	}

	for (i = 0; i < msize; i++) {
		arow[i] = BIG_NUM;
		acol[i] = BIG_NUM;
		aval[i] = 0.;
	}
/*
	for (i = 0; i < asize;i++) {
		for (j = 0; j < asize; j++ ) {
			arow[i * 5 + j] = i;
			acol[i * 5 + j] = j;
			aval[i * 5 + j] = i * 5 + j + 1;
		}
		b[i] = i + 1;
	}
	valsize = 25;
	aval[0] = 5;
	aval[24] = 19;
*/
	/*
	arow[0] = 0, arow[1] = 0, arow[2] = 0, arow[3] = 0;
	acol[0] = 0, acol[1] = 1, acol[2] = 2, acol[3] = 3;
	aval[0] = 1, aval[1] = 2, aval[2] = 3, aval[3] = 4;

	arow[4] = 1, arow[5] = 1, arow[6] = 1, arow[7] = 2;
	acol[4] = 1, acol[5] = 2, acol[6] = 3, acol[7] = 2;
	aval[4] = 1, aval[5] = 7, aval[6] = 8, aval[7] = 1;

	arow[8] = 2, arow[9] = 3;
	acol[8] = 3, acol[9] = 3;
	aval[8] = 1, aval[9] = 1;

	b[0] = 30, b[1] = 55, b[2] = 7, b[3] = 4;
	valsize = 10;
*/
	/*
	import_matrixi(arow, &valsize, "./dataset_sparse/awaawb2/L1min_awarow.dat");
	import_matrixi(acol, &valsize, "./dataset_sparse/awaawb2/L1min_awacol.dat");
	import_matrixd(aval, &valsize, "./dataset_sparse/awaawb2/L1min_awaval.dat");
	import_matrixd(b, &b_size,	   "./dataset_sparse/awaawb2/L1min_awb.dat");
	*/
	
	import_matrixi(arow, &valsize, "./dataset_sparse/make_sprand8/sprand_sample_row3.dat");
	import_matrixi(acol, &valsize, "./dataset_sparse/make_sprand8/sprand_sample_col3.dat");
	import_matrixd(aval, &valsize, "./dataset_sparse/make_sprand8/sprand_sample_val3.dat");
	import_matrixd(b, &b_size, "./dataset_sparse/make_sprand8/sprand_sample_b3.dat");
	
	
	/*
	arow[0] = 0, arow[1] = 0,  arow[2] = 0, arow[3] = 1, arow[4] = 1; 
	acol[0] = 0, acol[1] = 3,  acol[2] = 4, acol[3] = 0, acol[4] = 1;
	aval[0] = 1, aval[1] = 1,  aval[2] = 3, aval[3] = 3, aval[4] = 1;

	arow[5] = 1, arow[6] = 2, arow[7] = 2,  arow[8] = 2, arow[9] = 2;
	acol[5] = 4, acol[6] = 1, acol[7] = 2,  acol[8] = 3, acol[9] = 4;
	aval[5] = 2, aval[6] = 3, aval[7] = -1, aval[8] = 2, aval[9] = 2;

	arow[10] = 3, arow[11] = 3, arow[12] = 4, arow[13] = 4, arow[14] = 4;
	acol[10] = 0, acol[11] = 3, acol[12] = 2, acol[13] = 3, acol[14] = 4;
	aval[10] = 3, aval[11] = 2, aval[12] = 3, aval[13] = 4, aval[14] = 1;
	b[0] = 1, b[1] = 2, b[2] = 3, b[3] = 4, b[4] = 5;
	valsize = 15;
	*/
/*
	arow[15] = 1, arow[16] = 2, arow[17] = 2, arow[18] = 2, arow[19] = 3;
	arow[15] = 1, arow[16] = 2, acol[17] = 2, acol[18] = 3, acol[19] = 0;
	arow[15] = 1, arow[16] = 2, arow[17] = 2, aval[18] = 2, aval[19] = 3;
	
	arow[20] = 3, arow[21] = 1, arow[22] = 2, arow[23] = 2, arow[23] = 2;
	acol[20] = 3, acol[21] = 1, acol[22] = 2, acol[23] = 2, acol[23] = 2;
	aval[20] = 3, aval[21] = 1, aval[22] = 2, aval[23] = 2, aval[23] = 2;

	arow[25] = 2, arow[26] = 2, arow[27] = 1, arow[28] = 2, arow[29] = 2;
	acol[25] = 1, acol[26] = 2, acol[27] = 1, acol[28] = 2, acol[29] = 2;
	aval[25] = 1, aval[26] = 1, aval[27] = 1, aval[28] = 2, aval[29] = 2;

	arow[30] = 3, arow[21] = 1, arow[22] = 2, arow[23] = 2, arow[23] = 2;
	acol[30] = 3, acol[21] = 1, acol[22] = 2, acol[23] = 2, acol[23] = 2;
	aval[30] = 3, aval[21] = 1, aval[22] = 2, aval[23] = 2, aval[23] = 2;

	arow[35] = 2, arow[26] = 2, arow[27] = 1, arow[28] = 2, arow[29] = 2;
	acol[35] = 1, acol[26] = 2, acol[27] = 1, acol[28] = 2, acol[29] = 2;
	aval[35] = 1, aval[26] = 1, aval[27] = 1, aval[28] = 2, aval[29] = 2;

	arow[40] = 3, arow[21] = 1, arow[22] = 2, arow[23] = 2, arow[23] = 2;
	acol[40] = 3, acol[21] = 1, acol[22] = 2, acol[23] = 2, acol[23] = 2;
	aval[40] = 3, aval[21] = 1, aval[22] = 2, aval[23] = 2, aval[23] = 2;

	arow[45] = 2, arow[26] = 2, arow[27] = 1, arow[28] = 2, arow[29] = 2;
	acol[45] = 1, acol[26] = 2, acol[27] = 1, acol[28] = 2, acol[29] = 2;
	aval[45] = 1, aval[26] = 1, aval[27] = 1, aval[28] = 2, aval[29] = 2;


	b[0] = 4, b[1] = 4, b[2] = -4, b[3] = 2, b[4] = 5;
	b[5] = 4, b[6] = 9, b[7] = 7, b[8] = 11, b[9] = 2;
*/
	/*
	for (i = 0; i < msize; i++) {
		if (aval[i] == 0) break;
		printf("arow = %d, acol = %d, aval = %f\n", arow[i], acol[i], aval[i]);
	}
	*/
	/*
	for (i = 0; i < asize; i++) {
		for (j = 0; j < asize; j++) {
			//if((check_val_p(arow, acol, aval, i, j, valsize)) != BIG_NUM)
				//printf("(%d, %d) = %f\n", i, j, check_val(arow, acol, aval, i, j, valsize));
				printf("checkvalp(%d, %d) = %d\n", i, j, check_val_p(arow, acol, aval, i, j, valsize));
				
				printf("(%d, %d) = %f\n", i, j, aval[(check_val_p(arow, acol, aval, i, j, valsize))]);
		}
	}	
	*/
	/*
	for (i = 0; i < asize; i++) {
		for (j = 0; j < asize; j++) {
			if (check_val(arow, acol, aval, i, j, valsize) != 0)
				printf("(%d, %d) = %f\n", i, j, check_val(arow, acol, aval, i, j, valsize));
		}
	}	
	*/
	//printf("(%d, %d) = %f\n", 1, 0, aval[(check_val_p(arow, acol, aval, 1, 0, msize))]);
	/*
	printf("ans row = ");
	print_arri(arow, msize);

	printf("ans col = ");
	print_arri(acol, msize);
	
	printf("ans val = ");
	print_arrd(aval, msize);
	*/
/*
	arow[0] = 0, arow[1] = 0,  arow[2] = 1, arow[3] = 1;
	acol[0] = 0, acol[1] = 1,  acol[2] = 0, acol[3] = 1;
	aval[0] = 1, aval[1] = 2,  aval[2] = 3, aval[3] = 4;
*/
	//b[0] = 1;b[1] = 2;

/*
	for (i = 0; i < asize; i++) {
		for (j = 0; j < asize; j++) {
			aval[i * asize + j] = i * asize + j + 1;
			arow[i * asize + j] = i;
			acol[i * asize + j] = j;
		}
		b[i] = i + 1;
	}
	valsize  = 4;
*/
	lu_decomp(arow, acol, aval, asize, &valsize, p);
	
/*
	printf("after decomp\n");

	printf("ans row = ");
	print_arri(arow, msize);

	printf("ans col = ");
	print_arri(acol, msize);
	
	printf("ans val = ");
	print_arrd(aval, msize);
	printf("\n");
	*/
	
	export_matrixi(arow, valsize, "arow.txt");
	export_matrixi(acol, valsize, "acol.txt");
	export_matrixd(aval, valsize, "aval.txt");


	printf("p = ");
	for (i = 0; i < asize; i++) {
		printf("%d ", p[i]);
	}
	printf("\n");
	lu_solve(arow, acol, aval, asize, &valsize, b, p);
	
	//fprintf(fout, "Ax=bの解は次の通りです¥n");
	/*
	for(i = 0; i < asize; i++) {
		printf("%f\n",b[i]);
		//fprintf(fout, "%f¥n",b[i]);
	}
*/
	export_matrixd(b, asize, "fuga.txt");

	//fclose(fin); 
	//fclose(fout);
	free(arow);
	free(acol);
	free(aval);
	free(p);
	free(b);

	return 0;
} 
