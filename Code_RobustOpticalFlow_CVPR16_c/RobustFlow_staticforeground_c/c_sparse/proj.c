//function x = proj(A, b, x, lb, ub,  num1, num2)
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
void export_matrixd(double *array, int arrsize, char *fname);


void proj(int *arow, int *acol, double *aval,
		  double *b, double *x, 
		  int asize, int bsize, int xsize,
		  double lb, double ub, int num1, int num2) {

	int i, j, ind, min_ind, min_val;
	double st, step;
	double *err, *x_, *tmp, meanx;
	err = (double *)malloc(sizeof(double) * num1);
	x_ = (double *)malloc(sizeof(double) * xsize);
	tmp = (double *)malloc(sizeof(double) * bsize);

	//double x[xsize], tmp[bsize], err[num1];

	printf("hoge xsize = %d\n", xsize);
    //meanx = mean(x);
	double tmp_sum = 0.;
	for (i = 0; i < xsize; i++) { 
		tmp_sum += x[i];
		x_[i] = 0.;
	}
	meanx = tmp_sum / xsize;

	//x = x-meanx;
	//x = x+(ub-lb)/2;
	for (i = 0; i < xsize; i++) {
		x[i] = x[i] - meanx;
		x[i] = x[i] + ((ub - lb)/2);
	}
	for (i = 0; i < bsize; i++) {
		tmp[i] = 0.;
	}
	printf("piyo\n");
    //coarser
    st = -(ub-lb)/2;
    step = (ub-lb)/num1;
    //err = zeros(1,num1); //err = [0, 0, ...0]
    for (i = 0; i < num1; i++) {
		err[i] = 0.;
	}
	/*
	 for i = 1:num1
        x_= x + (st + i*step);
        x_(x_<lb) = lb; x_(x_>ub) = ub;
        err(i) = sum(abs(A*x_-b));
    end 
	*/
	for (i = 0; i < num1; i++) { 
		for (j = 0; j < xsize; j++) {
			x_[j] = x[j] + (st + (i + 1) * step);
			if (x_[j] < lb) 
				x_[j] = lb;
			if (x_[j] > ub)
				x_[j] = ub;
		}
		for (j = 0; j < bsize; j++) {
			tmp[j] = 0.;
		}
		for (j = 0; j < asize; j++) {
			tmp[arow[j]] = tmp[arow[j]] +  aval[j] * x_[acol[j]]; 
		}

		for (j = 0; j < bsize; j++) {
			tmp[j] -= b[j];
			err[i] += fabs(tmp[j]);
		}
	}
	export_matrixd(err, num1, "errsub.dat");
	printf("fuga\n");

   //[~,ind] = min(err); // == ones(1, err)?
	min_ind = 0;
	min_val = 99999999;
	for (i = 0; i < num1;i++) {
		if(err[i] < min_val) {
			min_val = err[i];
			min_ind = i;
		}
	}
	ind = min_ind;
	printf("piyo\n");
	printf("ind = %d\n", ind);
    //x = x + (st + ind*step);
	for (i = 0; i < xsize; i++) {
		x[i] = x[i] + (st + (ind + 1) * step);
	}

	//%finer---------------------------------------------
	st = -(ub-lb)/(num1 * 2);
	step = (ub-lb)/(num1 * num2);
	
	//err = zeros(1,num2);
    for (i = 0; i < num2; i++) {
		err[i] = 0.;
	}
	/*
	 for i = 1:num1
        x_= x + (st + i*step);
        x_(x_<lb) = lb; x_(x_>ub) = ub;
        err(i) = sum(abs(A*x_-b));
    end 
	*/
	for (i = 0; i < num2; i++) { 
		for (j = 0; j < xsize; j++) {
			x_[j] = x[j] + (st + (i + 1) * step);
			if (x_[j] < lb) 
				x_[j] = lb;
			if (x_[j] > ub)
				x_[j] = ub;
		}
		for (j = 0; j < bsize; j++) {
			tmp[j] = 0.;
		}
		for (j = 0; j < asize; j++) {
			tmp[arow[j]] += aval[j] * x_[acol[j]]; 
		}
		
		for (j = 0; j < bsize; j++) {
			tmp[j] -= b[j];
			err[i] += fabs(tmp[j]);
		}
	}

   //[~,ind] = min(err); // == ones(1, err)?
	min_ind = 0;
	min_val = 9999999;
	for (i = 0; i < num2; i++) {
		if(err[i] < min_val) {
			min_val = err[i];
			min_ind = i;
		}
	}
	ind = min_ind;

    //x = x + (st + ind*step);
    //x(x<lb) = lb; x(x>ub) = ub;
	for (i = 0; i < xsize; i++) { 
		x[i] = x[i] + (st + (ind + 1) * step);
		if (x[i] < lb) x[i] = lb;
		if (x[i] > ub) x[i] = ub;
	}

	free(x_);
	free(tmp);
	free(err);
}

