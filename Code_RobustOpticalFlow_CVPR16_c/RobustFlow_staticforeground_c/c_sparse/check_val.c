#include <stdio.h>
//スパース行列ではない座標(p_row, p_col)の座標にあるvalを返す

double check_val(int *row, int *col, double *val, int p_row, int p_col, int valsize) {
	int i, j;

	for (i = 0; i < valsize; i++) { 
		if (row[i] == p_row) {
			for (j = i; j < valsize; j++) {
				if( row[j] == p_row && col[j] == p_col) {
					return val[j];
				} else if (row[j] != p_row || val[j] == 0) return 0;
			}
		} else if (row[i] > p_row || val[i] == 0) return 0;
	}
	return 0;
}
