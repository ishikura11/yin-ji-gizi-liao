#include <stdio.h>
#include <stdlib.h>
#define MAX_VALSIZE 1200000
#define BIG_NUM 99999999
//extern void jly_L1Min_IRLS_Proj4( double *A, double *b, int iter, double lb, double ub, double *x_pre, double *x);
extern void import_matrixi(int *array, int *valsize, char *fname);
extern void import_matrixd(double *array, int *valsize, char *fname);
extern void export_matrixi(int *array, int arrsize, char *fname);
extern void export_matrixd(double *array, int arrsize, char *fname);
/*
void multi_matrix(int *row1, int *col1, double *val1, 
				 int *row2, int *col2, double *val2,
				 int *out_row, int *out_col, double *out_val,
		ii		 int rowsize1, int valsize1, 
				 int colsize2, int valsize2,
				 int *out_rowsize, int *out_colsize, int *out_valsize);
*/
extern void jly_L1Min_IRLS_Proj4(int *arow, int *acol, double *aval, int avalsize, 
						  double *b, int b_size, int iter, double lb, double ub, 
						  double *x0,int x0_size, double *x); 

int main() {
	//xに結果を保存
	int i, j, k, valsize, b_size, x_pre_size, asize = 10000, *arow, *acol;
	double *aval, *b, *x_pre, *x;
	int iter = 1, lb = 0, ub = 70;

	arow  = (int *)malloc(sizeof(int) * MAX_VALSIZE);
	acol  = (int *)malloc(sizeof(int) * MAX_VALSIZE);
	aval  = (double *)malloc(sizeof(double) * MAX_VALSIZE);
	b	  = (double *)malloc(sizeof(double) * asize * 5);
	x_pre = (double *)malloc(sizeof(double) * asize);
	x     = (double *)malloc(sizeof(double) * asize);

	for (i = 0; i < MAX_VALSIZE; i++) {
		arow[i] = BIG_NUM;
		acol[i] = BIG_NUM;
		aval[i] = 0.;
	}
	for (i = 0; i < asize * 5; i++) {
		b[i] = 0.;
	}
	for (i = 0; i < asize; i++) {
		x[i] = 0.;
		x_pre[i] = 0.;
	}
	import_matrixi(arow,   &valsize, "./dataset_sparse/neew100dataset/L1min_Arow.dat");
	import_matrixi(acol,	  &valsize, "./dataset_sparse/neew100dataset/L1min_Acol.dat");
	import_matrixd(aval,  &valsize, "./dataset_sparse/neew100dataset/L1min_Aval.dat");
	import_matrixd(b,	  &b_size, "./dataset_sparse/neew100dataset/L1min_b.dat");
	import_matrixd(x_pre, &x_pre_size, "./dataset_sparse/neew100dataset/L1min_xpre.dat");

	jly_L1Min_IRLS_Proj4( arow, acol, aval, valsize, b, b_size, iter, lb, ub, x_pre, x_pre_size, x);

	export_matrixd(b, asize * 5, "./dataset_sparse/out_dataset/b.dat");
	export_matrixd(x, asize, "./dataset_sparse/out_dataset/x.dat");
}

