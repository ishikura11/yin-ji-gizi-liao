// NOTE: output A^t instead of A
#include "mex.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define round(x) (x<0?ceil((x)-0.5):floor((x)+0.5))

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{  
    /*f*/
    const mxArray *mxf = prhs[0];
    double *f = (double *)mxGetPr(mxf);
    
    int H = mxGetM(mxf);
    int W = mxGetN(mxf)/2;
    int S = H*W;
    //printf("H=%d W=%d\n", H, W);
    
    int ind;
    int r, c;
    
    long ind0, ind1;
    long ele0, ele1;
    
    long elecount = 0;
    
    //1st loop: count the number of elements
    for(int j = 0; j < W; j++)
        for(int i = 0; i < H; i++)
        {
            ind = j*H+i;
            //printf("%d %d %d\n", j, i, ind);
            
            c = round(j + f[ind]);
            if(c < 0) c = 0;
            if(c > W-1) c = W-1;
            r = round(i + f[ind+S]);
            if(r < 0) r = 0;
            if(r > H-1) r = H-1;
            
            ind0 = ind;
            ind1 = c*H+r;
            
            if(ind1 != ind0)
                elecount += 2;
        }
    
    //printf("Number of elements:%d W*H*2=%d\n", elecount, S*2);
    
    //2nd loop: set the elements
    plhs[0] = mxCreateSparse(S, S, elecount, mxREAL);
    double* pr = mxGetPr(plhs[0]);
    mwIndex* ir = mxGetIr(plhs[0]);
    mwIndex* jc = mxGetJc(plhs[0]);
    
    elecount = 0;
    long colcount = 0; //0->S+1
    jc[0] = 0;
    for(int j = 0; j < W; j++)
        for(int i = 0; i < H; i++)
        {
            ind = j*H+i;
            //printf("%d %d %d\n", j, i, ind);
            
            c = round(j + f[ind]);
            if(c < 0) c = 0;
            if(c > W-1) c = W-1;
            r = round(i + f[ind+S]);
            if(r < 0) r = 0;
            if(r > H-1) r = H-1;
            
            ind0 = ind;
            ind1 = c*H+r;
            
            if(ind1 != ind0)
            {
                ir[elecount] = ind0;
                pr[elecount] = 1;
                elecount++;
                
                ir[elecount] = ind1;
                pr[elecount] = -1;
                elecount++;
                
                jc[colcount+1] = jc[colcount] + 2;
            }
            else
                jc[colcount+1] = jc[colcount];
            
            //printf("%d %d\n",colcount+1,jc[colcount+1]);
            colcount++;
        }
}