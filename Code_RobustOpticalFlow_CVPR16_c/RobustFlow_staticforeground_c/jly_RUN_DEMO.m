addpath ../TVL1Flow
addpath ../Flow_viz

%% build 
%mex -O -largeArrayDims jly_constructAfromFlow_nearest.cpp
tic;
%% load data
%load data_Dimetrodon_raindrop.mat
%load data_Dimetrodon_raindrop_sub2.mat
%load data_case10_color.mat
load data_renamori_color.mat

%% set params
param = jly_load_tvl1_param;

interp = 'nearest';
% interp = 'bilinear';

lambda_dI = 0.7;
lambda_dJ = 0.7;
lambda_dJ_tmp = 0.3; %used only in the begining iterations for faster convergance
IRLS_iter = 5;
ub = 70; % upper bound of all foreground pixel intensity
lb = 0; % lower bound of all foreground pixel intensity

max_iter = 5; % outer iteration

%% set initial solution
%J_ = J_init; %all 0
%f_ = f_init; %naive flow computed from raw input images 
J_ = O1;
J_(:) = 0;
f_ = jly_TVL1Flow(O1(:, :, 1), O2(:, :, 1), param);
%生画像から計算されたflowファイル?
%disp(['Initial: J_err=' num2str(jly_reconerror(J_gt, J_)) ' f_err=' num2str(jly_epe(f_, f_gt, noc_gt, noc_gt))]);

Js = {J_};
fs = {f_};

%O1_sub = double(rgb2gray(O1));
%O2_sub = double(rgb2gray(O2));

disp('前処理');
toc;
%% solve
for iter = 1:max_iter
    %tic;
    
    if iter <= 3 % for faster convergance
		lambda_dJ_ = lambda_dJ_tmp;
	else
		lambda_dJ_ = lambda_dJ;
    end
    %solve for J (static foreground image)
    for ch = 1:3
        [J_(:, :, ch), fail] = jly_separate_IRLS_Proj(O1(:,: ,ch), O2(:,: ,ch), f_, lambda_dJ_, lambda_dI, J_(:, :, ch), IRLS_iter, lb, ub, interp);
        %[J_, fail] = jly_separate_IRLS_Proj(O1, O2, f_, lambda_dJ_, lambda_dI, J_, IRLS_iter, lb, ub, interp);
    %disp(['Iter #' num2str(iter) '.1: J_err=' num2str(jly_reconerror(J_gt, J_))]);
        if fail
            break;
        end
    end
    if fail
        break;
    end
    %solve for f (background flow)
    %I1_ = O1 - J_;
    %I2_ = O2 - J_;
    I1_ = O1 - J_;
    I2_ = O2 - J_;
    %I1_ = double(rgb2gray(I1_));
    %I2_ = double(rgb2gray(I2_));
    
    %f_ = jly_TVL1Flow(I1_, I2_, param);
    f_ = jly_TVL1Flow(I1_(:, :, 1), I2_(:, :, 1), param);
    %disp(['Iter #' num2str(iter) '.2: f_err=' num2str(jly_epe(f_, f_gt, noc_gt, noc_gt))]);
    
    
    Js{end+1} = J_;
    fs{end+1} = f_;
    
    toc;
end

%% show results
figure;
subplot(3,4,1);imshow(uint8(O1-Js{1}));title('Initial L_1');
subplot(3,4,2);imshow(uint8(O2-Js{1}));title('Initial L_1''');
subplot(3,4,3);imshow(uint8(Js{1}));title('Initial L_2');
subplot(3,4,4);imshow(flowToColor(fs{1}));title('Initial U');

subplot(3,4,5);imshow(uint8(O1-Js{end}));title('Output L_1');
subplot(3,4,6);imshow(uint8(O2-Js{end}));title('Output L_1''');
subplot(3,4,7);imshow(uint8(Js{end}));title('Output L_2');
subplot(3,4,8);imshow(flowToColor(fs{end}));title('Ouput U');
%{
subplot(3,4,9);imshow(uint8(O1-J_gt));title('GT L_1');
subplot(3,4,10);imshow(uint8(O2-J_gt));title('GT L_1''');
subplot(3,4,11);imshow(uint8(J_gt));title('GT L_2');
subplot(3,4,12);imshow(flowToColor(f_gt));title('GT U');

figure;
Je = zeros(1,numel(Js));
fe = zeros(1,numel(Js));
for i=1:numel(Js); Je(i) = jly_reconerror(J_gt, Js{i}); end
for i=1:numel(fs); fe(i) = jly_epe(fs{i}, f_gt, noc_gt, noc_gt); end
subplot(1,2,1); plot(Je,'.-'); title('Layer reconstruction error');
subplot(1,2,2); plot(fe,'.-'); title('Flow error (EPE)');
%}

% save results.mat Js fs lambda_dI lambda_dJ lambda_dJ_tmp IRLS_iter lb ub interp max_iter
