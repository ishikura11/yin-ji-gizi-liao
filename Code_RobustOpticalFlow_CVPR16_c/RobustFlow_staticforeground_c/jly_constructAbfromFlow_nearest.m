function [ A, b ] = jly_constructAbfromFlow_nearest( f, O1, O2 )
%CONSTRUCTABFROMFLOW Summary of this function goes here
%   Detailed explanation goes here

h = size(f,1);
w = size(f,2);
s = h*w;

%% construct A, b 
% constraint: brightness consitency of I1(x), I2(x+u)
% disp('Constructing A,b for BCC...');
% tic;
A = jly_constructAfromFlow_nearest(f);
A = A';

f_ = round(f);
[c,r] = meshgrid(1:w,1:h);
r_ = reshape(r+f_(:,:,2),[s,1]); r_(r_>h)=h; r_(r_<1)=1;
c_ = reshape(c+f_(:,:,1),[s,1]); c_(c_>w)=w; c_(c_<1)=1; 
ind = sub2ind([h,w],r_,c_);
b = O1(:) - O2(ind);
% toc;

end