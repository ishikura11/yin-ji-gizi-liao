function [tmpJC] = make_tmp_JC(ind, IA, JA)
    %ind行目の要素のある座標(JA)をすべて抜き出す
    tmpJC = [];
    if ind + 1 <= length(IA) || IA(ind+1) <= length(JA)
        for j = IA(ind):IA(ind+1) - 1
            if j > length(JA)
                continue;
            end
            %{
            tmpJC;
            j
            disp('length(JA)')
            length(JA)
            JA(j);
            %}
            tmpJC = horzcat(tmpJC, JA(j));
        end
    end
end
