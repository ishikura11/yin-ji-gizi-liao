function [Gx,Gy,Gxx,Gxy,Gyy]=single_getGMat(w,h)
imgSize=w*h;  %image size 328 508

dS=[1,-1];
filtSizeS=1;

indsGx1=zeros(imgSize*2,1);
indsGx2=zeros(imgSize*2,1);
valsGx=zeros(imgSize*2,1);
indsGy1=zeros(imgSize*2,1);
indsGy2=zeros(imgSize*2,1);
valsGy=zeros(imgSize*2,1);

indy=0; indx=0;
for disp=0:filtSizeS,
    for x=1:w-1,	
       for y=1:h,
          indx=indx+1;
          indsGx1(indx)= sub2ind([h w],y,x);
          indsGx2(indx)= sub2ind([h w],y,x+disp);
          valsGx(indx)=dS(disp+1);
       end
    end
    for x=1:w,	
       for y=1:h-1,
           indy=indy+1;
           indsGy1(indy)= sub2ind([h w],y,x);
           indsGy2(indy)= sub2ind([h w],y+disp,x);
           valsGy(indy)=dS(disp+1);
       end;
    end;
end;

indsGx1=indsGx1(1:indx);
indsGx2=indsGx2(1:indx);
valsGx=valsGx(1:indx);
indsGy1=indsGy1(1:indy);
indsGy2=indsGy2(1:indy);
valsGy=valsGy(1:indy);

Gx = struct;
Gy = struct;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Gx.I, Gx.J, Gx.N] = single_sparse_from_val(indsGx1, indsGx2, valsGx, imgSize);
[Gy.I, Gy.J, Gy.N] = single_sparse_from_val(indsGy1, indsGy2, valsGy, imgSize);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Gx=sparse(indsGx1,indsGx2,valsGx,imgSize,imgSize);
%Gy=sparse(indsGy1,indsGy2,valsGy,imgSize,imgSize);

%{
if (nargout>2)
   Gxx=Gx'*Gx;
   Gxy=Gx*Gy;
   Gyy=Gy'*Gy;
   nzGxx=sum(Gxx~=0,2);
   nzGyy=sum(Gyy~=0,2);
   nzGxy=sum(Gxy~=0,2);
   fzGxx= nzGxx~=3;
   fzGyy= nzGyy~=3;
   fzGxy= nzGxy~=4;
   Gxx(fzGxx,:)=0;
   Gyy(fzGyy,:)=0;
   Gxy(fzGxy,:)=0;
end

%}
if (nargout>2)
   Gxx = struct;
   Gxy = struct;
   Gyy = struct;
   Gx_trp = struct;
   Gy_trp = struct;
   
   [Gx_trp.I, Gx_trp.J, Gx_trp.N, bluff_w, bluff_h] = single_transposition(Gx.I, Gx.J, Gx.N, length(Gx.I) - 1, length(Gx.I) - 1)
   [Gy_trp.I, Gy_trp.J, Gy_trp.N, bluff_w, bluff_h] = single_transposition(Gy.I, Gy.J, Gy.N, length(Gy.I) - 1, length(Gy.I) - 1)
   
   [Gxx.I Gxx.J Gxx.N] = single_sparse_mult(Gx_trp.I, Gx_trp.J, Gx_trp.N, Gx.I, Gx.J, Gx.N);
                          % = Gx'*Gx;   
   [Gxy.I Gxy.J Gxy.N] = single_sparse_mult(Gx.I, Gx.J, Gx.N, Gy.I, Gy.J, Gy.N);
   [Gyy.I Gyy.J Gyy.N] = single_sparse_mult(Gy_trp.I, Gy_trp.J, Gy_trp.N, Gy.I, Gy.J, Gy.N);
%   nzGxx=sum(Gxx~=0,2);
%   nzGyy=sum(Gyy~=0,2);
%   nzGxy=sum(Gxy~=0,2);
   tmp = struct;
   [tmp.I tmp.J tmp.N] = single_notEqual(Gxx.I, Gxx.J, Gxx.N, 0);
   nzGxx = single_sum(tmp.I, tmp.J, tmp.N, 2);
   [tmp.I tmp.J tmp.N] = single_notEqual(Gyy.I, Gyy.J, Gyy.N, 0);
   nzGyy = single_sum(tmp.I, tmp.J, tmp.N, 2);
   [tmp.I tmp.J tmp.N] = single_notEqual(Gxy.I, Gxy.J, Gxy.N, 0);
   nzGxy = single_sum(tmp.I, tmp.J, tmp.N, 2);

   fzGxx= nzGxx~=3;
   fzGyy= nzGyy~=3;
   fzGxy= nzGxy~=4;
   
   Gxx(fzGxx,:)=0;
   Gyy(fzGyy,:)=0;
   Gxy(fzGxy,:)=0;
end

