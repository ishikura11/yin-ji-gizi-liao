#include "mex.h"
#include <string.h>    /* memcpy */

/* undocumented function prototype */
EXTERN_C mxArray *mxCreateSparseNumericMatrix(mwSize m, mwSize n, 
    mwSize nzmax, mxClassID classid, mxComplexity ComplexFlag);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    const float pr[] = {1.0, 6.0};
    const mwIndex ir[] = {0, 3};
    const mwIndex jc[] = {0, 2};
    const mwSize nzmax = 10;
    const mwSize m = 4;
    const mwSize n = 1;

    plhs[0] = mxCreateSparseNumericMatrix(m, n, nzmax, mxSINGLE_CLASS, mxREAL);
    memcpy((void*)mxGetPr(plhs[0]), (const void*)pr, sizeof(pr));
    memcpy((void*)mxGetIr(plhs[0]), (const void*)ir, sizeof(ir));
    memcpy((void*)mxGetJc(plhs[0]), (const void*)jc, sizeof(jc));
}
