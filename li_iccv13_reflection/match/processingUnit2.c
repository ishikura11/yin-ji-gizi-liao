/*
 * processingUnit2.c
 *
 *  Created on: 2017/11/23
 *      Author: ken
 */

#include "match.h"
#include <math.h>
#include "dotprods.h"

void processingUnit2(double d1[DST_DIM], double d2[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_size) {
	int i, j, tmp;
	double distRatio = 0.6;
	double _d2[MAX_DST_NUM][DST_DIM];
	DOTPRODS dotprods[MAX_DST_NUM];
	//double small_val, small_val2;

	for(i = 0; i < MAX_DST_NUM; i++) {
		for(j = 0; j < DST_DIM; j++) {
			_d2[i][j] = d2[i * DST_DIM + j];
		}
	}

	for(i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size) {
			double small_val = 1.;
			double small_val2 = 1.;

			for (j = 0; j < DST_DIM; j++) {
				dotprods[i].val = dotprods[i].val + d1[j] * _d2[i][j];
			}

			tmp = dotprods[i].val;
			dotprods[i].val = M_PI / 2 - tmp - (tmp * tmp * tmp)/(2 * 3) -
							(3 * tmp * tmp * tmp * tmp * tmp) / (2 * 2 * 2 * 5);

	/* qsort(dotprods, size2_dst, sizeof(DOTPRODS), comp);*/
	//small_val = 1.; small_val2 = 1.; /* 0 < dotprods[i].val < 1. */

			if(dotprods[i].val < small_val) {
				small_val2 = small_val;
				small_val = dotprods[i].val;
				if(dotprods[i].val < small_val2 * distRatio) {
					matchnum[d1_ind] = i;
				} else {
					matchnum[d1_ind] = -1;
				}
			} else if(dotprods[i].val < small_val2) {
				small_val2 = dotprods[i].val;
				if(small_val >= distRatio * small_val2)
					matchnum[d1_ind] = -1;
			}
		}
	}
}
/*引数のsmall_val, small_val2, matchnumはポインタなのでここで値を更新するとプログラム全体に
 * 更新が反映される. */

