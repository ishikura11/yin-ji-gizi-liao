/*
 * make_x.c
 *
 *  Created on: 2017/10/31
 *      Author: ken
 */
#include <stdio.h>
#include "match.h"


/*#pragma SDS data sys_port(im1_loc:AFI, im2_loc:AFI)
#pragma SDS data zero_copy(im1_loc[0:size_dst * 4], im2_loc[0:size_dst * 4])
#pragma SDS data access_pattern(im1_loc:SEQUENTIAL, im2_loc:SEQUENTIAL)*/
void make_x(double x1[2 * MAX_DST_NUM], double x2[2 * MAX_DST_NUM], double im1_loc[MAX_DST_NUM * 2], double im2_loc[MAX_DST_NUM * 2], int matchnum[MAX_DST_NUM], int size_dst) {
	int i, j, num, count, _matchnum[MAX_DST_NUM];

//#pragma HLS array_partition variable=_matchnum factor=8 dim=1

	for (i = 0; i < MAX_DST_NUM;i++) {
#pragma HLS UNROLL factor = 8
		_matchnum[i] = matchnum[i];
	}

	num = 0;
	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i < size_dst) {
			if(_matchnum[i] >= 0)
				num++;
		}
	}

	for (i = 0; i < 2; i++) {
		for (j = 0;j < MAX_DST_NUM; j++) {
			x1[i * MAX_DST_NUM + j] = 0.;
			x2[i * MAX_DST_NUM + j] = 0.;
		}
	}

	count = 0;
	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i < size_dst) {
			if(_matchnum[i] >= 0) {
				x1[count] = im1_loc[i * 2 + 1];
				x1[MAX_DST_NUM + count] = im1_loc[i * 2 + 0];
				x2[count] = im2_loc[_matchnum[i] * 2 + 1];
				x2[MAX_DST_NUM + count] = im2_loc[_matchnum[i] * 2 + 0];
				count++;
			}
		}
	}
}
