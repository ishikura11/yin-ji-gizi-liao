/*
 * match_test.c
 *
 *  Created on: 2017/10/31
 *      Author: ken
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "match.h"
///#include <time.h>

extern void match(int size1_dst, int size2_dst, double im1_des[MAX_DST_NUM * DST_DIM],
				  double im2_des[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM]);
extern void make_x(double x1[2 * MAX_DST_NUM], double x2[2 * MAX_DST_NUM], double im1_loc[MAX_DST_NUM * 2],
				   double im2_loc[MAX_DST_NUM * 2], int matchnum[MAX_DST_NUM], int size_dst);
//extern void processingUnit(double d1[DST_DIM], double d2[DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_ind,
					//double *small_val, double *small_val2);

int main() {
	int i, j, k, size_x[6], size_y[6], *matchnum;
	double im1_des[MAX_DST_NUM * DST_DIM], im1_loc[MAX_DST_NUM * 2], im2_loc[MAX_DST_NUM * 2], im2_des[MAX_DST_NUM * DST_DIM];
	//double *im1_des, *im1_loc, *im2_loc, *im2_des;
	double *x1, *x2, *d1, *d2;//, *small_val, *small_val2;
	//struct timespec t1, t2;
	//clock_gettime(CLOCK_REALTIME, &t1);
	//*small_val = 1.; *small_val2 = 1.;

	/*
    x1 = (double *)sds_alloc(2 * MAX_DST_NUM * sizeof(double));
    x2 = (double *)sds_alloc(2 * MAX_DST_NUM * sizeof(double));
    d1 = (double *)sds_alloc(DST_DIM * sizeof(double));
    d2 = (double *)sds_alloc(DST_DIM * sizeof(double));
    matchnum = (int *)sds_alloc(MAX_DST_NUM * sizeof(int));
	*/

    x1 = (double *)malloc(2 * MAX_DST_NUM * sizeof(double));
    x2 = (double *)malloc(2 * MAX_DST_NUM * sizeof(double));
    d1 = (double *)malloc(DST_DIM * sizeof(double));
    d2 = (double *)malloc(DST_DIM * sizeof(double));
    matchnum = (int *)malloc(MAX_DST_NUM * sizeof(int));
		

    /*
    im1_des = (double *)sds_alloc(5000 * 128 * sizeof(double));
    im2_des = (double *)sds_alloc(128 * 5000 * sizeof(double));
    im1_loc = (double *)sds_alloc(5000 * 2 * sizeof(double));
    im2_loc = (double *)sds_alloc(5000 * 2 * sizeof(double));
*/
    if (!x1 || !x2 || !matchnum || !d1 || !d2/*|| !im1_des || !im2_des || !im1_loc || !im2_loc*/) {
         if (x1) free(x1);
         if (x2) free(x2);
         if (d1) free(d1);
         if (d2) free(d2);
         if (matchnum) free(matchnum);
/*
		 if (x1) sds_free(x1);
         if (x2) sds_free(x2);
         if (d1) sds_free(d1);
         if (d2) sds_free(d2);
         if (matchnum) sds_free(matchnum);
*/
/*         if (im1_des) sds_free(im1_des);
         if (im2_des) sds_free(im2_des);
         if (im1_loc) sds_free(im1_loc);
         if (im2_loc) sds_free(im2_loc);*/
         return 2;
    }

    for (i = 0;i < MAX_DST_NUM;i++)
		matchnum[i] = -1;

	FILE *fp, *fp2, *fp3, *fp4, *fp5, *fp6, *fp_x1, *fp_x2;

	fp  = fopen("./im1_size.txt", "r");
	fp2 = fopen("./im2_size.txt", "r");
	fp3 = fopen("./im1_des.txt", "r");
	fp4 = fopen("./im1_loc.txt", "r");
	fp5 = fopen("./im2_des.txt", "r");
	fp6 = fopen("./im2_loc.txt", "r");

	if(fp == NULL) {
		printf("read error\n");
		return -1;
	}

	if(fp2 == NULL) {
		printf("read error\n");
		return -1;
	}

	fscanf(fp, "%d %d", &size_x[0], &size_y[0]);
	fscanf(fp, "%d %d", &size_x[1], &size_y[1]);
	fscanf(fp, "%d %d", &size_x[2], &size_y[2]);
	fscanf(fp2, "%d %d", &size_x[3], &size_y[3]);
	fscanf(fp2, "%d %d", &size_x[4], &size_y[4]);
	fscanf(fp2, "%d %d", &size_x[5], &size_y[5]);
	fclose(fp);
	fclose(fp2);


	for (i = 0; i < MAX_DST_NUM; i++) {
		if (i >= size_x[1]) break;
		for (j = 0; j < DST_DIM; j++) {
			if(j >= size_y[1]) break;
			fscanf(fp3, "%lf", &im1_des[i * DST_DIM + j]);
			//printf("_im1_des[%d][%d] = %lf\n", i, j, im1_des[i * DST_DIM + j]);
		}
	}
	fclose(fp3);

	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i >= size_x[2]) break;
		for (j = 0; j < 2; j++) {
			if(j >= size_y[2]) break;
			//fscanf(fp4, "%lf", &im1_loc[i][j]);
			fscanf(fp4, "%lf", &im1_loc[i * 2 + j]);
		}
	}
	fclose(fp4);

	for (i = 0; i < MAX_DST_NUM; i++) {
		if (i >= size_x[4]) break;
		for (j = 0; j < DST_DIM; j++) {
			if (j >= size_y[4]) break;
			//fscanf(fp5, "%lf", &im2_des[j][i]);
			fscanf(fp5, "%lf", &im2_des[i * DST_DIM + j]);
		}
	}
	fclose(fp5);



	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i >= size_x[5]) break;
		for (j = 0; j < 2; j++) {
			if(j >= size_y[5]) break;
			fscanf(fp6, "%lf", &im2_loc[i * 2 + j]);
		}
	}
	fclose(fp6);
    printf("match before\n");

	/*
	 * insert match function
	 */
	for (i = 0; i < 6;i++) {
		printf("size_x[%d] = %d\n", i, size_x[i]);
		printf("size_y[%d] = %d\n", i, size_y[i]);
	}
	/*
	for(i = 0; i < size_x[1]; i++) {
		printf("im1_des[i] = %lf\n", im1_des[i * DST_DIM]);
	}
	*/

	match(size_x[1], size_x[4], im1_des, im2_des, matchnum);
	printf("match end\n");
	make_x(x1, x2, im1_loc, im2_loc, matchnum, size_x[1]);
	printf("make_x end\n");

	/*
	for(i = 0; i < 2 * MAX_DST_NUM; i++) {
		if(x1[i] > 0) 
			printf("x1[%d] = %lf\n", i, x1[i]);
	}*/

	fp_x1 = fopen("x1.txt", "w");
	fp_x2 = fopen("x2.txt", "w");
	for (i = 0;i < 2;i++) {
		for (j = 0; j < MAX_DST_NUM; j++) {
			if (x1[i * MAX_DST_NUM + j] == 0 || x2[i * MAX_DST_NUM + j] == 0) break;
			fprintf(fp_x1, "%lf ", x1[i * MAX_DST_NUM + j]);
			fprintf(fp_x2, "%lf ", x2[i * MAX_DST_NUM + j]);
		}
		fprintf(fp_x1, "\n");
		fprintf(fp_x2, "\n");
	}
	fclose(fp_x1);
	fclose(fp_x2);

	free(x1);
	free(x2);
    free(d1);
    free(d2);
	free(matchnum);
	/* 
	sds_free(x1);
	sds_free(x2);
    sds_free(d1);
    sds_free(d2);
	sds_free(matchnum);
	*/
	/*
	sds_free(im1_des);
	sds_free(im2_des);
	sds_free(im1_loc);
	sds_free(im2_loc);
	*/

	//clock_gettime(CLOCK_REALTIME, &t2);
	//printf("time = %f\n", (double)(t2 - t1) / CLOCKS_PER_SEC);
	//printf("time = %ld\n", t2.tv_nsec - t1.tv_nsec);

	return 0;
}
