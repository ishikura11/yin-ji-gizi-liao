/*
 * match.c
 *
 *  Created on: 2017/10/31
 *      Author: ken
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <complex.h>
#include "dotprods.h"
#include "match.h"

//#pragma SDS data zero_copy(im1_des[0:size1_dst][0:128], im2_dest[0:128][0:size2_dst])
/*void match(int size1_dst, int size2_dst, int size1y_dst, double im1_des[5000][128],
		   double im2_dest[128][5000], double x1[2000], double x2[2000], int matchnum[5000]) {*/
/* #pragma SDS data sys_port(im1_des:AFI, im2_dest:AFI) */
//#pragma SDS data zero_copy(im1_des[0:size1_dst*128], im2_dest[0:128 * size2_dst])
#pragma SDS data access_pattern(im1_des:SEQUENTIAL, im2_des:SEQUENTIAL)
void match(int size1_dst, int size2_dst, double im1_des[MAX_DST_NUM * DST_DIM],
			   double im2_des[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM]) {
	int i, j, k, small_indx;
	double tmp, distRatio = 0.6;
	double small_val, small_val2;
	double _im1_des[MAX_DST_NUM][DST_DIM], _im2_des[MAX_DST_NUM][DST_DIM];
	DOTPRODS dotprods[MAX_DST_NUM];

//#pragma HLS array_partition variable=dotprods factor=8 dim=1

	for(i = 0; i < MAX_DST_NUM; i++) {
		if (i < size1_dst) {
			for (j = 0; j < DST_DIM; j++) {
				_im1_des[i][j] = im1_des[i * DST_DIM + j];
				//printf("_im1_des[%d][%d] = %lf\n", i, j, _im1_des[i][j]);
			}
		}
	}

	for (i = 0; i < MAX_DST_NUM; i++) {
		if (i < size2_dst) {
			for(j = 0; j < DST_DIM; j++) {
				_im2_des[i][j] = im2_des[i * DST_DIM + j];
				//printf("im2_des[%d][%d] = %lf\n", i, j, _im2_des[i][j]);
			}
		}
	}

	for(i = 0;i < MAX_DST_NUM; i++) {
		if (i < size1_dst) {
			for (j = 0;j < MAX_DST_NUM; j++) {
#pragma HLS UNROLL factor = 8
				if (j < size2_dst) {
					dotprods[j].val = 0;
					dotprods[j].indx = j;
				}
			}

			for (j = 0; j < MAX_DST_NUM; j++) {
#pragma HLS PIPELINE
				if (j < size2_dst) {
					for (k = 0; k < DST_DIM; k++) {
						dotprods[j].val = dotprods[j].val + _im1_des[i][k] * _im2_des[j][k];
						/*
						if(k == 0)
							printf("k = %d, dotprods[%d].val = %lf\n", k, j, dotprods[j].val);
						if(k == 127)
							printf("k = %d, dotprods[%d].val = %lf\n", k, j, dotprods[j].val);
							*/
					}
					//printf("dotprods[%d].val = %lf\n", j, dotprods[j].val);
				}
			}
			for (j = 0; j < MAX_DST_NUM;j++) {
#pragma HLS PIPELINE
				if (j < size2_dst) {
					tmp = dotprods[j].val;
					dotprods[j].val = M_PI / 2 - tmp - (tmp * tmp * tmp)/(2 * 3) -
									(3 * tmp * tmp * tmp * tmp * tmp) / (2 * 2 * 2 * 5);
				}
			}
		/* qsort(dotprods, size2_dst, sizeof(DOTPRODS), comp);*/
			small_val = 1.; small_val2 = 1.; /* 0 < dotprods[i].val < 1. */
			small_indx = 1;
			for (j = 0; j < MAX_DST_NUM; j++) {
#pragma HLS PIPELINE
				if (j < size2_dst) {
					if (dotprods[j].val < small_val) {
						small_val2 = small_val;
						small_val = dotprods[j].val;
						small_indx = j;
					} else if (dotprods[j].val < small_val2) {
						small_val2 = dotprods[j].val;
					}
				}
			}
		/*
		 * if(dotprods[0].val < distRatio * dotprods[1].val)
		 * matchnum[i] = dotprods[0].indx;
		 */
			if(small_val < distRatio * small_val2) {
				matchnum[i] = small_indx;
			} else
				matchnum[i] = -1;
		}
	}
}
