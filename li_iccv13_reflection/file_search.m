function [ files ] = file_search()
% 現在のディレクトリ情報を表示

mFileList = dir(['*.m']); % 拡張子がmのファイルをリストアップ
mNum = size(mFileList);

files = cell(mNum);

currentDir = pwd;
fprintf('現在のディレクトリ: %s\n', currentDir);

if mNum(1) == 0
    fprintf('mファイルはありません。\n')
else
    for i = 1 : mNum(1)           
        % ファイル名の取得
        mFileName = char(mFileList(i).name); % char型に変換
        fprintf('mファイル %d: %s\n', i, mFileName);
        files{i} = mFileName;
    end;
end;
end

