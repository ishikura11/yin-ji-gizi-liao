#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "./dotprods.h"
#include "./makearray.h"

double im1_img[800][800], im2_img[800][800], im1_des[5000][128], im2_des[5000][128];
double im1_loc[5000][4], im2_loc[5000][4], im2_dest[128][5000], x1[2][1000], x2[2][1000];
int match[5000];
DOTPRODS dotprods[5000];

extern void mat_free(double **mat);
extern int comp(const void *c1, const void *c2);

int main(void) {
	int i, j, k, num, count, size_x[6], size_y[6];//, *match;
	FILE *fp, *fp2, *fp3, *fp4, *fp5, *fp6, *fp7, *fp8, *fp_x1, *fp_x2;
	//double **im1_img, **im1_des, **im1_loc, **im2_img, **im2_des, **im2_loc, **im2_dest, **x1, **x2;
	
	//double **x1, **x2;

	//double im1_img[800][800], im1_des[1000][128], im1_loc[4000][4];
	double distRatio = 0.6;

	fp  = fopen("./im1_size.txt", "r");
	fp2 = fopen("./im2_size.txt", "r");
	fp3 = fopen("./im1.txt", "r");
	fp4 = fopen("./im1_des.txt", "r");
	fp5 = fopen("./im1_loc.txt", "r");
	fp6 = fopen("./im2.txt", "r");
	fp7 = fopen("./im2_des.txt", "r");
	fp8 = fopen("./im2_loc.txt", "r");

	if(fp == NULL) { 
		printf("read error\n");
		return -1;
	}

	if(fp2 == NULL) {
		printf("read error\n");
		return -1;
	}
	
	fscanf(fp, "%d %d", &size_x[0], &size_y[0]);
	fscanf(fp, "%d %d", &size_x[1], &size_y[1]);
	fscanf(fp, "%d %d", &size_x[2], &size_y[2]);
	fscanf(fp2, "%d %d", &size_x[3], &size_y[3]);
	fscanf(fp2, "%d %d", &size_x[4], &size_y[4]);
	fscanf(fp2, "%d %d", &size_x[5], &size_y[5]);
	
	
	/*makearray(size_x[0], size_y[0], im1_img);
	makearray(size_x[1], size_y[1], im1_des);
	makearray(size_x[2], size_y[2], im1_loc);
	makearray(size_x[3], size_y[3], im2_img);
	makearray(size_x[4], size_y[4], im2_des);
	makearray(size_x[5], size_y[5], im2_loc);
	makearray(size_y[4], size_x[4], im2_dest);
	*/

	fclose(fp);
	fclose(fp2);


	for (i = 0; i < size_x[0]; i++) {
		for (j = 0; j < size_y[0]; j++) {
			fscanf(fp3, "%lf", &im1_img[i][j]);
		}
	}
	fclose(fp3);
	
	for (i = 0; i < size_x[1]; i++) {
		for (j = 0; j < size_y[1]; j++) {
			fscanf(fp4, "%lf", &im1_des[i][j]);
		}
	}
	fclose(fp4);

	for (i = 0; i < size_x[2]; i++) {
		for (j = 0; j < size_y[2]; j++) {
			fscanf(fp5, "%lf", &im1_loc[i][j]);
		}
	}
	fclose(fp5);

	for (i = 0; i < size_x[3]; i++) {
		for (j = 0; j < size_y[3]; j++) {
			fscanf(fp6, "%lf", &im2_img[i][j]);
		}
	}
	fclose(fp6);

	for (i = 0; i < size_x[4]; i++) {
		for (j = 0; j < size_y[4]; j++) {
			fscanf(fp7, "%lf", &im2_des[i][j]);
		}
	}
	fclose(fp7);

	for (i = 0; i < size_x[5]; i++) {
		for (j = 0; j < size_y[5]; j++) {
			fscanf(fp8, "%lf", &im2_loc[i][j]);
		}
	}
	fclose(fp8);

	for (i = 0;i < size_x[1];i++)
		match[i] = -1;

	for(i = 0;i < size_x[1]; i++) {
		for (j = 0;j < size_x[4]; j++) {
			dotprods[j].val = 0;
			dotprods[j].indx = j;
		}

		for (j = 0;j < size_x[4]; j++) {
			for (k = 0; k < size_y[1]; k++) {
				dotprods[j].val = dotprods[j].val + im1_des[i][k] * im2_dest[k][j];
			}
		}
		for (j = 0; j < size_x[4];j++) {
			dotprods[j].val = acos(dotprods[j].val);
		}
		qsort(dotprods, size_x[4], sizeof(DOTPRODS), comp);
		if(dotprods[0].val < distRatio * dotprods[1].val)
			match[i] = dotprods[0].indx;
		else
			match[i] = -1;
		//free(dotprods);
	}
	
	num = 0;
	for (i = 0; i < size_x[1]; i++) {
		//printf("match[%d] = %d\n", i, match[i]);
		if(match[i] >= 0)
			num++;
	}
	
	/*
	makearray(2, num, x1);
	makearray(2, num, x2);
	*/
	for (i = 0; i < 2; i++) {
		for (j = 0;j < num; j++) {
			x1[i][j] = 0.;
			x2[i][j] = 0.;
		}
	}
	
	count = 0;
	for (i = 0; i < size_x[1]; i++) {
		//printf("i = %d, count = %d\n", i, count);
		if(match[i] >= 0) {
			x1[0][count] = im1_loc[i][1];
			x1[1][count] = im1_loc[i][0];
			x2[0][count] = im2_loc[match[i]][1];
			x2[1][count] = im2_loc[match[i]][0];
			count++;
		}
	}
	
/*
	for (i = 0; i < size_x[2]; i++) {
		printf("im1_loc[%d][1] = %lf\n", i, im1_loc[i][1]);
		printf("im1_loc[%d][0] = %lf\n", i, im1_loc[i][0]);
	}
*/



	/*
	for (j = 0; j < num; j++) {
		printf("x1[0][%d] = %lf\n", j, x1[0][j]);
		printf("x1[1][%d] = %lf\n", j, x1[1][j]);
	}
*/
	fp_x1 = fopen("x1.txt", "w");
	fp_x2 = fopen("x2.txt", "w");
	for (i = 0;i < 2;i++) {
		for (j = 0; j < num; j++) {
			fprintf(fp_x1, "%lf ", x1[i][j]);
			fprintf(fp_x2, "%lf ", x2[i][j]);
		}
		fprintf(fp_x1, "\n");
		fprintf(fp_x2, "\n");
	}
	fclose(fp_x1);
	fclose(fp_x2);
	
	//num = sum(match > 0);
	//

	/*
	int num = 0;
	for (i = 0; i < size_x[0];i++) {
		for (j = 0;j < size_y[0];j++) {
			printf("%lf\n", im1_img[i][j]);
		}
		num++;
	}
	printf("num = %d\n", count);
	*/
	
	/*mat_free(im1_img);
	mat_free(im1_des);
	mat_free(im1_loc);
	mat_free(im2_img);
	mat_free(im2_des);
	mat_free(im2_loc);
	mat_free(im2_dest);
	mat_free(x1);
	mat_free(x2);
	*/

	return -1;
}

void mat_free(double **mat) {
	int i;
	int mat_size;

	mat_size = sizeof(mat) / sizeof(mat[0]);

	for (i=0 ; i < mat_size ; i++) {
		free(mat[i]);
	}
	free(mat);
}

int comp(const void *c1, const void *c2) {
	DOTPRODS dotprods1 = *(DOTPRODS *)c1;
	DOTPRODS dotprods2 = *(DOTPRODS *)c2;
	
	double tmp1 = dotprods1.val;
	double tmp2 = dotprods2.val;

	if (tmp1 > tmp2) return 1;
	else if (tmp1 == tmp2) return 0;
	else return -1;
}
