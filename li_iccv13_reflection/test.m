function [ flag ] = test( w, h, loop_num, test_num)
%TEST この関数の概要をここに記述
%   詳細説明をここに記述
   % test_num = 0 singleからdouble その逆 
   % test_num = 1 multiply
   % test_num = 2 plus
   % test_num = 3 merge
   % test_num = 4 notEqual
   % test_num = 5 reshape input: w, h
   % test_num = 6 spdiags
   % test_num = 7 setdiff
   % test_num = 8 sum
   % test_num = 9 find
   % test_num = 10 check val
   % test_num = 11 find_col
   % test_num = 12
   % test_num = 13
   % test_num = 14
   % test_num = 0
   % test_num = 0


    flag = 1;
    new_w = input('w is?');
    new_h = input('h is?');

    for i = 1:loop_num
        % singleからdoubleの変換テスト、その逆のテストも兼ねる
        if test_num == 0
            mat = sprand(w, h, rand);
            ans_mat = single(full(mat));
            [a, b, c, d, e] = single_sparse_from_matrix(mat);
            mat = single_to_double(a, b, c, d, e);
            mat = single(full(mat));
            if ~isequal(mat, ans_mat)
                disp('bad')
                i
                flag = 0;
            end
            if flag == 0
                break;
            end
        end
        
        %singleの掛け算のテスト
        if test_num == 1
            % multiply output is w * w mat
            mat1 = sprand(w, h, rand);
            mat2 = sprand(h, w, rand);
            ans_mat = mat1 * mat2;
            ans_mat = single(full(ans_mat));
            mat1S = struct;
            [mat1S.I, mat1S.J, mat1S.N, mat1S.w, mat1S.h] = single_sparse_from_matrix(mat1);
            mat2S = struct;
            [mat2S.I, mat2S.J, mat2S.N, mat2S.w, mat2S.h] = single_sparse_from_matrix(mat2);
            mat = struct;
            [mat.I, mat.J, mat.N] = single_sparse_mult(mat1S.I, mat1S.J, mat1S.N, mat2S.I, mat2S.J, mat2S.N);
            matS = single_to_double(mat.I, mat.J, mat.N, w, w);
            matS = single(full(matS));
            if ~isequal(matS, ans_mat)
                disp('bad')
                i
                flag = 0;
                ans_mat
                matS
            end
            if flag == 0
                break;
            end            
        end
        
        %singleの足し算のテスト
        if test_num == 2
            % multiply output is w * h mat
            mat1 = sprand(w, h, rand);
            mat2 = -mat1;
            mat2(4) = rand;
            %mat2 = sprand(w, h, rand);
            ans_mat = mat1 + mat2;
            ans_mat = single(full(ans_mat));
            mat1S = struct;
            [mat1S.I, mat1S.J, mat1S.N, mat1S.w, mat1S.h] = single_sparse_from_matrix(mat1);
            mat2S = struct;
            [mat2S.I, mat2S.J, mat2S.N, mat2S.w, mat2S.h] = single_sparse_from_matrix(mat2);
            mat = struct;
            [mat.I, mat.J, mat.N] = single_sparse_plus(mat1S.I, mat1S.J, mat1S.N, mat2S.I, mat2S.J, mat2S.N);
            matS = single_to_double(mat.I, mat.J, mat.N, h, w);
            matS = single(full(matS));            
            if ~isequal(matS, ans_mat)
                disp('bad')
                i
                flag = 0;
                ans_mat;
                matS;
            end
            if flag == 0
                break;
            end
        end
        
        %single merge test
        if test_num == 3
            % multiply output is w * h mat
            mat1 = sprand(w, h, rand);
            mat2 = sprand(w, h, rand);
            ans_mat = [mat1;mat2];
            ans_mat = single(full(ans_mat));
            mat1S = struct;
            [mat1S.I, mat1S.J, mat1S.N, mat1S.w, mat1S.h] = single_sparse_from_matrix(mat1);
            mat2S = struct;
            [mat2S.I, mat2S.J, mat2S.N, mat2S.w, mat2S.h] = single_sparse_from_matrix(mat2);
            mat = struct;
            [mat.I, mat.J, mat.N] = single_merge(mat1S.I, mat1S.J, mat1S.N, mat2S.I, mat2S.J, mat2S.N);
            matS = single_to_double(mat.I, mat.J, mat.N, h, w * 2);
            matS = single(full(matS));
            if ~isequal(matS, ans_mat)
                disp('bad')
                i
                flag = 0;
                ans_mat
                matS
            end
            if flag == 0
                break;
            end
        end
        
        %single notEqual test
        if test_num == 4
            mat = sprand(w, h, rand);
            ans_mat = single(full(mat ~= 0));
            [a, b, c, d, e] = single_sparse_from_matrix(mat);
            [a, b, c] = single_notEqual(a, b, c, 0);
            mat = single_to_double(a, b, c, d, e);
            mat = single(full(mat));
            if ~isequal(mat, ans_mat)
                disp('bad')
                i
                flag = 0;
            end
            if flag == 0
                break;
            end
        end
        
        %single reshape test
        if test_num == 5
            mat = sprand(w, h, rand);
            mat = reshape(mat, new_h, new_w);
            ans_mat = single(full(mat));
            
            [a, b, c, d, e] = single_sparse_from_matrix(mat);
            [a, b, c] = single_reshape(a, b, c, d, e, new_w, new_h);
            mat = single_to_double(a, b, c, new_w, new_h);
            mat = single(full(mat));
            if ~isequal(mat, ans_mat)
                disp('bad')
                i
                flag = 0;
            end
            if flag == 0
                break;
            end
        end
        
        %single spdiags test h = 1
        if test_num == 6
            mat = rand(w, h);
            ans_mat = spdiags(mat, 0, length(mat), length(mat));
            ans_mat = single(full(ans_mat));
            [a, b, c, d, e] = single_sparse_from_matrix(mat);
            [a, b, c] = single_spdiags(a, b, c);
            mat = single_to_double(a, b, c, length(a)-1, length(a)-1);
            mat = single(full(mat));
            if ~isequal(mat, ans_mat)
                disp('bad')
                i
                mat
                ans_mat
                flag = 0;
            end
            if flag == 0
                break;
            end
        end

        %setdiff test
        if test_num == 7
            mat1 = sprand(w, h, rand);
            mat2 = sprand(w, h, rand);
            ans_mat = setdiff(mat1, mat2);
            ans_mat = single(full(ans_mat));
            mat1S = struct;
            [mat1S.I, mat1S.J, mat1S.N, mat1S.w, mat1S.h] = single_sparse_from_matrix(mat1);
            mat2S = struct;
            [mat2S.I, mat2S.J, mat2S.N, mat2S.w, mat2S.h] = single_sparse_from_matrix(mat2);
            mat = struct;
            [mat.I, mat.J, mat.N] = single_setdiff(mat1S.I, mat1S.J, mat1S.N, mat2S.I, mat2S.J, mat2S.N);
            matS = single_to_double(mat.I, mat.J, mat.N, length(mat.I)-1, 1);
            matS = single(full(matS));
            if ~isequal(matS, ans_mat)
                disp('bad')
                i
                flag = 0;
                ans_mat
                matS
            end
            if flag == 0
                break;
            end
        end
        
        % sum test
        if test_num == 8
            mat = sprand(w, h, rand);
            anssum = sum(mat, 2);
            anssum = single(full(anssum));
            [a, b, c, d, e] = single_sparse_from_matrix(mat);
            sumsum = single_sum(a, b, c, 2, d, e);
            if ~isequal(sumsum, anssum)
                disp('bad')
                i
                sumsum
                anssum
                flag = 0;
            end
            if flag == 0
                break;
            end
        end
        
        % find test
        if test_num == 9
            mat = sprand(w, h, rand);
            ans_out = find(mat);
            ans_out = single(full(ans_out));
            [a, b, c, d, e] = single_sparse_from_matrix(mat);
            out = single_find(a, b, c, d, e);
            out = single(out');
            if ~isequal(out, ans_out)
                disp('bad')
                i
                out
                ans_out
                flag = 0;
            end
            if flag == 0
                break;
            end
        end

        % check val test
        if test_num == 10
            mat = sprand(w, h, rand);
            x = randi(w);
            y = randi(h);
            ans_out = mat(x, y);
            ans_out = single(full(ans_out));
            [a, b, c, d, e] = single_sparse_from_matrix(mat);
            out = single_check_val(a, b, c, x, y);
            out = single(out);
            if ~isequal(out, ans_out)
                disp('bad')
                i
                out
                ans_out
                flag = 0;
            end
            if flag == 0
                break;
            end
        end
        
        % find_col test
        %{
        if test_num == 11
            mat = sprand(w, h, rand);
            x = randi(w);
            y = randi(h);
            ans_out = mat(x, y);
            ans_out = single(full(ans_out));
            [a, b, c, d, e] = single_sparse_from_matrix(mat);
            out = single_check_val(a, b, c, x, y);
            out = single(out);
            if ~isequal(out, ans_out)
                disp('bad')
                i
                out
                ans_out
                flag = 0;
            end
            if flag == 0
                break;
            end
        end
        %}
    end
end

