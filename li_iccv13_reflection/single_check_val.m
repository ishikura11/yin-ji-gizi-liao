function [ val ] = single_check_val(IA, JA, AN, x, y)
    val = 0;
    %[IA JA AN w h] = single_transposition(IA, JA, AN, length(IA)-1, length(IA)-1);
    if IA(x+1) == IA(x)
        return
    else
        for i = IA(x):IA(x+1)-1
            if y == JA(i)
                val = AN(i);
                return
            end
        end
    end
end
