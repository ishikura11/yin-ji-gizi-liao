function [I1,I2] = single_layerSepIRLS (I,G, f_inds,b_inds)
%Layers reconstruction using Iterative Reweighted Least Square (IRLS)
%   [ I1 I2 ] = layerSepIRLS( I, G, edgeProbB, edgeProbF)
%   I: mixture image
%   G: derivative filter matrix (pre-computed)
%   f_inds,b_inds:  pixels labelled as belonging to reflection/background
%tic
iter = 3;

w1=50;
w2=1;

[h,w]=size(I);
imgSize=h*w;

%constraints matrix
Gx = G.Gx;
Gy = G.Gy;
Gxx = G.Gxx;
Gyy = G.Gyy;



A1=[Gx;Gy;Gxx;Gyy];
%size(A1) = 9360 2340
%[h_A1 w_A1] = size(A1);
%[x y val] = find(A1);
A1sub = struct;

%[A1sub.I, A1sub.J, A1sub.N] = single_sparse_from_val(x, y, val, w_A1, h_A1);
[A1sub.I, A1sub.J, A1sub.N, A1sub.w, A1sub.h] = single_sparse_from_matrix(A1);
%A=[A1;A1];
A = struct;

[A.I, A.J, A.N] = single_merge(A1sub.I, A1sub.J, A1sub.N, A1sub.I, A1sub.J, A1sub.N);
%A 18721 45184 45184
disp('end make A ')
toc

%tic;
%b=[zeros(size(A1,1),1); A1*I(:)];
tmp_s = struct;
%%%%%%%%% very long
[tmp_s.I, tmp_s.L, tmp_s.N, tmp_s.w, tmp_s.h] = single_sparse_from_matrix(zeros(size(A1, 1), 1)); 

% make I single
II = struct;
[II.I, II.J, II.N, II.w, II.h] = single_sparse_from_matrix(I(:));

tmp_s2 = struct;
%%%%%%%%%%%%% very long
[tmp_s2.I, tmp_s2.J, tmp_s2.N] = single_sparse_mult(A1sub.I, A1sub.J, A1sub.N, II.I, II.J, II.N);

b = struct;
[b.I, b.J, b.N] = single_merge(tmp_s.I, tmp_s.L, tmp_s.N, tmp_s2.I, tmp_s2.J, tmp_s2.N);
%b=[zeros(size(A1,1),1); A1*I(:)]; ここまで
disp('end make b')
toc

%tic;
%f1=[ones(imgSize*2,1)*w1; w2*ones(imgSize*2,1); w2*ones(imgSize,1)];
tmp_s3 = struct;
tmp_s = struct;

[tmp_s.I, tmp_s.J, tmp_s.N] = single_sparse_from_matrix(ones(imgSize*2,1)*w1);
[tmp_s2.I, tmp_s2.J, tmp_s2.N] = single_sparse_from_matrix(w2*ones(imgSize*2,1));
%%% under operatin's time is41.308972 seconds
[tmp_s3.I, tmp_s3.J, tmp_s3.N] = single_sparse_from_matrix(w2*ones(imgSize,1));

f1_t = struct;
f1 = struct;

[f1_t.I, f1_t.J, f1_t.N] = single_merge(tmp_s.I, tmp_s.J, tmp_s.N, tmp_s2.I, tmp_s2.J, tmp_s2.N);
[f1.I, f1.J, f1.N] = single_merge(f1_t.I, f1_t.J, f1_t.N, tmp_s3.I, tmp_s3.J, tmp_s3.N);

disp('end make f1')
toc

%tic;
%f1=[ones(imgSize*2,1)*w1; w2*ones(imgSize*2,1); w2*ones(imgSize,1)];ここまで

%f2 = struct;

%[h_f1 w_f1] = size(f1);
h_f1 = length(f1.I) - 1;
w_f1 = 1;

%転置は下記の行列に値を代入する処理が直線距離で行われているため(後で戻す)
%f1は1 * x の行列なので転置する必要がなかった
%{
mat_f1 = single_to_double(f1.I, f1.J, f1.N, w_f1, h_f1);
mat_f1 = mat_f1';
[f1.I f1.J f1.N] = single_sparse_from_matrix(mat_f1);
%}
%[f1.I f1.J f1.N] = single_transposition(f1.I, f1.J, f1.N, w_f1, h_f1);

f2 = f1;
%f1([f_inds,f_inds+imgSize])=0;
%f1([b_inds,b_inds+imgSize])=100;

%f1([b_inds+imgSize*2,b_inds+imgSize*3,b_inds+imgSize*4])=4;
f1.N([b_inds+imgSize*2,b_inds+imgSize*3,b_inds+imgSize*4])=4;

%f2([f_inds+imgSize*2,f_inds+imgSize*3,f_inds+imgSize*4])=4;
f2.N([f_inds+imgSize*2,f_inds+imgSize*3,f_inds+imgSize*4])=4;

f1.N([b_inds,b_inds+imgSize])=100;
f2.N([f_inds,f_inds+imgSize])=100;

IFcount = zeros(length(f1.I), 1);
f1.N([f_inds,f_inds+imgSize])=0;
for i = 1:length(f1.N)
    if f1.N(i) == 0
        del = find_col(f1.I, i);
        IFcount(del+1) = IFcount(del+1) + 1;
    end
end
f1.I = f1.I - IFcount;
f1.J([f_inds,f_inds+imgSize])=[];
f1.N([f_inds,f_inds+imgSize])=[];
%f_indsとb_indsに共通した値はない

%f2([f_inds,f_inds+imgSize])=100;
%f2([b_inds,b_inds+imgSize])=0;

IFcount = zeros(length(f2.I), 1);
f2.N([b_inds,b_inds+imgSize])=0;
for i = 1:length(f2.N)
    if f2.N(i) == 0
        del = find_col(f2.I, i);
        IFcount(del+1) = IFcount(del+1) + 1;
    end
end
f2.I = f2.I - IFcount;
f2.J([b_inds,b_inds+imgSize])=[];
f2.N([b_inds,b_inds+imgSize])=[];


%転置する必要がなかった
%{
mat_f1 = single_to_double(f1.I, f1.J, f1.N);
mat_f2 = single_to_double(f2.I, f2.J, f2.N);
mat_f1 = mat_f1';
mat_f2 = mat_f2';
[f1.I, f1.J, f1.N] = single_sparse_from_matrix(mat_f1);
[f2.I, f2.J, f2.N] = single_sparse_from_matrix(mat_f2);
%}

%[f1.I f1.J f1.N] = single_transposition(f1.I, f1.J, f1.N, h_f1, w_f1);
%[f2.I f2.J f2.N] = single_transposition(f1.I, f1.J, f1.N, h_f1, w_f1);

%f=[f1;f2];
f = struct;
[f.I, f.J, f.N] = single_merge(f1.I, f1.J, f1.N, f2.I, f2.J, f2.N);

disp('end make f');
toc

%tic;
tmp_s = struct;
%rinds1=find(sum(A~=0,2)==0);
[tmp_s.I, tmp_s.J, tmp_s.N] = single_notEqual(A.I, A.J, A.N, 0); 
tmp = single_sum(tmp_s.I, tmp_s.J, tmp_s.N, 2, size(A1,2) * 2, length(tmp_s.I) - 1);
rinds1 = find(tmp == 0);

%rinds2=find(f==0);
tmp_s = struct;
tmp_s2 = struct;
tmp_s3 = struct;
[tmp_s.I, tmp_s.J, tmp_s.N] = single_notEqual(f.I, f.J, f.N, 0);
[tmp_s2.I, tmp_s2.J, tmp_s2.N] = single_sparse_from_matrix(ones(length(f.I)-1, 1));
[tmp_s3.I, tmp_s3.J, tmp_s3.N] = single_sparse_plus(tmp_s2.I, tmp_s2.J, tmp_s2.N, tmp_s.I, tmp_s.J, -tmp_s.N);
rinds2 = single_find(tmp_s3.I, tmp_s3.J, tmp_s3.N, 1, length(tmp_s3.I)-1);

%inds=setdiff([1:size(A,1)],[rinds1;rinds2]);
%inds = single_setdiff([1:length(A.I)-1], [rinds1;rinds2]);
inds = setdiff([1:length(A.I)-1], [rinds1;rinds2]);
%inds size is 2263? too smaaaaaaaaall
% [1:length(A.I)-1] 's size is 284498.normal
%[rinds1;rinds2] 's size is 276624. too biiiiiiiiiiiiiig
disp('end make inds')
toc

%tic;
A_double = single_to_double(A.I, A.J, A.N, size(A1, 2), length(A.I) - 1);
%size(A_double) = 15392 1924
b_double = single_to_double(b.I, b.J, b.N, 1, length(b.I) - 1);
%size(b_double) = 15392 1
f_double = single_to_double(f.I, f.J, f.N, 1, length(f.I) - 1);
%size(f_double) = 19240 1

A_double=A_double(inds, :);
b_double=b_double(inds, :);
f_double=f_double(inds);
A = struct;
b = struct;
f = struct;
[A.I, A.J, A.N] = single_sparse_from_matrix(A_double);
[b.I, b.J, b.N] = single_sparse_from_matrix(b_double);
[f.I, f.J, f.N] = single_sparse_from_matrix(f_double);

oA=A;
ob=b;

%df=spdiags(f,0,length(f),length(f));
df = struct;
[df.I, df.J, df.N] = single_spdiags(f.I, f.J, f.N);

%A=df*oA; b=df*ob;
[A.I, A.J, A.N] = single_sparse_mult(df.I, df.J, df.N, oA.I, oA.J, oA.N);
[b.I, b.J, b.N] = single_sparse_mult(df.I, df.J, df.N, ob.I, ob.J, ob.N);

%x=(A'*A)\(A'*b);
A_double = single_to_double(A.I, A.J, A.N, size(A1, 2), length(A.I) - 1);
b_double = single_to_double(b.I, b.J, b.N, 1, length(b.I)-1);
x_double = (A_double' * A_double)\(A_double' * b_double);
x = struct;
[x.I, x.J, x.N] = single_sparse_from_matrix(x_double);
disp('before loop')
toc

%関数はスパースの入力に対して定義されません
%fprintf('Initial error = %g \n',sum(abs(A_double*x_double-b_double)));
%tic;
for j=1:iter
      %e=abs(oA*x-ob);
      tmp_s = struct;
      e = struct;
      E = struct;

      [tmp_s.I, tmp_s.J, tmp_s.N] = single_sparse_mult(oA.I, oA.J, oA.N, x.I, x.J, x.N);
      [e.I, e.J, e.N] = single_sparse_plus(tmp_s.I, tmp_s.J, tmp_s.N, ob.I, ob.J, -ob.N);
      e.N = abs(e.N);
      %e=max(e,0.00001);
      e.N = max(e.N, 0.00001);
      
      %e=1./e;
      e.N = 1./e.N;
      
      %E=spdiags(e,0,length(f),length(f));
      [E.I, E.J, E.N] = single_spdiags(e.I, e.J, e.N);
      %x=(A'*E*A)\(A'*E*b);
      E_double = single_to_double(E.I, E.J, E.N, length(f.I)-1, length(f.I)-1);
      x_double=(A_double'*E_double*A_double)\(A_double'*E_double*b_double);
      [x.I, x.J, x.N] = single_sparse_from_matrix(x_double);
      %xdsub = single_to_double(x.I, x.J, x.N, x_w, x_h);
      %disp('x_double == xdsub ???')
      %isequal(xdsub, x_double)
      
      %fprintf('iteration = %d, current residue = %g \n', j, sum(abs(A_double*x_double-b_double)));   
end
disp('end loop')
toc

orgI = struct;
[orgI.I, orgI.J, orgI.N] = single_sparse_from_matrix(I);


%I1=reshape(x,h,w);
I1_single = struct;
[I1_single.I, I1_single.J, I1_single.N] = single_reshape(x.I, x.J, x.N, length(x.I)-1, length(x.I)-1, w, h);

%I2=I-I1;
I2_single = struct;
[I2_single.I, I2_single.J, I2_single.N] = single_sparse_plus(orgI.I, orgI.J, orgI.N, I1_single.I, I1_single.J, -I1_single.N);

I1 = single_to_double(I1_single.I, I1_single.J, I1_single.N, w, h);
I1 = full(I1);
I1 = single(I1);
I2 = single_to_double(I2_single.I, I2_single.J, I2_single.N, w, h);
I2 = full(I2);
I2 = single(I2);
