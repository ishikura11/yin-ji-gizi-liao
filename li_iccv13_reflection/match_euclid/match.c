/*
 * match.c
 *
 *  Created on: 2017/10/31
 *      Author: ken
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "match.h"

extern void processingUnit2(double d1[DST_DIM], double d2[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_size);

//#pragma SDS data zero_copy(im1_des[0:size1_dst][0:128], im2_dest[0:128][0:size2_dst])
/*void match(int size1_dst, int size2_dst, int size1y_dst, double im1_des[5000][128],
		   double im2_dest[128][5000], double x1[2000], double x2[2000], int matchnum[5000]) {*/
#pragma SDS data access_pattern(im1_des:SEQUENTIAL, im2_des:SEQUENTIAL)
void match(int size1_dst, int size2_dst, double im1_des[MAX_DST_NUM * DST_DIM],
			   double im2_des[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM], 
			   int tmp_matchnum1[MAX_DST_NUM], int tmp_matchnum2[MAX_DST_NUM]) {
	int i, j, k;//, small_indx;
	double small_val, threshold = 0.6;//, small_val2;
	double _im1_des[MAX_DST_NUM][DST_DIM], _im2_des[MAX_DST_NUM][DST_DIM], euc_d[MAX_DST_NUM], d1[DST_DIM], d1_sub[DST_DIM];

//#pragma HLS array_partition variable=dotprods factor=8 dim=1

	for(i = 0; i < MAX_DST_NUM; i++) {
		if (i < size1_dst) {
			for (j = 0; j < DST_DIM; j++) {
				_im1_des[i][j] = im1_des[i * DST_DIM + j];
				//printf("_im1_des[%d][%d] = %lf\n", i, j, _im1_des[i][j]);
			}
		}
	}

	for (i = 0; i < MAX_DST_NUM; i++) {
		if (i < size2_dst) {
			for(j = 0; j < DST_DIM; j++) {
				_im2_des[i][j] = im2_des[i * DST_DIM + j];
				//printf("im2_des[%d][%d] = %lf\n", i, j, _im2_des[i][j]);
			}
		}
	}

	for(i = 0;i < MAX_DST_NUM/2; i++) {
		if (i < size1_dst/2 + 1) {
			for (j = 0; j < DST_DIM; j++) {
				d1[j] = _im1_des[i][j];
				d1_sub[j] = _im1_des[i + size1_dst/2][j];
			}
			processingUnit2(d1, im2_des, tmp_matchnum1, i, size2_dst);
			processingUnit2(d1_sub, im2_des, tmp_matchnum2, i, size2_dst);
		}
	}
	//normal PE
/*
	for(i = 0;i < MAX_DST_NUM; i++) {
		if (i < size1_dst) {
			for (j = 0; j < DST_DIM; j++) {
				d1[j] = _im1_des[i][j];
			}
			processingUnit2(d1, im2_des, matchnum, i, size2_dst);
		}
	}

	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i < size1_dst) {
			printf("matchnum[%d] = %d\n", i, matchnum[i]);
		}
	}
*/

/*
	for (i = 0; i < MAX_DST_NUM/2; i++) {
		if(i < size1_dst / 2 + 1) {
			matchnum[i] = tmp_matchnum1[i];
			matchnum[i + size1_dst/2] = tmp_matchnum2[i];
		}
	}
	*/
/*
	for (i = 0; i < MAX_DST_NUM/2; i++) {
		if(i < size1_dst/2) {
			printf("matchnum1[%d] = %d\n", i, tmp_matchnum1[i]);
		}
	}
	for (i = 0; i < MAX_DST_NUM/2; i++) {
		if(i < size1_dst/2 + 1) {
			printf("matchnum2[%d] = %d\n", i, tmp_matchnum2[i]);
		}
	}
	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i < size1_dst) {
			printf("matchnum[%d] = %d\n", i, matchnum[i]);
		}
	}
*/

}
