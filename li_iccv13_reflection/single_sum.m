function [ output ] = single_sum( IA, JA, AN, flag, w, h)
%SINGLE_SUM Summary of this function goes here
%   Detailed explanation goes here
    output = single(zeros(length(IA) - 1, 1));

    if flag == 2
        mat = single_to_double(IA, JA, AN, w, h);
        mat = mat';
        [IA JA AN] = single_sparse_from_matrix(mat);
        %[IA JA AN] = single_transposition(IA, JA, AN, length(IA) - 1, length(IA) - 1);
        %tmp = w;
        %w = h;
        %h = tmp;
    end
    for i = 1:length(JA)
        output(JA(i), 1) = output(JA(i), 1) + AN(i);
    end
    if flag == 0
        output = output';
    end
end

