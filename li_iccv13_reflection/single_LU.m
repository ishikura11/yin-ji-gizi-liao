function [ IL, JL, LN, IU, JU, UN ] = single_LU( IA, JA, AN)
%SINGLE_FIND Summary of this function goes here
%   Detailed explanation goes here
	IL = [1];
	IU = [1];
	JL = [];
	JU = [];
	LN = single([]);
	UN = single([]);

	for i = 1:length(IA) - 1
		i
		IL = horzcat(IL, IL(length(IL)));
		IU = horzcat(IU, IU(length(IU)) + 1);
		JU = horzcat(JU, i);
		UN = horzcat(UN, 1);

		for j = i:length(IA) - 1
			j
			% make L
            % L はこの時点では転置した状態で作成されている(後で転置する)
			%if(i >= j)
				ans = single_check_val(IA, JA, AN, j, i)
				for k = 1:i-1
					ans = ans - (single_check_val(IL, JL, LN, k, j) * single_check_val(IU, JU, UN, k, i))
				end
				if(ans ~= 0)
					%IL_count = IL_count + 1
					IL(length(IL)) = IL(length(IL)) + 1
					JL = horzcat(JL, j)
					LN = horzcat(LN, ans)
				end
			%end
		end

		for j = i + 1:length(IA) - 1
            j
            disp('hoge')
			%make U
			%if( i < j)
				if (single_check_val(IL, JL, LN, i, i) ~= 0)
					ans = single_check_val(IA, JA, AN, i, j);
					for k = 1:i-1
						ans = ans - single_check_val(IL, JL, LN, k, i) * single_check_val(IU, JU, UN, k, j);
                    end
                    ans = ans / single_check_val(IL, JL, LN, i, i)
                else
					ans = 0
				end
				if(ans ~= 0)
					%IU_count = IU_count + 1
					IU(length(IU)) = IU(length(IU)) + 1
					JU = horzcat(JU, j)
					UN = horzcat(UN, ans)
				end
			%end
		end
		%IL = horzcat(IL, IL(length(IL)) + IL_count);
		%IU = horzcat(IU, IU(length(IU)) + IU_count);

		%IL_count = 0;
		%IU_count = 0;
	end
	IL;
	JL;
	LN;
	[IL JL LN] = single_transposition(IL, JL, LN, length(IL) - 1, length(IL) - 1);
	IU = IU';
    JU = JU';
    UN = UN';

end
