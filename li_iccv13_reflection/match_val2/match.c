/*
 * match.c
 *
 *  Created on: 2017/10/31
 *      Author: ken
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <complex.h>
//#include "dotprods.h"
#include "match.h"
extern void processingUnit2(double d1[DST_DIM], double d2[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_size);

//#pragma SDS data zero_copy(im1_des[0:size1_dst][0:128], im2_dest[0:128][0:size2_dst])
/*void match(int size1_dst, int size2_dst, int size1y_dst, double im1_des[5000][128],
		   double im2_dest[128][5000], double x1[2000], double x2[2000], int matchnum[5000]) {*/
/* #pragma SDS data sys_port(im1_des:AFI, im2_dest:AFI) */
//#pragma SDS data zero_copy(im1_des[0:size1_dst*128], im2_dest[0:128 * size2_dst])
#pragma SDS data access_pattern(im1_des:SEQUENTIAL, im2_des:SEQUENTIAL)
void match(int size1_dst, int size2_dst, double im1_des[MAX_DST_NUM * DST_DIM],
			   double im2_des[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM]) {
	int i, j, k;//, small_indx;
	double small_val, threshold = 0.6;//, small_val2;
	double _im1_des[MAX_DST_NUM][DST_DIM], _im2_des[MAX_DST_NUM][DST_DIM], euc_d[MAX_DST_NUM], d1[DST_DIM];

//#pragma HLS array_partition variable=dotprods factor=8 dim=1

	for(i = 0; i < MAX_DST_NUM; i++) {
		if (i < size1_dst) {
			for (j = 0; j < DST_DIM; j++) {
				_im1_des[i][j] = im1_des[i * DST_DIM + j];
				//printf("_im1_des[%d][%d] = %lf\n", i, j, _im1_des[i][j]);
			}
		}
	}

	for (i = 0; i < MAX_DST_NUM; i++) {
		if (i < size2_dst) {
			for(j = 0; j < DST_DIM; j++) {
				_im2_des[i][j] = im2_des[i * DST_DIM + j];
				//printf("im2_des[%d][%d] = %lf\n", i, j, _im2_des[i][j]);
			}
		}
	}

	for(i = 0;i < MAX_DST_NUM; i++) {
		if (i < size1_dst) {
			
			for (j = 0; j < DST_DIM; j++) {
				d1[j] = _im1_des[i][j];
			}
			processingUnit2(d1, im2_des, matchnum, i, size2_dst);
			/*
			for (j = 0; j < MAX_DST_NUM; j++) {
				euc_d[j] = 0.;
			}
			for(j = 0; j < MAX_DST_NUM; j++) {
				if(j < size2_dst) {
					for (k = 0; k < DST_DIM; k++) {
						euc_d[j] = euc_d[j] + (_im1_des[i][k] - _im2_des[j][k]) * (_im1_des[i][k] - _im2_des[j][k]); 
						//(A - B) ^ 2
					}
					//euc_d[j] = sqrt(euc_d[j]);
				}
			}

			small_val = 2.;
			for (j = 0; j < MAX_DST_NUM; j++) {
#pragma HLS PIPELINE
				if (j < size2_dst) {
					if (euc_d[j] < small_val) {
						small_val = euc_d[j];
						if(euc_d[j] < threshold)
							matchnum[i] = j;
					}
				}
			}
			*/
		/*
		 * if(dotprods[0].val < distRatio * dotprods[1].val)
		 * matchnum[i] = dotprods[0].indx;
		 */
		}
	}
	/*
	for (i = 0; i < size2_dst; i++) {
		printf("matchnum[%d] = %d\n", i, matchnum[i]);
	}
	*/
}
