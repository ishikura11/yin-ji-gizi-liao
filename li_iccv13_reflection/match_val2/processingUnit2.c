/*
 * processingUnit2.c
 *
 *  Created on: 2017/11/23
 *      Author: ken
 */

#include "match.h"
#include <math.h>
#include "dotprods.h"

/*引数のsmall_val, small_val2, matchnumはポインタなのでここで値を更新するとプログラム全体に
 * 更新が反映される. */
void processingUnit2(double d1[DST_DIM], double d2[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_size) {
	int i, j;
	double small_val, threshold = 0.6, small_val2;
	double _d2[MAX_DST_NUM][DST_DIM], euc_d[MAX_DST_NUM];

	for(i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size) {
			for(j = 0; j < DST_DIM; j++) {
				_d2[i][j] = d2[i * DST_DIM + j];
			}
		}
	}

	for (i = 0; i < MAX_DST_NUM; i++) 
		euc_d[i] = 0.;
small_val = 2.;
small_val2 = 2.;

	for(i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size) {

			for (j = 0; j < DST_DIM; j++) {
				euc_d[i] = euc_d[i] + (d1[j] - _d2[i][j]) * (d1[j] - _d2[i][j]); /*(A - B) ^ 2*/
			}
			//euc_d[i] = sqrt(euc_d[i]);

			if(euc_d[i] < small_val ) {
				small_val2 = small_val;
				small_val = euc_d[i];
				if(euc_d[i] < threshold)
					matchnum[d1_ind] = i;
			} else if (euc_d[i] < small_val2) {
				small_val2 = euc_d[i];
			}
		}
	}
	for (i = 0; i < MAX_DST_NUM; i++) {
		if(small_val >= small_val2 * 0.6) {
			matchnum[d1_ind] = -1;
		}
	}
}


