function img = findFiles(casePath,option)
%Image load with some pre-processing
%   img = findFiles(casePath,option)
%   casePath: file location. will load every image in this path
%   option: some pre-processing options, e.g.
%               downsample - downsample the image to its half size.
%               aligncrop -first roughly align the image using a glaobal homography and crop the images to the shared region of all images.  
%               射影変換を用いてすべての画像で共通した部分だけ残すようにする

curPath = pwd;
cd(casePath);
list = dir();
names = {list(~[list.isdir]).name};
[t, idx] = sort(names);
names = names(idx);
img = cell(1,length(names));
for i = 1: length(names)
    img{i} = imread(names{i});
end
cd(curPath);

if strcmp(option.imtype,'gray') == true
    for i = 1: length(names)
        img{i} = rgb2gray(img{i});
    end
end

if option.downsample == true
    for i = 1: length(names)
        img{i}=imresize(imfilter(img{i},fspecial('gaussian',7,1.),'same','replicate'),0.5,'bicubic');
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if option.aligncrop == true
    addpath('Sift');
    addpath(genpath('MatlabFns'));
    ref = 1;
    refImg = img{ref};
    sz = size(refImg);
    warp{ref}.img = refImg;
    warp{ref}.r = [1 sz(1)]; % row
    warp{ref}.c = [1 sz(2)]; % column
    rmin = 1;
    rmax = sz(1); % row max
    cmin = 1;
    cmax = sz(2); %column max

    cnrPt      =     [ 1, 1;
                               1, sz(1);
                             sz(2), sz(1);
                             sz(2), 1]';
    tsub = toc;
    for i = 2:length(names)
        img2 = img{i};
        
        %[x1,x2] = match(refImg , img2); % refImgとimg2でそれぞれ対応する点を出力
        
        [x1sub,x2sub] = match(refImg , img2); % refImgとimg2でそれぞれ対応する点を出力
        %[im1, des1, loc1] = sift(refImg, 'im1');
        
        eval(['save ', 'x1sub.txt']);
        dlmwrite('x1sub.txt',x1sub,'delimiter','\t');
        eval(['save ', 'x2sub.txt']);
        dlmwrite('x2sub.txt',x2sub,'delimiter','\t');
        disp('pause')
        pause(5);
        
        %eval(command);
        x1 = dlmread('x1.txt');
        x2 = dlmread('x2.txt');
        
        [H, inliers] = ransacfithomography(x2, x1, 0.001); %x2 = H * x1 となるHを出力 この関数にランダム要素がある
        %disp('x1')
        %size(x1)
        %disp('inliers')
        %size(inliers)
        T = maketform('projective',H'); % Hを用いた射影変換に対応するTFORM構造体を作成する
        xp = round(getProjectedPos(H, cnrPt)); 
        [r o1 o2] = imtransform(img2,T,'XYScale',1);
        % img2に射影変換TFORMをかけたものをrとして出力
        % o1は"rの最初の列と最後の列のx座標を指定する2要素ベクトル", 
        % o2は"rの最初の行と最後の行のy座標を指定する2要素ベクトル"size
        warp{i}.img = r;
        warp{i}.r = [round(o2(1)) round(o2(2))];
        warp{i}.c = [round(o1(1)) round(o1(2))];
        rmin = max([rmin, xp(1,1),xp(1,4)]);
        rmax = min([rmax, xp(1,3),xp(1,2)]);
        cmin = max([cmin, xp(2,1),xp(2,2)]);
        cmax = min([cmax, xp(2,3),xp(2,4)]);
    end
    t1 = toc;
    rlim = round([rmin rmax]);
    clim = round([cmin cmax]);
    canvas = cell(1,length(names));

    for i = 1:length(names)
    canvas{i} = zeros( rmax-rmin+1,cmax-cmin+1,3);
    canvas{i} = warp{i}.img(rmin-warp{i}.r(1)+1:rmax-warp{i}.r(1)+1,cmin-warp{i}.c(1)+1:cmax-warp{i}.c(1)+1,:);
    end
    img = canvas;
    fprintf('\n画像の位置合わせ\n')
    disp(toc-t1)
    fprintf('\n前処理合計\n')
    disp(toc-tsub)
end

if option.datatype == 'double'
    for i = 1: length(names)
        img{i} = im2double(img{i});
    end
end
end


function xp = getProjectedPos(H, x1)
x1 = [x1; ones(1,size(x1,2))];
x = H*x1;
x = bsxfun(@rdivide,x,x(3,:));
xp = x([2,1],:);
end
