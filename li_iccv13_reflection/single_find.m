function [ output ] = single_find( IA, JA, AN, w, h)
%SINGLE_FIND Summary of this function goes here
%   Detailed explanation goes here
    mat = single_to_double(IA, JA, AN, w, h);
    mat = mat';
    [tmpIA tmpJA tmpAN] = single_sparse_from_matrix(mat);
    %[tmpIA tmpJA tmpAN] = single_transposition(IA, JA, AN, w, h);
    tmp = w;
    w = h;
    h = tmp;
    
    matrix_size = length(tmpIA) - 1;
    output = [];
    
    for i = 1:length(tmpJA)
        col = find_col(tmpIA, i);
        output = horzcat(output, (col-1) * w + tmpJA(i));
    end
    output = output';
end

