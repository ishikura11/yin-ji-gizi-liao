function [ IB, JB, BN ] = single_reshape( IA, JA, AN, old_w, old_h, w, h)
%SINGLE_RESHAPE Summary of this function goes here
%   Detailed explanation goes here
    %BN = zeros(AN, 1);
    IB = [1];
    JB = [];
    BN = single([]);
    
    len_list = single_find(IA, JA, AN, old_w, old_h);
    [hoge BN] = quicksort(JA, AN);
    
    for i = 1:length(len_list)
        %転置前なのでx, yは逆になっている
        [x, y] = inv_find(w, h, len_list(i));
        %if (length(IB) < y)
            while(length(IB) <= y)
               IB = horzcat(IB, IB(length(IB)));
               %IBind = IBind + 1;
            end
        %else
            IB(length(IB)) = IB(length(IB)) + 1;
            %JB = horzcat(JB, fix((len_list(i) - 1)/h) + 1);
            JB = horzcat(JB, x);
            %BN = horzcat(BN, single_check_val(IA, JA, AN, ))
        %end
    end
    
    IB = IB';
    JB = JB';
    mat = single_to_double(IB, JB, BN, h, w);
    mat = mat';
    [IB JB BN] = single_sparse_from_matrix(mat);
    %[IB JB BN] = single_transposition(IB, JB, BN, h, w);
end

