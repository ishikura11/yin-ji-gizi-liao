function [ IC, JC, CN ] = single_sparse_mult(IA, JA, AN, IB, JB, BN)
%SPARSE_MULT Summary of this function goes here
%   Detailed explanation goes here
% A * B
    IC = [1];
    JC = [];
    CN = single([]);
    IAind = 1;
    ICcount = zeros(1, length(IA) - 1);
    tmpCN = zeros(1, length(IA) - 1);
    flagJC = zeros(1, length(IA) - 1);

    for i = 1:length(JA)
        %�?Aが示す�?の属するIAを探索する
        while((IAind ~= length(IA)) && (IA(IAind) == IA(IAind + 1)))
            IAind = IAind + 1;
            IC = horzcat(IC, IC(length(IC)));
            ICcount = zeros(1, length(IA) - 1);
        end
        
        if IB(JA(i)+1) > IB(JA(i)) %(JA(i)+1)行に要素が存在するか確かめる
            for j = IB(JA(i)):IB(JA(i)+1) - 1
                if ICcount(JB(j)) == 0
                    %JC = horzcat(JC, JB(j))
                    flagJC(JB(j)) = 1;
                end
                
                ICcount(JB(j)) = ICcount(JB(j)) + 1;
                tmpCN(JB(j)) = tmpCN(JB(j)) + AN(i) * BN(j);
                if tmpCN(JB(j)) == 0
                    flagJC(JB(j)) = 0;
                    ICcount(JB(j)) = ICcount(JB(j)) - 1;
                end
            end
        end
        
        if (i+1 > IA(IAind + 1) - 1)
            IAind = IAind + 1;
            ICcount(find(ICcount == 0)) = [];
            IC_total = length(ICcount);
            IC = horzcat(IC, IC_total + IC(length(IC)));
            ICcount = zeros(1, length(IA) - 1);

            tmpCN(find(tmpCN == 0)) = [];
            %CNがsize = [1 0]の空�?��になる�?を避ける
            if(size(tmpCN) == [1 0])
                tmpCN = [];
            end
            CN = horzcat(CN, tmpCN);
            tmpCN = zeros(1, length(IA) - 1);
            JC = horzcat(JC, find(flagJC));
            flagJC = zeros(1, length(IA) - 1);
            %{
            if(length(CN) ~= length(JC))
                CN
                JC
                tmpCN
                flagJC
                i
                pause;
            end
            %}
        end
    end
    
    %adjust last IC
    while(length(IC) ~= length(IA))
        IC = horzcat(IC, IC(length(IC)));
    end
    IC = IC';
    JC = JC';
    CN = CN';
end
