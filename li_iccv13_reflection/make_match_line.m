function [  ] = make_match_line( img1, img2, x1, x2 )
%MAKE_MATCH_LINE この関数の概要をここに記述
%   詳細説明をここに記述
img_size = size(img1);
center_size = 30;
center_space = zeros(img_size(1), center_size, 3);

out_img = horzcat(img1, center_space, img2);
x2(1, :) = x2(1, :) + img_size(2) + center_size;
px = [];
py = [];
for i = 1:length(x1)
   px = horzcat(px, x1(1, i), x2(1, i));
   py = horzcat(py, x1(2, i), x2(2, i));
end
size(px)
size(py)
imshow(out_img);
hold on
plot(x1(1, :), x1(2, :), 'c*');
plot(x2(1, :), x2(2, :), 'y*');
for i = 1:2:length(px)-1
    plot(px(i:i+1), py(i:i+1), '-');
end
%plot(x1, x2, '-');
%plot(x1(1, :), x1(2, :), x2(1, :), x2(2, :), '-');
%plot(x1(1, :), x1(2, :), '-');
%plot(x2(1, :) , x2(2, :), '-');
%plot(x1(1, 1), x1(2, 1), x2(1, 1), x2(2, 1), '-');


end

