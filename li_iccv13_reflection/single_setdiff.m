function [ IC, JC, CN ] = single_setdiff( IA, JA, AN, IB, JB, BN)
%SINGLE_SETDIFF Summary of this function goes here
%   Detailed explanation goes here
    CN = setdiff(AN, BN);
    IC = [1:length(CN) + 1];
    JC = [1:length(CN)];
end

