function [ IA, JA, AN ] = single_sparse_from_val( x, y, val, w, h)
%SINGLE_SPARSE_FROM_VAL Summary of this function goes here
%   Detailed explanation goes here
    IA = zeros(h+1, 1);
    IA(1) = 1;
    JA = y;
    AN = single(val);
    plusCount = zeros(length(IA), 1);
    
    for i = 1:length(x)
        plusCount(x(i)+1) = plusCount(x(i)+1) + 1;
    end
    
    for i = 2:length(IA)
        IA(i) = IA(i - 1) + plusCount(i);
    end
end

