clear all;
addpath('SiftFlow');
%diary('result_03S.txt');
%% preparing data
tic;
dataPath = 'data/case01';
option.datatype = 'double';
option.imtype= 'rgb';
option.downsample = true;
option.aligncrop = true;

toc, fprintf('no.1\n');

%tic;
img = findFiles(dataPath,option);

imNum = length(img);
clear casePath option

%% configuration
ref = 1;
%sift params 
patchsize=8;
gridspacing=1;
%siftflow params
SIFTflowpara.alpha= 2; %smoothness
SIFTflowpara.d=40;
SIFTflowpara.gamma=0.01; %size of flow
SIFTflowpara.nlevels=4;
SIFTflowpara.wsize=5;
SIFTflowpara.topwsize=20;
SIFTflowpara.nIterations=60;

%gradient
w = fspecial('sobel');
grad = cell(1,imNum);

for i = 1:length(img)
    for ch = 1:size(img{i},3)
        gx(:,:,ch) = imfilter(img{i}(:,:,ch),w);
        gy(:,:,ch) = imfilter(img{i}(:,:,ch),w');
    end
    grad{i} = sqrt(gx.^2+gy.^2);
    grad{i} = max(grad{i},[],3); 
end

%% dense sift feature extraction and flow estimation

sift = cell(1,imNum);
fprintf('Dense Sift Extraction');
for i = 1:length(img)
    fprintf('..');
    sift{i} =  dense_sift(img{i},patchsize,gridspacing);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for i = 1:length(img)
        figure;imshow(showColorSIFT(sift{i}));title('SIFT image');
    end
    close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n Sift Flow Estimation');
for i = setdiff(1:length(img),ref)
    fprintf([' ', num2str(i)]);
    [vx{i},vy{i},energylist{i}]=SIFTflowc2f(double(sift{ref}),double(sift{i}),SIFTflowpara);
end
toc, fprintf('no.2\n');

%tic;

warpIm = cell(1,imNum);
warpGrad = cell(1,imNum);


for i = 1: length(img)
    warpIm{i} = img{i}(patchsize/2:end-patchsize/2+1,patchsize/2:end-patchsize/2+1,:);
    warpGrad{i} = grad{i}(patchsize/2:end-patchsize/2+1,patchsize/2:end-patchsize/2+1);
    if i ~=ref
        warpIm{i}=warpImage(warpIm{i},vx{i},vy{i});
        warpGrad{i} = warpImage(warpGrad{i},vx{i},vy{i});
    end
    gradT(:,:,i) = warpGrad{i};
end

toc, fprintf('no.3\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%
for i = 1:length(img)
       figure,  subplot 121;imshow(warpIm{i});title('warped img');
                   subplot 122;imshow(warpGrad{i});title('warped grad');
end
%     close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars -except img ref warpIm warpGrad gradT
fprintf('\n');%toc

%tic;

%% edge likelihood estimation 
gradThrshd = 0.2; 
gradTn = sum(bsxfun(@rdivide,gradT,sum(gradT,3)).^2,3);
edgeProbF =  imadjust(gradTn).*(warpGrad{ref}>gradThrshd);
edgeProbF = 1./(1+exp(-(edgeProbF-0.05)/0.05));
edgeProbB = (1-edgeProbF).*(warpGrad{ref}>gradThrshd);
figure,subplot(121),imagesc(edgeProbF); title('reflection likelihood');
axis image,%colorbar
subplot(122),imagesc(edgeProbB); title('background likelihood');
axis image,%colorbar

toc, fprintf('no.4\n');

%tic;
%% layer reconstruction
L1 = cell(1,length(warpIm));
L2 = cell(1,length(warpIm));
[h,w,d] = size(warpGrad{1});
G = struct;
[G.Gx,G.Gy,G.Gxx,G.Gxy,G.Gyy]=getGMat(w,h);

for i = 1:length(warpIm)
    warpIm{i} = single(warpIm{i});
end

for ind = 1: length(warpIm)
    %tic;
    ind
    edgeProbF =  imadjust(gradTn).*(warpGrad{ind}>gradThrshd);
    edgeProbF = 1./(1+exp(-(edgeProbF-0.05)/0.05));
    edgeProbB = (1-edgeProbF).*(warpGrad{ind}>gradThrshd);
    indF = find((edgeProbF>0.8)==1);
    indB = find((edgeProbB>0.5)==1);
    % dead
    disp('layerSepIRLS実行前まで')
    toc
    for ch = 1:3
        %tic;
        ch
        [L1{ind}(:,:,ch) L2{ind}(:,:,ch)] = single_layerSepIRLS ( warpIm{ind}(:,:,ch), G, indF, indB);
        disp('layerSepIRLS1周終わり')
        toc
    end
end
%clear G gradT gradTn edgeProbF edgeProbB indB indF 

%toc, fprintf('no.5\n');

%tic;
%% final combination
for i = 1:length(img)
     if i == 1 
         L2min = L2{i};
     else
         L2min = min( L2min, L2{i});
     end
end
 figure,    subplot 121;imshow(L2min);title('Background');
                subplot 122;imshow(0.5+warpIm{ref}-L2min);title('Reflection');
    
toc, fprintf('no.6,7\n');

%diary off;

%% write the output
imwrite(L2min,'LB.png');
imwrite(0.5+warpIm{ref}-L2min,'LR.png');


