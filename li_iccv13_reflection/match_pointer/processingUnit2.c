/*
 * processingUnit2.c
 *
 *  Created on: 2017/11/23
 *      Author: ken
 */

#include "match.h"
#include <math.h>
#include "dotprods.h"
#include <stdio.h>

extern void make_euc_d(double d1[DST_DIM], double d2[DST_DIM], double euc_d[MAX_DST_NUM], int d2_ind) ;

/*引数のsmall_val, small_val2, matchnumはポインタなのでここで値を更新するとプログラム全体に
 * 更新が反映される. */
//void processingUnit2(double d1[DST_DIM], double d2[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_size) {
void processingUnit2(double d1[DST_DIM], double d2[MAX_DST_NUM * DST_DIM], int *matchnum, int d1_ind, int d2_size) {
	int i, j;
	double small_val = 2., threshold = 0.6, small_val2 = 2.;
	double _d2[MAX_DST_NUM][DST_DIM], euc_d[MAX_DST_NUM], __d2[DST_DIM], tmp_euc_d[DST_DIM];
		
	for(i = 0; i < MAX_DST_NUM; i++) {
		euc_d[i] = 0.;
		if(i < d2_size) {
			for(j = 0; j < DST_DIM; j++) {
				_d2[i][j] = d2[i * DST_DIM + j];
			}
		}
	}
/*
	for (i = 0; i < DST_DIM; i++) 
		tmp_euc_d[i] = 0.;
*/

/*	
	for(i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size) {
			for (j = 0; j < DST_DIM; j++) {
				tmp_euc_d[j] = (d1[j] - _d2[i][j]) * (d1[j] - _d2[i][j]); 
				//euc_d[i] = euc_d[i] + (d1[j] - _d2[i][j]) * (d1[j] - _d2[i][j]); (A - B) ^ 2
			}
			//euc_d[i] = sqrt(euc_d[i]);

			for (j = 0; j < DST_DIM; j++) {
				euc_d[i] = euc_d[i] + tmp_euc_d[j];
			}

			if(euc_d[i] < small_val ) {
				small_val2 = small_val;
				small_val = euc_d[i];
				if(euc_d[i] < threshold)
					matchnum[d1_ind] = i;
			} else if(euc_d[i] < small_val2) {
				small_val2 = euc_d[i];
			}
		}
	}
	if(small_val >= small_val2 * 0.6) {
		matchnum[d1_ind] = -1;
	}
*/
	for(i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size) {
			/*
			for(j = 0; j < DST_DIM; j++) {
				__d2[j] = _d2[i][j];
			}
			make_euc_d(d1, __d2, euc_d, i);
			*/
			for (j = 0; j < DST_DIM; j++) {
				tmp_euc_d[j] = (d1[j] - _d2[i][j]) * (d1[j] - _d2[i][j]); 
				//euc_d[i] = euc_d[i] + (d1[j] - _d2[i][j]) * (d1[j] - _d2[i][j]); (A - B) ^ 2
			}
			//euc_d[i] = sqrt(euc_d[i]);
		
			for (j = 0; j < DST_DIM; j++) {
				euc_d[i] = euc_d[i] + tmp_euc_d[j];
			}
			
		}
	}
	int count = 0;
	
	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size) {
			if(euc_d[i] > 1.1) {
				euc_d[i] = 9999.;
				//printf("euc_d[%d] = %lf\n", i, euc_d[i]);
				//count++;
			}
		}
	}
	

	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size && euc_d[i] < small_val2) {
			if(euc_d[i] < small_val ) {
				small_val2 = small_val;
				small_val = euc_d[i];
				if(euc_d[i] < small_val2 * threshold) {
					*matchnum = i;
					//matchnum[d1_ind] = i;
				} else {
					//matchnum[d1_ind] = -1;
					*matchnum = -1;
				}
			} else{
				small_val2 = euc_d[i];
				if(small_val > small_val2 * threshold)
					*matchnum = -1;
					//matchnum[d1_ind] = -1;
			}
		}
	}
}


