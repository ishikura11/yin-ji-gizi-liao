/*
 * processingUnit.c
 *
 *  Created on: 2017/11/14
 *      Author: ken
 */

#include "match.h"
#include <math.h>

void processingUnit(double d1[DST_DIM], double d2[DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_ind,
					double small_val, double small_val2) {
	int i, tmp;
	double distRatio = 0.6, dotprods = 0;
	//double small_val, small_val2;

	for (i = 0; i < DST_DIM; i++) {
		dotprods = dotprods + d1[i] * d2[i];
	}

	tmp = dotprods;
	dotprods = M_PI / 2 - tmp - (tmp * tmp * tmp)/(2 * 3) -
				   (3 * tmp * tmp * tmp * tmp * tmp) / (2 * 2 * 2 * 5);

	/* qsort(dotprods, size2_dst, sizeof(DOTPRODS), comp);*/
	//small_val = 1.; small_val2 = 1.; /* 0 < dotprods[i].val < 1. */

	if(dotprods < small_val) {
		small_val2 = small_val;
		small_val = dotprods;
		if(small_val < small_val2 * distRatio)
			matchnum[d1_ind] = d2_ind;
		else matchnum[d1_ind] = -1;
	} else if(dotprods < small_val2) {
		small_val2 = dotprods;
		if(small_val >= distRatio * small_val2)
			matchnum[d1_ind] = -1;
	}
}
/*引数のsmall_val, small_val2, matchnumはポインタなのでここで値を更新するとプログラム全体に
 * 更新が反映される. */
