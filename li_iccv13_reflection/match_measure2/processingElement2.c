/*
 * processingUnit2.c
 *
 *  Created on: 2017/11/23
 *      Author: ken
 */

#include "match.h"
#include <math.h>
#include <stdio.h>


extern void make_euc_d(double d1[DST_DIM], double d2[DST_DIM], double euc_d[MAX_DST_NUM], int d2_ind);

void processingElement2(double d1[DST_DIM], double d2[MAX_DST_NUM][DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_size) {
	int i, j;
	double small_val = 2., small_val2 = 2., threshold = 0.6;
	double euc_d[MAX_DST_NUM], tmp_euc_d[DST_DIM], tmp[DST_DIM-1];


//#pragma HLS array_partition variable=euc_d complete
//#pragma HLS array_partition variable=tmp_euc_d complete
//#pragma HLS array_partition variable=tmp complete

	/*
	for(i = 0; i < MAX_DST_NUM; i++) {
#pragma HLS pipeline
		euc_d[i] = 0.;
	}
*/
	for(i = 0; i < MAX_DST_NUM; i++) {
#pragma HLS pipeline
		if(i >= d2_size) break;
			for (j = 0; j < DST_DIM; j++) {
//#pragma HLS unroll factor = 128
				tmp_euc_d[j] = (d1[j] - d2[i][j]) * (d1[j] - d2[i][j]);
			}

			for (j = 0; j < DST_DIM/2; j++) { //0~63
//#pragma HLS unroll factor = 64
				tmp[j] = tmp_euc_d[0 + j * 2] + tmp_euc_d[1 + j * 2];
				//tmp[0~63]
			}


			for (j = 0; j < DST_DIM/2; j+=2) { //0~31
//#pragma HLS unroll factor = 32
				tmp[j/2 + DST_DIM/2] = tmp[0 + j] + tmp[1 + j];
				//tmp[64~95] = tmp[0~62] + tmp[1~63]
			}

			for (j = 0; j < DST_DIM/4; j+=2) { //0~15
//#pragma HLS unroll factor = 16
				tmp[j/2 + 3 * DST_DIM/4] = tmp[0 + j + DST_DIM/2] + tmp[1 + j + DST_DIM/2];
				//tmp[96~111] = tmp[64~94] + tmp[65~95]
			}

			for (j = 0; j < DST_DIM/8; j+=2) { //0~7
//#pragma HLS unroll factor = 8
				tmp[j/2 + 7 * DST_DIM/8] = tmp[0 + j + 3 * DST_DIM/4] + tmp[1 + j + 3 * DST_DIM/4];
				//tmp[112~119] = tmp[96~110] + tmp[97~111]
			}

			for (j = 0; j < DST_DIM/16; j+=2) { //0~3
//#pragma HLS unroll factor = 4
				tmp[j/2 + 15 * DST_DIM/16] = tmp[0 + j + 7 * DST_DIM/8] + tmp[1 + j + 7 * DST_DIM/8];
				//tmp[120~123] = tmp[112~118] + tmp[113~119]
			}

			for (j = 0; j < DST_DIM/32; j+=2) { //0~1
//#pragma HLS unroll factor = 2
				tmp[j/2 + 31 * DST_DIM/32] = tmp[0 + j + 15 * DST_DIM/16] + tmp[1 + j + 15 * DST_DIM/16];
				//tmp[124, 125] = tmp[120, 122] + tmp[121, 123]
			}

			//tmp[126] = tmp[124] + tmp[125];
			euc_d[i] = tmp[124] + tmp[125];

/*
			tmp[64] = tmp[0] + tmp[1];
			tmp[65] = tmp[2] + tmp[3];
			tmp[66] = tmp[4] + tmp[5];
			tmp[67] = tmp[6] + tmp[7];
			tmp[68] = tmp[8] + tmp[9];
			tmp[69] = tmp[10] + tmp[11];
			tmp[70] = tmp[12] + tmp[13];
			tmp[71] = tmp[14] + tmp[15];
			tmp[72] = tmp[16] + tmp[17];
			tmp[73] = tmp[18] + tmp[19];
			tmp[74] = tmp[20] + tmp[21];
			tmp[75] = tmp[22] + tmp[23];
			tmp[76] = tmp[24] + tmp[25];
			tmp[77] = tmp[26] + tmp[27];
			tmp[78] = tmp[28] + tmp[29];
			tmp[79] = tmp[30] + tmp[31];
			tmp[80] = tmp[32] + tmp[33];
			tmp[81] = tmp[34] + tmp[35];
			tmp[82] = tmp[36] + tmp[37];
			tmp[83] = tmp[38] + tmp[39];
			tmp[84] = tmp[40] + tmp[41];
			tmp[85] = tmp[42] + tmp[43];
			tmp[86] = tmp[44] + tmp[45];
			tmp[87] = tmp[46] + tmp[47];
			tmp[88] = tmp[48] + tmp[49];
			tmp[89] = tmp[50] + tmp[51];
			tmp[90] = tmp[52] + tmp[53];
			tmp[91] = tmp[54] + tmp[55];
			tmp[92] = tmp[56] + tmp[57];
			tmp[93] = tmp[58] + tmp[59];
			tmp[94] = tmp[60] + tmp[61];
			tmp[95] = tmp[62] + tmp[63];

			tmp[96] = tmp[64] + tmp[65];
			tmp[97] = tmp[66] + tmp[67];
			tmp[98] = tmp[68] + tmp[69];
			tmp[99] = tmp[70] + tmp[71];
			tmp[100] = tmp[72] + tmp[73];
			tmp[101] = tmp[74] + tmp[75];
			tmp[102] = tmp[76] + tmp[77];
			tmp[103] = tmp[78] + tmp[79];
			tmp[104] = tmp[80] + tmp[81];
			tmp[105] = tmp[82] + tmp[83];
			tmp[106] = tmp[84] + tmp[85];
			tmp[107] = tmp[86] + tmp[87];
			tmp[108] = tmp[88] + tmp[89];
			tmp[109] = tmp[90] + tmp[91];
			tmp[110] = tmp[92] + tmp[93];
			tmp[111] = tmp[94] + tmp[95];

			tmp[112] = tmp[96] + tmp[97];
			tmp[113] = tmp[98] + tmp[99];
			tmp[114] = tmp[100] + tmp[101];
			tmp[115] = tmp[102] + tmp[103];
			tmp[116] = tmp[104] + tmp[105];
			tmp[117] = tmp[106] + tmp[107];
			tmp[118] = tmp[108] + tmp[109];
			tmp[119] = tmp[110] + tmp[111];

			tmp[120] = tmp[112] + tmp[113];
			tmp[121] = tmp[114] + tmp[115];
			tmp[122] = tmp[116] + tmp[117];
			tmp[123] = tmp[118] + tmp[119];

			tmp[124] = tmp[120] + tmp[121];
			tmp[125] = tmp[122] + tmp[123];

			euc_d[i] = tmp[124] + tmp[125];

			/*
			for (j = 0; j < 8; j++) {
#pragma HLS unroll factor = 8
				tmp[j]  = 0.;
			}

			for (j = 0; j < DST_DIM; j = j+16) {
#pragma HLS pipeline

				for (k = 0; k < 8; k++) {
					tmp[k] = tmp_euc_d[j + k * 2] + tmp_euc_d[j + k * 2 + 1] + tmp[k];
				}

				tmp[0]  = tmp_euc_d[j]    + tmp_euc_d[j+1]  + tmp[0];
				tmp[1]  = tmp_euc_d[j+2]  + tmp_euc_d[j+3]  + tmp[1];
				tmp[2]  = tmp_euc_d[j+4]  + tmp_euc_d[j+5]  + tmp[2];
				tmp[3]  = tmp_euc_d[j+6]  + tmp_euc_d[j+7]  + tmp[3];
				tmp[4]  = tmp_euc_d[j+8]  + tmp_euc_d[j+9]  + tmp[4];
				tmp[5]  = tmp_euc_d[j+10] + tmp_euc_d[j+11] + tmp[5];
				tmp[6]  = tmp_euc_d[j+12] + tmp_euc_d[j+13] + tmp[6];
				tmp[7]  = tmp_euc_d[j+14] + tmp_euc_d[j+15] + tmp[7];
 * ここ以上に増やすと速度が落ちる.
				tmp[8]  = tmp_euc_d[j+16] + tmp_euc_d[j+17] + tmp[8];
				tmp[9]  = tmp_euc_d[j+18] + tmp_euc_d[j+19] + tmp[9];
				tmp[10] = tmp_euc_d[j+20] + tmp_euc_d[j+21] + tmp[10];
				tmp[11] = tmp_euc_d[j+22] + tmp_euc_d[j+23] + tmp[11];
				tmp[12] = tmp_euc_d[j+24] + tmp_euc_d[j+25] + tmp[12];
				tmp[13] = tmp_euc_d[j+26] + tmp_euc_d[j+27] + tmp[13];
				tmp[14] = tmp_euc_d[j+28] + tmp_euc_d[j+29] + tmp[14];
				tmp[15] = tmp_euc_d[j+30] + tmp_euc_d[j+31] + tmp[15];

			}
			euc_d[i] = euc_d[i] + tmp[0]  + tmp[1]  + tmp[2]  + tmp[3]
								+ tmp[4]  + tmp[5]  + tmp[6]  + tmp[7];
//								+ tmp[8]  + tmp[9]  + tmp[10] + tmp[11]
//								+ tmp[12] + tmp[13] + tmp[14] + tmp[15];
 *
 */
		//}
	}

	for(i = 0; i < MAX_DST_NUM; i++) {
#pragma HLS pipeline
		if (i >= d2_size) break;
		if(i < d2_size && euc_d[i] < small_val2) {
			if(euc_d[i] < small_val ) {
				small_val2 = small_val;
				small_val = euc_d[i];
				if(small_val < small_val2 * threshold ) {
					matchnum[d1_ind] = i;
				} else {
					matchnum[d1_ind] = -1;
				}
			} else /*if(euc_d[i] < small_val2)*/ {
				small_val2 = euc_d[i];
				if(small_val > small_val2 * threshold )
					matchnum[d1_ind] = -1;
			}
		}
	}
}


