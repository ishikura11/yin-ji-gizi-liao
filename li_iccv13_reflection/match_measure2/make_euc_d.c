/*
 * make_euc_d.c
 *
 *  Created on: 2017/12/14
 *      Author: ken
 */

#include "match.h"

void make_euc_d(double d1[DST_DIM], double d2[DST_DIM], double euc_d[MAX_DST_NUM], int d2_ind) {
	double tmp_euc_d[DST_DIM];
	int i;

//#pragma HLS array_partition variable=tmp_euc_d complete
//#pragma HLS array partition variable=d1 complete
//#pragma HLS array partition variable=d2 complete
/*
	for(i = 0; i < DST_DIM; i++) {
#pragma HLS unroll
		tmp_euc_d[i] = 0;
	}
*/
	for (i = 0; i < DST_DIM; i++) {
#pragma HLS unroll //factor=128
		//euc_d[i] = euc_d[i] + (d1[j] - _d2[i][j]) * (d1[j] - _d2[i][j]); (A - B) ^ 2
		tmp_euc_d[i] = (d1[i] - d2[i]) * (d1[i] - d2[i]);
	}
	//euc_d[i] = sqrt(euc_d[i]);

	for (i = 0; i < DST_DIM; i++) {
#pragma HLS pipeline
		euc_d[d2_ind] = euc_d[d2_ind] + tmp_euc_d[i];
	}
}
