/*
 * match.c
 *
 *  Created on: 2017/10/31
 *      Author: ken
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "match.h"


extern void processingElement2(double d1[DST_DIM], double d2[MAX_DST_NUM][DST_DIM],
							   int matchnum[MAX_DST_NUM], int d1_ind, int d2_size);

#pragma SDS data access_pattern(im1_des:SEQUENTIAL, im2_des:SEQUENTIAL)
void match(int size1_dst, int size2_dst, double im1_des[MAX_DST_NUM * DST_DIM],
			   double im2_des[MAX_DST_NUM * DST_DIM], int matchnum[MAX_DST_NUM]) {
	int i, j;//, k;
	double _im1_des[MAX_DST_NUM][DST_DIM], d1[DST_DIM], _im2_des[MAX_DST_NUM ][DST_DIM];//, euc_d[MAX_DST_NUM];

#pragma HLS array_partition variable=_im1_des block factor=128 dim=2 //128
#pragma HLS array_partition variable=_im2_des block factor=128 dim=2
#pragma HLS array_partition variable=d1 block factor=128
//#pragma HLS array_partition variable=matchnum factor=2

	for(i = 0; i < MAX_DST_NUM; i++) {
		for (j = 0; j < DST_DIM; j++) {
#pragma HLS pipeline
			_im1_des[i][j] = im1_des[i * DST_DIM + j];
			_im2_des[i][j] = im2_des[i * DST_DIM + j];
		}
	}


	for(i = 0;i < MAX_DST_NUM; i++) {
		if (i >= size1_dst) break;
			for(j = 0; j < DST_DIM; j++) {
#pragma HLS unroll factor = 128
				d1[j] = _im1_des[i][j];
			}
			processingElement2(d1, _im2_des, matchnum, i, size2_dst);
		
	}
}
