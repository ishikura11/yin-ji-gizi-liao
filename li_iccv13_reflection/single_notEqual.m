function [ IB, JB, BN ] = single_notEqual( IA, JA, AN, val )
%SINGLE_NOTEQUAL Summary of this function goes here
%   Detailed explanation goes here
    for i = 1:length(JA)
        if AN(i) ~= val
            AN(i) = 1;
        else
            col = find_col(IA, i);
            JA(i) = 0;
            AN(i) = 0;
            IA(col+1:length(IA)) = IA(col+1:length(IA)) - 1;
        end
    end
    IB = IA;
    JB = JA;
    BN = AN;
    
    JB(find(JB == 0)) = [];
    BN(find(BN == 0)) = [];
end

