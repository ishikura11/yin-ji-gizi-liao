function [ IA, JA, AN, w, h ] = single_sparse_from_matrix(matrix)
    [h w] = size(matrix);

    IA = zeros(h+1, 1);
    IA(1) = 1;
    IAcount = zeros(h, 1);
    %JA = [];
    %AN = single([]);
    [JA y AN] = find(matrix');
    
    for i = 1:length(y)
        IAcount(y(i)) = IAcount(y(i)) + 1;
    end

    for i = 1:length(IAcount)
        IA(i+1) = IA(i) + IAcount(i);
    end
    if size(JA, 1) == 1
        JA = JA';
        AN = single(AN');
    end
end

