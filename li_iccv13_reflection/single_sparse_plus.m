function [ IC, JC, CN ] = single_sparse_plus(IA, JA, AN, IB, JB, BN)
%SINGLE_SPARSE_PLUS Summary of this function goes here
%   Detailed explanation goes here
% A + B
    IC = [1];
    JC = [];
    CN = single([]);
    inds_AN = 1;
    inds_BN = 1;
    %tmpJCa = zeros(1);
    %tmpJCb = zeros(1);

    for i = 1:length(IA)-1
        tmpJCa = make_tmp_JC(i, IA, JA);
        tmpJCb = make_tmp_JC(i, IB, JB);
        tmpJC = unique(horzcat(tmpJCa, tmpJCb));
        tmpCN = zeros(1, max(tmpJC));
        
        for j = 1:length(tmpJCa)
            %AN
            tmpCN(tmpJCa(j)) = tmpCN(tmpJCa(j)) + AN(inds_AN);
            inds_AN = inds_AN + 1;
        end
        for j = 1:length(tmpJCb)
            %BN
            tmpCN(tmpJCb(j)) = tmpCN(tmpJCb(j)) + BN(inds_BN);
            inds_BN = inds_BN + 1;
        end
        no_tmpCN = setdiff([1:max(tmpJC)], tmpJC);
        tmpCN(no_tmpCN) = [];
        IC = horzcat(IC, IC(length(IC)) + length(tmpJC));
        JC = horzcat(JC, tmpJC);
        CN = horzcat(CN, tmpCN);
    end
    
    %行列中にある0の値を削除する
    zero_list = [];
    IFcount = zeros(1, length(IC));
    for j = 1:length(CN)
        if CN(j) == 0
            del = find_col(IC, j);
            IFcount(del+1:length(IFcount)) = IFcount(del+1:length(IFcount)) + 1;
            zero_list = horzcat(zero_list, j);
        end
    end
    IC = IC - IFcount;
    JC(zero_list) = [];
    CN(zero_list) = [];
    IC = IC';
    JC = JC';
    CN = CN';
    %IC = single(IC);
    %JC = single(JC);
end