function [col] = find_col(IA, JAind)
    %JAind番目にあるJAの要素が何行目の要素か調べる
    %二分探索
    col = 0;
    %{
    for i = 2:length(IA)
        if IA(i-1) <= JAind && JAind < IA(i)
            col = i - 1;
            break;
        end
    end
    %}
    l = 1;
    r = length(IA);
    while r - l > 0
        if IA(fix((r + l) / 2)+1) > JAind
            if IA(fix((r + l) / 2)) <= JAind
                col = fix((r + l) / 2);
                return;
            else
                r = fix((r + l) / 2);
            end
        else
            l = fix((r + l) / 2) + 1;
        end
    end
    
end