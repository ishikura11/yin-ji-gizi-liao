function [ matrix ] = single_to_double( IA, JA, AN, w, h)
%SINGLE_TO_DOUBLE Summary of this function goes here
%   Detailed explanation goes here
    %mat_size = length(IA) - 1;
    x = [];
    y = [];
    val = [];
    
    for i = 1:length(JA)
        y = horzcat(y, JA(i));
        col = find_col(IA, i);
        x = horzcat(x, col);
        val = horzcat(val, double(AN(i)));
    end
    matrix = sparse(x, y, val, h, w);
end

