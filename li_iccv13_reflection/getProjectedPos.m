function xp = getProjectedPos(H, x1)
x1 = [x1; ones(1,size(x1,2))];
x = H*x1;
x = bsxfun(@rdivide,x,x(3,:));
xp = x([2,1],:);
end