
***************************************************************************************
** Matlab code and data for "Exploiting Reflection Change for Automatic Reflection Removal" (ICCV 2013)
** by Yu Li (liyu@nus.edu.sg)
**	
** V 1.0 (Dec. 2013)
** -Demo code showing the reflection separation process.
**   
** v 1.1 (Apr. 2014)
** -Rewrite part of the code with the layer reconstruction part added.
***************************************************************************************


If you use/adapt our code/data in your work, please appropriately cite our ICCV 2013 paper.


This code also includes:

- SIFT keypoint detector by David Lowe:
http://www.cs.ubc.ca/~lowe/keypoints/

- Part of the computer vision and image processing toolbox by Peter Kovesi:
http://www.csse.uwa.edu.au/~pk/Research/MatlabFns/index.html

- SIFT-flow estimation code by Ce liu which is used for align the input images: 
http://people.csail.mit.edu/celiu/SIFTflow/SIFTflow.zip 

- A modified IRLS solver from Anat Levin's code:
http://www.wisdom.weizmann.ac.il/~levina/papers/reflections.zip

***************************************************************************************
***************************************************************************************
 