function [ IB, JB, BN ] = single_spdiags( IA, JA, AN)
%SINGLE_SPDIAGS Summary of this function goes here
%   Detailed explanation goes here
    IB = [1];
    JB = [];
    BN = single([]);
    IAind = 1;
    for i = 1:length(JA)
        if JA(i) == 1
            elmJB = find_col(IA, i);
            IB = horzcat(IB, IB(length(IB)) + 1);
            IAind = IAind + 1;
            JB = horzcat(JB, elmJB);
            BN = horzcat(BN, AN(i));
        end
        if find_col(IA, i) + 1 > IAind 
            hoge = find_col(IA, i) + 1;
            IB = horzcat(IB, IB(length(IB)));
            IAind = IAind + 1;
        end
    end
end

