/*
 * processingUnit2.c
 *
 *  Created on: 2017/11/23
 *      Author: ken
 */

#include "match.h"
#include <math.h>
#include <stdio.h>

//extern void make_euc_d(double d1[DST_DIM], double d2[DST_DIM], double euc_d[MAX_DST_NUM], int d2_ind) ;

/*引数のsmall_val, small_val2, matchnumはポインタなのでここで値を更新するとプログラム全体に
 * 更新が反映される. */
//void processingUnit2(double d1[DST_DIM], double d2[MAX_DST_NUM][DST_DIM], int matchnum[MAX_DST_NUM], int d1_ind, int d2_size) {
void processingUnit2(double d1[DST_DIM], double d2[MAX_DST_NUM][DST_DIM], int *matchnum, int d2_size) {
	int i, j;
	double small_val = 2., threshold = 0.8, small_val2 = 2.;
	double euc_d[MAX_DST_NUM], tmp_euc_d[DST_DIM];
		
	for(i = 0; i < MAX_DST_NUM; i++) {
		euc_d[i] = 0.;
	}

	for(i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size) {
			/*
			for(j = 0; j < DST_DIM; j++) {
				__d2[j] = _d2[i][j];
			}
			make_euc_d(d1, __d2, euc_d, i);
			*/
			for (j = 0; j < DST_DIM; j++) {
				//euclid
				//tmp_euc_d[j] = (d1[j] - d2[i][j]) * (d1[j] - d2[i][j]); 
				
				//manhattan
				tmp_euc_d[j] = fabs(d1[j] - d2[i][j]);

			}
		
			for (j = 0; j < DST_DIM; j++) {
				euc_d[i] = euc_d[i] + tmp_euc_d[j];
			}
			
		}
	}

	for (i = 0; i < MAX_DST_NUM; i++) {
		if(i < d2_size && euc_d[i] < small_val2) {
			if(euc_d[i] < small_val ) {
				small_val2 = small_val;
				small_val = euc_d[i];
				if(euc_d[i] < small_val2 * threshold) {
					//matchnum[d1_ind] = i;
					*matchnum = i;
				} else {
					//matchnum[d1_ind] = -1;
					*matchnum = -1;
				}
			} else{
				small_val2 = euc_d[i];
				if(small_val > small_val2 * threshold)
					//matchnum[d1_ind] = -1;
					*matchnum = -1;
			}
		}
	}
}


