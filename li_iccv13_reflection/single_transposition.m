function [ IB, JB, BN ] = single_transposition( IA, JA, AN, w, h)
%TRANSPOSITION Summary of this function goes here
%   Detailed explanation goes here
%TODO outargs lost "w", "h"

    IB = [1];
	JB = [];
	BN = single([]);
	

	% make IB
	JAind = 1;
	count = 0;
	sortJA = sort(JA);
    
    %JAγζ?ι ?«γ½γΌγγγ¦γγγγ?θ‘γγ¨γ«ε­ε¨γγθ¦η΄?°γζ°γδΈγγ?	%for i = 1:length(IA) - 1
	for i = 1:w
		while(JAind <= length(sortJA) && sortJA(JAind) == i)
			count = count + 1;
			JAind = JAind + 1;
			if JAind > length(sortJA)
				break;
			end
		end
		IB = horzcat(IB, IB(length(IB)) + count);
		count = 0;
    end	
   	
	% make JB
    subJA = JA;
    tmpJA = sort(JA);
    for i = 1:length(tmpJA)
        i
        for j = 1:length(subJA)
            if(tmpJA(i) == subJA(j))
                col = find_col(IA, j);
                JB = horzcat(JB, col);
                subJA(j) = 0;
                break;
            end
        end
    end
    
	% make BN
    JAsize = size(JA);
    ANsize = size(AN);
    if(JAsize(1) == 1)
	    JA = JA';
    end

    if(ANsize(1) == 1)
	    AN = AN';
    end

    [AA BN] = quicksort(JA, AN);
    
    IB = IB';
    JB = JB';
    
end
