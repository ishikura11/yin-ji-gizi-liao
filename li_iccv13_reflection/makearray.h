#define makearray(m,n,a) \
{ \
        int i,j; \
\
        /*********************/ \
        (a)=(double **)malloc(sizeof(double *)*(m)); \
		if ((a) == NULL) exit(1); \
        for(i=0;i<(m);i++) \
        { \
                (a)[i]=(double *)malloc(sizeof(double)*(n)); \
			if ((a)[i] == NULL) exit(1); \
        } \
}
