#pragma once

/* ----------------------------------------------------
takapack.h is written by Takashi Ijiri @ RIKEN
This libraly support computing
 + Dense  linear system (by CG method)
 + Sparse linear system (by CG method)
 + Sparse linear system (by LU factorization)

 This codes is distributed with NYSL (Version 0.9982) license.

 免責事項   : 本ソースコードによって起きたいかなる障害についても、著者は一切の責任を負いません。
 disclaimer : The author (Takashi Ijiri) is not to be held responsible for any problems caused by this software.
 ----------------------------------------------------*/

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h>

#include "math.h"
#include <list>
#include <algorithm>
using namespace std;

#include <unistd.h>
//#include <omp.h>
#include "sds_lib.h"

#include <fstream>
#include <iostream>
#include <iomanip>
#define DIM 2048
#define SUBDIV_DIM 256
#define _SPROW_SIZE DIM
#define _SPCOL_SIZE 5000 //DIM*DIM/50
#define _OUT_SPCOL_SIZE 10000


#define MCH 0
#define SPMCH 1
#define INCH 2
#define SPINCH 3
#define LU 4

typedef float t_element;

//#define INV
//#define VERIFY
//#define SOL
/*
void takapack_test(int *Ap, int *Ai, t_element *Ax, t_element *x1, t_element *x2, t_element *b, t_element (*e)[DIM]);

//CG method (for sparse matrix)
bool takapack_CG_sparse_solve( const int N,
								const int    *Ap, const int* Ai, const t_element* Ax,
								const t_element *b ,
								t_element* result, t_element  threshold);
//CG method (for dense matrix)
void takapack_CG_dense_solve( const int N, t_element** A, const t_element* b, t_element* result);

//LU method
void takapack_traceMat( int N, const int *Ap , const int *Ai , const t_element *Ax );
void takapack_LU_factorization( const int N, const int *Ap , const int *Ai , const t_element *Ax, int* &LUp, int* &LUi, t_element* &LUx, int* &LU_rowFlip);
void takapack_LU_solve        ( const int N, const int *LUp, const int *LUi, const t_element *LUx, const int *LU_rowFlip, const t_element *b, t_element *res);
void takapack_LU_free         (              int *LUp, int *LUi, t_element *LUx, int *LU_rowFlip );

void verify_inv( int N, const int *Ap , const int *Ai , const t_element *Ax, const t_element (*inv)[DIM] );
*/

//for Compressed row storage
// Sparse Matrix size define


// CRS用３配列
struct _crsData{
  int *row;//[_SPROW_SIZE + 1]; // 縦軸
  int *col;//[_SPROW_SIZE * _SPROW_SIZE]; // 横軸
  t_element *val;//[_SPROW_SIZE * _SPROW_SIZE]; // 実際の要素
} typedef crsData;


//void gencrs(t_element x1[DIM], t_element x2[DIM], t_element b[DIM], t_element (*e)[DIM], t_element *sparseMatrix, int dim);

//int ModifiedCholeskyDecomp(const t_element *A, t_element (*L)[DIM], t_element *d, int n);
//int IncompleteCholeskyDecomp(const t_element *A, t_element (*L)[DIM], t_element *d, int n);

// global variables
//int symm = 0;

//ファイル入力関連
#define BUF_SIZE (1024*64)

int Getc();
void unGetc(int ch);
void parse_error(char *msg);
void skip_line();
int skip_blanks();
void get_word(char word[]);
void init_read(char *file);
void inputfile_mtx(char *file, t_element *mat, int *dim, int *nnz);

int Getc();
/*
{
    int ch;

    if (buf_pos == n_chars_in_buf) {
	n_chars_in_buf = read(fd, &buf[128], BUF_SIZE);
	if (n_chars_in_buf == 0)
	    return EOF;
	n_chars_in_buf += 128;
	buf_pos = 128;
    }
    ch = buf[buf_pos++];
    return ch;
}
*/
void unGetc(int ch);
/*
{
    buf[--buf_pos] = ch;
}
*/
void parse_error(char *msg);
/*
{
    fprintf(stderr, "%s\n", msg);
    exit(1);
}
*/
void skip_line();
/*
{
    int ch;

    while ((ch = Getc()) != '\n' && ch != EOF);
}
*/
int skip_blanks();
/*
{
    int ch;

    while ((ch = Getc()) == ' ' || ch == '\t');
    unGetc(ch);
    return ch;
}
*/
void get_word(char word[]);
/*
{
    int ch, i = 0;

    while ((ch = Getc()) == ' ' || ch == '\t');

    if (ch != ' ' && ch != '\t' && ch != '\n' && ch != EOF) {
	word[i++] = ch;
	while ((ch = Getc()) != ' ' && ch != '\t' && ch != '\n' && ch != EOF)
	    word[i++] = ch;
    }

    word[i] = '\0';
    unGetc(ch);

    return;

    skip_blanks();
    while ((ch = Getc()) == ' ' || ch == '\t');
    unGetc(ch);

    while ((ch = Getc()) != ' ' && ch != '\t' && ch != '\n' && ch != EOF)
	word[i++] = ch;

    word[i] = '\0';
    unGetc(ch);
}
*/
void init_read(char *file);
/*
{
    if ((fd = open(file, O_RDONLY)) < 0) {
	fprintf(stderr, "can't open %s\n", file);
	exit(1);
    }
    n_chars_in_buf = 128;
    buf_pos = 128;
}
*/
void export_LU(int *lup, int *lui, t_element *lux, int nunZeroEntryNum, int matsize);
/*
{
	char *filelup = "testlup.dat";
	char *filelui = "testlui.dat";
	char *filelux = "testlux.dat";

	printf("size = %d\n", matsize);
	printf("nunZeroEntryNum = %d\n", nunZeroEntryNum);

	ofstream writing_lup;
	ofstream writing_lui;
	ofstream writing_lux;

	writing_lup.open(filelup, std::ios::out);
	writing_lui.open(filelui, std::ios::out);
	writing_lux.open(filelux, std::ios::out);

	for (int i = 0; i < nunZeroEntryNum; i++) {
		writing_lui << lui[i] << endl;
		writing_lux << lux[i] << endl;
	}
	for (int i = 0; i < matsize; i++) {
		writing_lup << fixed << setprecision(15) << lup[i] << endl;
	}
}
*/
void swap(t_element *array, int i, int j);
/*
{
	t_element tmp;
	tmp = array[i];
	array[i] = array[j];
	array[j] = tmp;
}
*/
void swap(int *array, int i, int j);
/*
{
	int tmp;
	tmp = array[i];
	array[i] = array[j];
	array[j] = tmp;
}
*/
void q_sort(int *numbers, int *numbers2, t_element *numbers3, int left, int right);
/*
{
	int pivot, l_hold, r_hold;

	l_hold = left;
	r_hold = right;
	pivot = numbers[(left + right)/2];
	while (1) {
		while (numbers[l_hold] < pivot)
			l_hold++;
		while ((numbers[r_hold] > pivot))
			r_hold--;
		if (l_hold >= r_hold) break;
				swap(numbers,  l_hold, r_hold);
				swap(numbers2, l_hold, r_hold);
				swap(numbers3, l_hold, r_hold);
				l_hold++;
				r_hold--;
	}
	if (left < l_hold - 1)
		q_sort(numbers, numbers2, numbers3, left, l_hold-1);
	if (right > r_hold + 1)
		q_sort(numbers, numbers2, numbers3, r_hold+1, right);
}
*/
void q_sort(int *numbers, t_element *numbers2, int left, int right);
/*
{
	int pivot, l_hold, r_hold;

	l_hold = left;
	r_hold = right;
	pivot = numbers[(left + right)/2];
	while (1) {
		while (numbers[l_hold] < pivot)
			l_hold++;
		while ((numbers[r_hold] > pivot))
			r_hold--;
		if (l_hold >= r_hold) break;
				swap(numbers,  l_hold, r_hold);
				swap(numbers2, l_hold, r_hold);
				l_hold++;
				r_hold--;
	}
	if (left < l_hold - 1)
		q_sort(numbers, numbers2, left, l_hold-1);
	if (right > r_hold + 1)
		q_sort(numbers, numbers2, r_hold+1, right);
}
*/
void arr_init(int *arr, int size);
/*
{
	for (int i = 0; i < size; i++) {
		arr[i] = 99999999;
	}
}
*/
void arr_init(t_element *arr, int size);
/*
{
	for (int i = 0; i < size; i++) {
		arr[i] = 99999999;
	}
}
*/
