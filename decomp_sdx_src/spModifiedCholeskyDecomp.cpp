#include "takapack.h"

/* CRS形式に対応 */
//int spModifiedCholeskyDecomp(const int *Ap, const int *Ai, const t_element *A, int *Lp, int *Li, t_element *Lx, t_element *d, int n)
#pragma SDS data zero_copy(Ap, Ai, A, Lp, Li, Lx, d)
//#pragma SDS data access_pattern(Ap:SEQUENTIAL, Ai:SEQUENTIAL, Ax:SEQUENTIAL)
#pragma SDS data mem_attribute(Ap:PHYSICAL_CONTIGUOUS, Ai:PHYSICAL_CONTIGUOUS, A:PHYSICAL_CONTIGUOUS, Li:PHYSICAL_CONTIGUOUS, Lx:PHYSICAL_CONTIGUOUS, d:PHYSICAL_CONTIGUOUS)
int spModifiedCholeskyDecomp(const int Ap[_SPROW_SIZE], const int Ai[_SPCOL_SIZE], const t_element A[_SPCOL_SIZE], int Lp[DIM+1], int Li[_OUT_SPCOL_SIZE], t_element Lx[_OUT_SPCOL_SIZE], t_element d[DIM], int n)
{
    if(n <= 0) return 0;
    /***
	int idx = 0;
	int Apidx = 0;
	int num;
	  for (Apidx = 0;Apidx < n;Apidx++){
	    num = Ap[Apidx+1] - Ap[Apidx];

	    for (int x = 0;x < n;x++){
	      if( x == Ai[idx] && num > 0) { fprintf( stderr, "%.4f  ", A[idx]); ++idx; --num;}
	      else               { fprintf( stderr, "*.****  " );                  }
	    } fprintf(stderr,"\n");
	}
    ***/

    d[0] = A[0];
    //L[0][0] = 1.0;
	/*
    Lp = new int   [ n+1 ];
	Li = new int   [ n * n / 2 + n];
	Lx = new t_element[ n * n / 2 + n ];
*/
	Li[0] = 0;
	Lp[0] = 1;
	Lx[0] = 1.0;
    //    fprintf(stderr,"A[0] %f\n",A[0]);
    int idx = 0;
    int cidx;
    int num;
    t_element val;
    t_element lld;
    t_element ld;
    t_element tmp_ik;
    t_element tmp_cidxk;
	t_element tmp_lld[DIM];
	t_element tmp_lld2[DIM/SUBDIV_DIM];
	t_element tmp_ld[DIM];
	t_element tmp_ld2[DIM/SUBDIV_DIM];
	int lidx = 1;
	int cidx_ind = 0;
	int ik_ind = 0;
	int cidx_num = 0;
	int ik_num = 0;
	int ikcidxmaxnum;
	int lld_count = 0;
#pragma HLS array_partition variable=tmp_lld cyclic factor=256
#pragma HLS array_partition variable=tmp_lld2 block factor=8
#pragma HLS array_partition variable=tmp_ld cyclic factor=256
#pragma HLS array_partition variable=tmp_ld2 block factor=8
	for (int i = 1; i < n; i++) {
		if(i % 100 == 0)fprintf(stderr,".");
		idx = Ap[i];//対角成分から先は見ないから毎回基底アドレスを代入しないといけない
		num = Ap[i+1] - Ap[i]; //この列ではいくつの要素を処理するか
		//fprintf(stderr,"%d\n",num);
		for (cidx = 0; cidx < i; cidx++) {
			if( cidx == Ai[idx] && num > 0)
			{
				lld = A[idx];
				++idx;
				--num;
			}
			else
				lld = 0.;

			ik_ind = 0;
			cidx_ind = 0;
			ik_num = lidx - Lp[i-1];

			cidx_num = Lp[cidx] - Lp[cidx-1];
			ikcidxmaxnum = max(ik_num, cidx_num);

			for (int k = 0; k < DIM; k++) {
#pragma HLS unroll factor=256
				tmp_lld[k] = 0.;
			}
			lld_count = 0;
			for (int k = 0; k < cidx; k++) {
#pragma HLS pipeline
				tmp_ik = 0; tmp_cidxk = 0;
				//lld -= L[i][k]*L[cidx][k]*d[k];
				if (Li[Lp[i-1] + ik_ind] == k && ik_num > 0) {
					tmp_ik = Lx[Lp[i-1] + ik_ind];
					ik_ind++;
					ik_num--;
				}
				if (Li[Lp[cidx-1] + cidx_ind] == k && cidx_num > 0) {
					tmp_cidxk = Lx[Lp[cidx-1] + cidx_ind];
					cidx_ind++;
					cidx_num--;
				}
				tmp_lld[lld_count] = tmp_ik*tmp_cidxk*d[k];
				if (tmp_lld[lld_count] != 0) lld_count++;
				//lld -= tmp_ik*tmp_cidxk*d[k];
				/*
				if (i >= 5 && i < 7) {
					printf("i = %d, cidx = %d, k = %d, lld = %f\n", i, cidx, k, lld);
					printf("tmp_ik = %f, tmp_cidxk = %f\n", tmp_ik, tmp_cidxk);
				}
				*/
			}
			ikcidxmaxnum = lld_count;
			if (ikcidxmaxnum != 0) {
			for (int j = 0; j < ikcidxmaxnum/(SUBDIV_DIM+1) + 1; j++) {
#pragma HLS pipeline
				for (int k = 0; k < SUBDIV_DIM; k+=2) {
#pragma HLS unroll //factor = 2
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+1 + j*SUBDIV_DIM];
				}
				tmp_lld2[0] = tmp_lld[j*SUBDIV_DIM];

				if (ikcidxmaxnum > 2) {
					for (int k = 0; k < SUBDIV_DIM; k+=4) {
#pragma HLS unroll //factor = 2
						tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+2 + j*SUBDIV_DIM];
					}
				tmp_lld2[0] = tmp_lld[j*SUBDIV_DIM];// + tmp_lld[2 + j*SUBDIV_DIM];
				}
				if (ikcidxmaxnum > 4) {
					for (int k = 0; k < SUBDIV_DIM; k+=8) {
#pragma HLS unroll //factor = 2
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+4 + j*SUBDIV_DIM];
					}
				tmp_lld2[0] = tmp_lld[j*SUBDIV_DIM];// + tmp_lld[4 + j*SUBDIV_DIM];
				}
				if (ikcidxmaxnum > 8) {
					for (int k = 0; k < SUBDIV_DIM; k+=16) {
#pragma HLS unroll //factor = 2
						tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+8 + j*SUBDIV_DIM];
					}
				tmp_lld2[0] = tmp_lld[j*SUBDIV_DIM];// + tmp_lld[8 + j*SUBDIV_DIM];
				}
				if (ikcidxmaxnum > 16) {
					for (int k = 0; k < SUBDIV_DIM; k+=32) {
#pragma HLS unroll //factor = 2
						tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+16 + j*SUBDIV_DIM];
					}
				tmp_lld2[0] = tmp_lld[j*SUBDIV_DIM];// + tmp_lld[16 + j*SUBDIV_DIM];
				}
				if (ikcidxmaxnum > 32) {
					for (int k = 0; k < SUBDIV_DIM; k+=64) {
#pragma HLS unroll //factor = 2
						tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+32 + j*SUBDIV_DIM];
					}
				tmp_lld2[0] = tmp_lld[j*SUBDIV_DIM];// + tmp_lld[32 + j*SUBDIV_DIM];
				}
				if (ikcidxmaxnum > 64) {
					for (int k = 0; k < SUBDIV_DIM; k+=128) {
#pragma HLS unroll //factor = 2
						tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+64 + j*SUBDIV_DIM];
					}
				tmp_lld2[0] = tmp_lld[j*SUBDIV_DIM];// + tmp_lld[128 + j*SUBDIV_DIM];
				}
				if (ikcidxmaxnum > 128) {
					tmp_lld[j*SUBDIV_DIM] += tmp_lld[128 + j*SUBDIV_DIM];
					tmp_lld2[j] = tmp_lld[j*SUBDIV_DIM];// + tmp_lld[128 + j*SUBDIV_DIM];
				}
			}
			if (ikcidxmaxnum <= 256)
				lld -= tmp_lld2[0];

			if (ikcidxmaxnum > 256) {
				for (int k = 0; k < ikcidxmaxnum/SUBDIV_DIM; k+=2) {
#pragma HLS unroll //factor = 2
					tmp_lld2[k] += tmp_lld2[k+1];
				}
/*
				for (int k = 0; k < DIM/SUBDIV_DIM; k+=4) {
#pragma HLS unroll //factor = 2
					tmp_lld2[k] += tmp_lld2[k+2];
				}
			/*
			for (int k = 0; k < DIM/SUBDIV_DIM; k+=8) {
#pragma HLS unroll //factor = 2
				tmp_lld2[k] += tmp_lld2[k+4];
			}
			for (int k = 0; k < DIM/SUBDIV_DIM; k+=16) {
#pragma HLS unroll //factor = 2
				tmp_lld2[k] += tmp_lld2[k+8];
			}
			*/
			/*
			for (int k = 0; k < DIM/SUBDIV_DIM; k+=32) {
#pragma HLS unroll //factor = 2
				tmp_lld2[k] += tmp_lld2[k+16];
			}
			*/
/*
			for (int k = 0; k < DIM; k+=256) {
#pragma HLS unroll factor = 2
				tmp_lld[k] += tmp_lld[k+128];
			}

			for (int k = 0; k < DIM; k+=512) {
#pragma HLS unroll factor = 2
				tmp_lld[k] += tmp_lld[k+256];
			}
			*/
				lld -= tmp_lld2[0] + tmp_lld2[2];
			}
			}
			//ld -= total;
			//L[i][cidx] = (1.0/d[cidx])*lld;
			if (lld != 0) {
				Lx[lidx] = (1.0/d[cidx])*lld;
				Li[lidx] = cidx;
				lidx++;
			}
		}

		//if(cidx != i){fprintf(stderr,"%d %d\n",cidx,i);sleep(1);}
		//fprintf(stderr,"%d(%d) \n",num,i);
		if(i == Ai[idx] )
			ld = A[idx];
		else
			ld = 0.0;
		ik_ind = 0;
		ik_num = lidx - Lp[i-1];


		for (int k = 0; k < DIM; k++) {
#pragma HLS unroll factor=256
			tmp_ld[k] = 0.;
		}

		for(int k = 0; k < i; ++k){
#pragma HLS pipeline
			//ld -= L[i][k]*L[i][k]*d[k];
			tmp_ik = 0;
			if (Li[Lp[i-1] + ik_ind] == k && ik_num > 0) {
				tmp_ik = Lx[Lp[i-1] + ik_ind];
				ik_ind++;
				ik_num--;
			}
			//ld -= tmp_ik*tmp_ik*d[k];
			tmp_ld[k] = tmp_ik*tmp_ik*d[k];
		}

		for (int j = 0; j < DIM/SUBDIV_DIM; j++) {
#pragma HLS pipeline
			for (int k = 0; k < SUBDIV_DIM; k+=2) {
#pragma HLS unroll //factor = 2
				tmp_ld[k + j*SUBDIV_DIM] += tmp_ld[k+1 + j*SUBDIV_DIM];
			}
			for (int k = 0; k < SUBDIV_DIM; k+=4) {
#pragma HLS unroll //factor = 2
				tmp_ld[k + j*SUBDIV_DIM] += tmp_ld[k+2 + j*SUBDIV_DIM];
			}
			for (int k = 0; k < SUBDIV_DIM; k+=8) {
#pragma HLS unroll //factor = 2
				tmp_ld[k + j*SUBDIV_DIM] += tmp_ld[k+4 + j*SUBDIV_DIM];
			}
			for (int k = 0; k < SUBDIV_DIM; k+=16) {
#pragma HLS unroll //factor = 2
				tmp_ld[k + j*SUBDIV_DIM] += tmp_ld[k+8 + j*SUBDIV_DIM];
			}
			for (int k = 0; k < SUBDIV_DIM; k+=32) {
#pragma HLS unroll //factor = 2
				tmp_ld[k + j*SUBDIV_DIM] += tmp_ld[k+16 + j*SUBDIV_DIM];
			}

			for (int k = 0; k < SUBDIV_DIM; k+=64) {
#pragma HLS unroll //factor = 2
				tmp_ld[k + j*SUBDIV_DIM] += tmp_ld[k+32 + j*SUBDIV_DIM];
			}

			for (int k = 0; k < SUBDIV_DIM; k+=128) {
#pragma HLS unroll //factor = 2
				tmp_ld[k + j*SUBDIV_DIM] += tmp_ld[k+64 + j*SUBDIV_DIM];
			}
			tmp_ld2[j] = tmp_ld[j*SUBDIV_DIM] + tmp_ld[128 + j*SUBDIV_DIM];
		}

		for (int k = 0; k < DIM/SUBDIV_DIM; k+=2) {
#pragma HLS unroll //factor = 2
			tmp_ld2[k] += tmp_ld2[k+1];
		}
		for (int k = 0; k < DIM/SUBDIV_DIM; k+=4) {
#pragma HLS unroll //factor = 2
			tmp_ld2[k] += tmp_ld2[k+2];
		}
		/*
		for (int k = 0; k < DIM/SUBDIV_DIM; k+=8) {
#pragma HLS unroll //factor = 2
			tmp_lld2[k] += tmp_lld2[k+4];
		}
		for (int k = 0; k < DIM/SUBDIV_DIM; k+=16) {
#pragma HLS unroll //factor = 2
			tmp_lld2[k] += tmp_lld2[k+8];
		}
		*/
		ld -= tmp_ld2[0] + tmp_ld2[4];
/*
		for (int k = 0; k < DIM/SUBDIV_DIM; k++) {
			ld -= tmp_ld2[k];
		}
*/

		d[i] = ld;
		//L[i][i] = 1.0;
		Lx[lidx] = 1;
		Li[lidx] = i;
		lidx++;
		Lp[i] = lidx;
	}
    /*
	for (int i = 0; i < 10; i++) {
		printf("Lx[%d] = %f\n", i, Lx[i]);
	}
	*/


	fprintf(stderr,"LDLT factorization(sp) Completed.\n");
	return 1;

}
