#include "takapack.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#pragma SDS data zero_copy(Ap, Ai, A, Lp, Li, Lx, d)
//#pragma SDS data access_pattern(Ap:SEQUENTIAL, Ai:SEQUENTIAL, Ax:SEQUENTIAL)
#pragma SDS data mem_attribute(Ap:PHYSICAL_CONTIGUOUS, Ai:PHYSICAL_CONTIGUOUS, A:PHYSICAL_CONTIGUOUS, Li:PHYSICAL_CONTIGUOUS, Lx:PHYSICAL_CONTIGUOUS, d:PHYSICAL_CONTIGUOUS)

int spModifiedCholeskyDecomp_2loop(const int Ap[_SPROW_SIZE], const int Ai[_SPCOL_SIZE], const t_element A[_SPCOL_SIZE], int Lp[DIM+1], int Li[_OUT_SPCOL_SIZE], t_element Lx[_OUT_SPCOL_SIZE], t_element d[DIM], int n)
{
    if(n <= 0) return 0;
    /***
	int idx = 0;
	int Apidx = 0;
	int num;
	  for (Apidx = 0;Apidx < n;Apidx++){
	    num = Ap[Apidx+1] - Ap[Apidx];

	    for (int x = 0;x < n;x++){
	      if( x == Ai[idx] && num > 0) { fprintf( stderr, "%.4f  ", A[idx]); ++idx; --num;}
	      else               { fprintf( stderr, "*.****  " );                  }
	    } fprintf(stderr,"\n");
	}
    ***/

    d[0] = A[0];
    //L[0][0] = 1.0;
	/*
    Lp = new int   [ n+1 ];
	Li = new int   [ n * n / 2 + n];
	Lx = new t_element[ n * n / 2 + n ];
*/
	//Li[0] = 0;
	Lp[0] = 1;
	//Lx[0] = 1.0;
    //    fprintf(stderr,"A[0] %f\n",A[0]);
    int idx = 0;
    int cidx;
    int num;
    t_element val;
    t_element lld;
    t_element ld;
    //t_element tmp_ik;
    //t_element tmp_cidxk;
	int lidx = 1;
	int cidx_ind = 0;
	int ik_ind = 0;
	int cidx_num = 0;
	int ik_num = 0;
	int count = 0;
	int ik_num_max = 0;
	int lld_count = 0;
	//int lld_count_max = 0;
	//int lld_total = 0;
	t_element tmp_lld[DIM];
	t_element tmp_lld2[DIM/SUBDIV_DIM];
	t_element tmp_ik[DIM];
	t_element tmp_cidxk[DIM];
	int _Li[_OUT_SPCOL_SIZE];
	t_element _Lx[_OUT_SPCOL_SIZE];
#pragma HLS array_partition variable=tmp_lld cyclic factor=256
#pragma HLS array_partition variable=tmp_lld2 block factor=8
#pragma HLS array_partition variable=_Li cyclic factor=2
#pragma HLS array_partition variable=_Lx cyclic factor=2
#pragma HLS array_partition variable=tmp_ik cyclic factor=16
#pragma HLS array_partition variable=tmp_cidxk cyclic factor=16

	for (int i = 0; i < _OUT_SPCOL_SIZE; i++)
	{
#pragma HLS pipeline
		_Li[i] = 0;
		_Lx[i] = 0;
	}

	_Li[0] = 0;
	_Lx[0] = 1.0;

	for (int i = 1; i < n; i++)
	{
		if(i % 100 == 0)fprintf(stderr,".");
		//printf("i = %d\n",i );
		idx = Ap[i];//対角成分から先は見ないから毎回基底アドレスを代入しないといけない
		num = Ap[i+1] - Ap[i]; //この列ではいくつの要素を処理するか
		//fprintf(stderr,"%d\n",num);
		for (cidx = 0; cidx < i; cidx++)
		{
			if( cidx == Ai[idx] && num > 0)
			{
				lld = A[idx];
				++idx;
				--num;
			}
			else
				lld = 0.;

			ik_ind = 0;
			cidx_ind = 0;
			ik_num = lidx - Lp[i-1];
			cidx_num = Lp[cidx] - Lp[cidx-1];

			if (ik_num > ik_num_max) ik_num_max = ik_num;
			for (int k = 0; k < DIM;k++)
			{
#pragma HLS unroll //factor = 2
				tmp_lld[k] = 0;
			}
			lld_count = 0;

			for (int k = 0; k < DIM; k++)
			{
#pragma HLS unroll
				tmp_ik[k] = 0;
				tmp_cidxk[k] = 0;
			}

			for (int k = 0; k < cidx; k++)
			{
#pragma HLS unroll factor=2
				//tmp_ik = 0; tmp_cidxk = 0;
				//lld -= L[i][k]*L[cidx][k]*d[k];
				if (k > _Li[Lp[i-1] + ik_num-1] || k > _Li[Lp[cidx-1] + cidx_num-1] ||
					k < _Li[Lp[i-1]]			   || k < _Li[Lp[cidx-1]])
				{
					//printf("k = %d\n", k);
					tmp_lld[k] = 0;
				} else {
					for (int j = 0; j < ik_num; j++)
					{
#pragma HLS unroll
						if (_Li[Lp[i-1] + j] == k)
						{
							tmp_ik[k] = _Lx[Lp[i-1] + j];
						}
					}
					for (int j = 0; j < cidx_num; j++)
					{
#pragma HLS unroll
						if (_Li[Lp[cidx-1] + j] == k)
						{
							tmp_cidxk[k] = _Lx[Lp[cidx-1] + j];
						}
					}
					count++;
					tmp_lld[k] = tmp_ik[k] * tmp_cidxk[k] * d[k];
				}
			}

			for (int j = 0; j < DIM/SUBDIV_DIM; j++)
			{
#pragma HLS pipeline
				for (int k = 0; k < SUBDIV_DIM; k+=2)
				{
#pragma HLS unroll //factor = 2
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+1 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=4)
				{
#pragma HLS unroll //factor = 2
					//tmp_lld[k] += tmp_lld[k+2];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+2 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=8)
				{
#pragma HLS unroll //factor = 2
					//tmp_lld[k] += tmp_lld[k+4];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+4 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=16)
				{
#pragma HLS unroll //factor = 2
					//tmp_lld[k] += tmp_lld[k+8];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+8 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=32)
				{
#pragma HLS unroll //factor = 2
					//tmp_lld[k] += tmp_lld[k+16];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+16 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=64)
				{
#pragma HLS unroll //factor = 2
					//tmp_lld[k] += tmp_lld[k+32];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+32 + j*SUBDIV_DIM];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=128)
				{
#pragma HLS unroll //factor = 2
					//tmp_lld[k] += tmp_lld[k+64];
					tmp_lld[k + j*SUBDIV_DIM] += tmp_lld[k+64 + j*SUBDIV_DIM];
				}
				/*
				for (int k = 0; k < SUBDIV_DIM; k+=256) {
					tmp_lld[k] += tmp_lld[k+128];
				}
				for (int k = 0; k < SUBDIV_DIM; k+=512) {
					tmp_lld[k] += tmp_lld[k+256];
				}
				*/
				tmp_lld2[j] = tmp_lld[j*SUBDIV_DIM] + tmp_lld[128 + j*SUBDIV_DIM];
			}

			for (int k = 0; k < DIM/SUBDIV_DIM; k+=2)
			{
#pragma HLS unroll //factor = 2
				tmp_lld2[k] += tmp_lld2[k+1];
			}

			for (int k = 0; k < DIM/SUBDIV_DIM; k+=4)
			{
#pragma HLS unroll //factor = 2
				tmp_lld2[k] += tmp_lld2[k+2];
			}
/*
			for (int k = 0; k < DIM/SUBDIV_DIM; k+=8) {
#pragma HLS unroll //factor = 2
				tmp_lld2[k] += tmp_lld2[k+4];
			}

			for (int k = 0; k < DIM/SUBDIV_DIM; k+=16) {
#pragma HLS unroll //factor = 2
				tmp_lld2[k] += tmp_lld2[k+8];
			}
*/
			lld -= tmp_lld2[0] + tmp_lld2[4];

			//L[i][cidx] = (1.0/d[cidx])*lld;
			if (lld != 0)
			{
			//if (fabs(lld) > 0.1) {
			//if (fabs(lld) != DBL_EPSILON) {
				//printf("lidx = %d\n", lidx);
				_Lx[lidx] = (1.0/d[cidx])*lld;
				_Li[lidx] = cidx;
				lidx++;
			}
		}
		if(i == Ai[idx] )
			ld = A[idx];
		else
			ld = 0.0;
		ik_ind = 0;
		ik_num = lidx - Lp[i-1];
		for (int k = 0; k < DIM; k++)
		{
#pragma HLS unroll
			tmp_ik[k] = 0;
		}
		for(int k = 0; k < i; ++k)
		{
#pragma HLS pipeline
			//ld -= L[i][k]*L[i][k]*d[k];
			//tmp_ik[k] = 0;
			if (_Li[Lp[i-1] + ik_ind] == k && ik_num > 0)
			{
				tmp_ik[k] = _Lx[Lp[i-1] + ik_ind];
				ik_ind++;
				ik_num--;
			}
			ld -= tmp_ik[k]*tmp_ik[k]*d[k];
		}
		d[i] = ld;
		//L[i][i] = 1.0;
		_Lx[lidx] = 1;
		_Li[lidx] = i;
		lidx++;
		Lp[i] = lidx;
	}
    /*
	for (int i = 0; i < 10; i++) {
		printf("Lx[%d] = %f\n", i, Lx[i]);
	}
	*/

	//printf("lld_count avg:%d, max:%d \n", lld_total/n, lld_count_max);
	printf("ik_num avg:%d, max:%d \n", lidx/n, ik_num_max);
	for(int i = 0; i < lidx; i++)
	{
#pragma HLS pipeline
		Li[i] = _Li[i];
		Lx[i] = _Lx[i];
	}

	fprintf(stderr,"LDLT factorization(sp) Completed.\n");
	return 1;

}

